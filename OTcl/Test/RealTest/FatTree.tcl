#source "/home/sabatino/ns-allinone-2.35/ns-2.35/myFiles/OTcl/Test/RealTest/FatTree.tcl"
 
# Create a new Simulator object
set ns [new Simulator]

# Create the DcManager object
set dcManager [new DcManager]

####################################################################
####################### Simulation parameters ######################
####################################################################

# In case you want to define a trace file
set dir(traces) "/home/sabatino/Desktop/Traces"
set tracefile [open "$dir(traces)/main.tr" w]
#$ns trace-all $tracefile
set namfile [open "$dir(traces)/main.nam" w]
#$ns namtrace-all $namfile

# Simulation start time 
set sim(start_time) 0.0
# Simulation end time  
set sim(end_time) 200.0 
# Simulation total time
set sim(tot_time) [expr $sim(end_time) - $sim(start_time)]
puts "\nSimulation time: $sim(tot_time)"

# Routing protocol (Link State)
$ns rtproto LS

# Multipath routing
Node set multiPath_ 1

# Switches, racks, servers and vms number
set coreSwitchN 2
set aggSwitchN 2
set accSwitchN 4
set rackN $accSwitchN
set serverN 20
set vmN 100

####################################################################
########################## RACKS CREATION ##########################
####################################################################

# Create racks
for {set i 0} {$i < $rackN} {incr i} {
  set rack($i) [new Rack [expr $i+1] 2 10]
  #puts "rack($i) = $rack($i)"
  #$rack($i) print ;puts ""
}
puts "RACKS CREATION COMPLETE"

####################################################################
######################## SWITCHES CREATION #########################
####################################################################

# Create core switches
for {set i 0} {$i < $coreSwitchN} {incr i} {
  set coreSwitch($i) [new Switch [expr $i+1] SWITCH_10Gbps]
  # Turn on core switch
  $coreSwitch($i) start
  #puts "coreSwitch($i) = $coreSwitch($i)"
  #$coreSwitch($i) print ;puts ""
  set coreSwitch_Node($i) [$coreSwitch($i) get-node]
  $coreSwitch_Node($i) color Red
  $coreSwitch_Node($i) label "CoreSwitch $i" 
}

# Create aggregation switches
for {set i 0} {$i < $aggSwitchN} {incr i} {
  set id [expr $i+$coreSwitchN+1]
  set aggSwitch($i) [new Switch $id SWITCH_10Gbps]
  # Turn on aggregation switch
  $aggSwitch($i) start
  #puts "aggSwitch($i) = $aggSwitch($i)"
  #$aggSwitch($i) print ;puts ""
  set aggSwitch_Node($i) [$aggSwitch($i) get-node]
  $aggSwitch_Node($i) color Blue
  $aggSwitch_Node($i) label "AggSwitch $i" 
}

# Create access switches
for {set i 0} {$i < $accSwitchN} {incr i} {
  set id [expr $i+$coreSwitchN+$aggSwitchN+1]
  set accSwitch($i) [new ToRSwitch $id SWITCH_1Gbps]
  # Turn on access switch
  $accSwitch($i) start
  #puts "accSwitch($i) = $accSwitch($i)"
  #$accSwitch($i) print ;puts ""
  set accSwitch_Node($i) [$accSwitch($i) get-node]
  $accSwitch_Node($i) color Green
  $accSwitch_Node($i) label "AccSwitch $i"

}
puts "SWITCHES CREATION COMPLETE"

# Assign each access switch to the corresponding rack
for {set i 0} {$i < $accSwitchN} {incr i} {
  set j [expr ($i%$rackN)]
  $rack($j) add-switch $accSwitch($i)
}

puts "SWITCHES ASSIGNED TO RACKS"

####################################################################
######################### SERVERS CREATION #########################
####################################################################

# Create servers
for {set i 0} {$i < $serverN} {incr i} {
  set server($i) [new Pm [expr $i+1]]
  $server($i) add-resource [new Cpu 1 CPU_4CORE]
  $server($i) add-resource [new Cpu 2 CPU_4CORE]
  $server($i) add-resource [new Memory 3 MEMORY_8GB]
  $server($i) add-resource [new Storage 4 STORAGE_1TB]
  $server($i) add-resource [new Networking 5 NET_1GbE]
  # Turn on server
  $server($i) set-current-state ON
  #puts "server($i) = $server($i)"
  #$server($i) print ;puts "" 
  set server_Node($i) [$server($i) get-node]
  $server_Node($i) color Black
  $server_Node($i) label "Server $i"
}

# Server 0 (Inter-DC communications)
set serverOut [new Pm 0]
$serverOut add-resource [new Cpu 1 CPU_4CORE]
$serverOut add-resource [new Memory 2 MEMORY_8GB]
$serverOut add-resource [new Storage 3 STORAGE_1TB]
$serverOut add-resource [new Networking 4 NET_1GbE]
$serverOut set-current-state ON
#$serverOut print ;puts "" 
set serverOut_Node [$serverOut get-node]
$serverOut_Node color Black
$serverOut_Node label "Out"

puts "SERVERS CREATION COMPLETE"

# Add servers to racks
for {set i 0} {$i < $serverN} {incr i} {
  set j [expr ($i%$rackN)]
  $rack($j) add-pm $server($i)
}

puts "SERVERS ASSIGNED TO RACKS"

####################################################################
###################### VIRTUAL MACHINES CREATION ###################
####################################################################

# Create virtual machines
for {set i 0} {$i < $vmN} {incr i} {
  set vm($i) [new Vm [expr $i+1] NANO]
  #puts "vm($i) = $vm($i)"
  #$vm($i) print ;puts ""
}

# Vm 0 (Inter-DC communications)
set vmOut [new Vm 0 LARGE]

puts "VMS CREATION COMPLETE"

# Spread the Vm between the servers
for {set i 0} {$i < $vmN} {incr i} {
  set j [expr ($i%$serverN)]
  $server($j) add-vm $vm($i)
}

# Add vmOut on serverOut
$serverOut add-vm $vmOut

#####################################################################
############################ DCN Topology ###########################
#####################################################################

# Links between serverOut and Core switches
for {set i 0} {$i < $coreSwitchN} {incr i} {
  $coreSwitch($i) link-pm-to-port $serverOut 11
  #puts "serverOut connected to port 11 of coreSwitch($i)"
}

# Links between Core and Aggregation switches: 
# each aggregation switch is connected with all the core switches
set w 10
for {set i 0} {$i < $coreSwitchN} {incr i} {
  set k 1
  for {set j 0} {$j < $aggSwitchN} {incr j} {
    $coreSwitch($i) link-switch-to-port $aggSwitch($j) $k $w
    #puts "port $k of coreSwitch($i) connected to port $w of aggSwitch($j)"
    incr k
  }
  incr w -1
}

# Links between Aggregation and Access switches:
# each access switch is connected with all the aggregation switches
set w 10
for {set i 0} {$i < $aggSwitchN} {incr i} {
  set k 1
  for {set j 0} {$j < $accSwitchN} {incr j} {
    $aggSwitch($i) link-switch-to-port $accSwitch($j) $k $w
    #puts "port $k of aggSwitch($i) connected to port $w of accSwitch($j)" 
    incr k
  }
  incr w -1
}

# Links between Access switch and Servers
for {set i 0} {$i < $rackN} {incr i} {
  set switchList [$rack($i) get-switch-list]
  set serverList [$rack($i) get-server-list]
  set nSwt [llength $switchList]
  set nSrv [llength $serverList]
  # For each rack switch
  for {set j 0} {$j < $nSwt} {incr j} {
    set torSwitch [lindex $switchList $j]
    set w 1
    # For each rack server
    for {set k 0} {$k < $nSrv} {incr k} {
      set srv [lindex $serverList $k]
      $torSwitch link-pm-to-port $srv $w
      #puts "$srv connected to port $w of $torSwitch"
      incr w
    }
  }
}

puts "DCN CREATION COMPLETE"

#####################################################################
############################ Vm Connections #########################
#####################################################################

#Vm1-Vm21 (Intra-Pm communication)
#set traffic_vm1vm21 [new Application/Traffic/CBR]
#$traffic_vm1vm21 set packetSize_ 512Bytes ;# 4096b
#$traffic_vm1vm21 set rate_ 1Mbps ;# 0.125MBps
#$vm(0) create-connection $vm(20) $traffic_vm1vm21

#Vm1-Vm5 (Intra-Rack communication)
#set traffic_vm1vm5 [new Application/Traffic/CBR]
#$traffic_vm1vm5 set packetSize_ 512Bytes ;# 4096b
#$traffic_vm1vm5 set rate_ 1Mbps ;# 0.125MBps
#$vm(0) create-connection $vm(4) $traffic_vm1vm5

#Vm1-Vm2 (Inter-Rack communication - AggSwitch)
#set traffic_vm1vm2 [new Application/Traffic/CBR]
#$traffic_vm1vm2 set packetSize_ 512Bytes ;# 4096b
#$traffic_vm1vm2 set rate_ 1Mbps ;# 0.125MBps
#$vm(0) create-connection $vm(1) $traffic_vm1vm2

#Vm1-Vm4 (Inter-Rack communication - CoreSwitch)
#set traffic_vm1vm4 [new Application/Traffic/CBR]
#$traffic_vm1vm4 set packetSize_ 512Bytes ;# 4096b
#$traffic_vm1vm4 set rate_ 1Mbps ;# 0.125MBps
#$vm(0) create-connection $vm(3) $traffic_vm1vm4

#Vm1-VmOut (Inter-Dc communication - CoreSwitch)
set traffic_vm1vm0 [new Application/Traffic/CBR]
$traffic_vm1vm0 set packetSize_ 512Bytes ;# 4096b
$traffic_vm1vm0 set rate_ 1Mbps ;# 0.125MBps
$vm(0) create-connection $vmOut $traffic_vm1vm0

#Vm4-VmOut (Inter-Dc communication - CoreSwitch)
set traffic_vm4vm0 [new Application/Traffic/CBR]
$traffic_vm4vm0 set packetSize_ 512Bytes ;# 4096b
$traffic_vm4vm0 set rate_ 1Mbps ;# 0.125MBps
$vm(3) create-connection $vmOut $traffic_vm4vm0

#####################################################################
########### Scheduling of activation and migration events ###########
#####################################################################

# Vms activation and stop
for {set i 0} {$i < $vmN} {incr i} {
  $ns at [expr $sim(start_time)+1] "$vm($i) start"
  $ns at [expr $sim(end_time)+1] "$vm($i) stop"
}

# Start vmOut
$ns at [expr $sim(start_time)+1] "$vmOut start"
$ns at [expr $sim(end_time)+1] "$vmOut stop"

# Migration events
#$ns at 10.0 "$dcManager migrate-vm-precopy $vm1 $servers_cpp(3)"

#####################################################################
#################### Scheduling of fault events #####################
#####################################################################

# Fault port 1 accSwitch(0) at 20s
#$accSwitch(0) link-fault-at 20.0 1
# Reactivation port 1 accSwitch(0) at 40s
#$accSwitch($i)(0) link-reactivation-at 40.0 1

#####################################################################
######################## Utility procedures #########################
#####################################################################

proc record {} {
  
  global ns dcManager rack coreSwitch aggSwitch accSwitch server vm

  #Set the time after which the procedure should be called again
  set time 10 ;#10s
  set now [$ns now]
  puts "Stats at [expr $now-1] seconds:"
  #$dcManager print-rack-list
  #$dcManager print-switch-list
  #$dcManager print-pm-list
  #$dcManager print-vm-list

  #Re-schedule the procedure
  $ns at [expr $now+$time] "record"

}

proc finish {} {

  global ns tracefile namfile dir
  global dcManager rack coreSwitch aggSwitch accSwitch server vm

  set now [$ns now]
  puts "Stats at [expr $now-1] seconds:"
  #$dcManager print-rack-list
  $dcManager print-switch-list
  $dcManager print-pm-list
  #$dcManager print-vm-list
  
  # The Simulator instproc flush-trace will dump the traces on 
  # the respectives files
  #$ns flush-trace
  # Close the trace files previously defined
  close $tracefile
  close $namfile
  # Execute the nam program (unix command) in background 
  # on the trace file "out.nam" for visualization
  #exec nam "$dir(traces)/main.nam" &
  puts "...STOP SIMULATION"
  # End the simulation and return the number 0 as status to the system
  exit 0

}

#####################################################################
##################### Scheduling of main events #####################
#####################################################################

# Show the DC status before start time
#puts "Stats at $sim(start_time) seconds:"
#$dcManager print-rack-list
#$dcManager print-pm-list
#$dcManager print-switch-list
#$dcManager print-vm-list

# Start the statistics recording
#$ns at [expr $sim(start_time)+1] "record"
# Stop the simulation
$ns at [expr $sim(end_time)+2] "finish"
# Start the simulation
puts "START SIMULATION..."
$ns run