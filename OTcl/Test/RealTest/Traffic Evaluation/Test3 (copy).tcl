#source "/home/sabatino/ns-allinone-2.35/ns-2.35/myFiles/OTcl/Test/RealTest/Traffic Evaluation/Test3.tcl"

# Create a new Simulator object
set ns [new Simulator]

# Create the DcManager object
set dcManager [new DcManager]

####################################################################
####################### Simulation parameters ######################
####################################################################

# Simulation start time 
set sim(start_time) 0.0
# Simulation end time  
set sim(end_time) 3600.0 ;# 1h
# Simulation total time
set sim(tot_time) [expr $sim(end_time) - $sim(start_time)]

# Number of Switches, Racks, Servers and Vms
set aggSwitchN 2
set accSwitchN 4
set rackN 4
set serverN 16
set vmN 64

####################################################################
####################### Racks configuration  #######################
####################################################################

# Racks creation
for {set i 0} {$i < $rackN} {incr i} {
  set id [expr $i+1]
  # Defines a Rack which can accomodate 2 switches and 10 servers (12 RU)
  set rack($i) [new Rack $id 2 10]
}

####################################################################
##################### Switches configuration #######################
####################################################################

# Creates a Core Switch of type "SWITCH_10Gbps" (the root of the DCN)
set coreSwitch [new Switch 1 SWITCH_10Gbps]
# Turns on the created Switch
$coreSwitch start

# Creates 2 Aggregation Switches ($aggSwitchN = 2) of type "SWITCH_10Gbps"
for {set i 0} {$i < $aggSwitchN} {incr i} {
  set id [expr $i+2]
  set aggSwitch($i) [new Switch $id SWITCH_10Gbps]
  # Turns on the created Switch
  $aggSwitch($i) start
}

# Create 4 Access Switches ($accSwitchN = 4) of type "SWITCH_1Gbps"
for {set i 0} {$i < $accSwitchN} {incr i} {
  set id [expr $aggSwitchN+$i+2]
  set accSwitch($i) [new ToRSwitch $id SWITCH_1Gbps]
  # Turns on the created Switch
  $accSwitch($i) start
}

# Assigns each Access Switch (ToRSwitch) to the corresponding Rack
for {set i 0} {$i < $accSwitchN} {incr i} {
  set j [expr ($i%$rackN)]
  $rack($j) add-switch $accSwitch($i)
}

####################################################################
###################### Servers configuration #######################
####################################################################

# Physical Servers in Data Center
for {set i 0} {$i < $serverN} {incr i} {
  set id [expr $i+1]
  # Creates a new empty Server (with no internal resources)
  set server($i) [new Pm $id]
  # Specifies the Server physical resources
  $server($i) add-resource [new Cpu 1 CPU_4CORE]
  $server($i) add-resource [new Cpu 2 CPU_4CORE]
  $server($i) add-resource [new Memory 3 MEMORY_8GB]
  $server($i) add-resource [new Storage 4 STORAGE_1TB]
  $server($i) add-resource [new Networking 5 NET_1GbE]
  # Turn on the created Server
  $server($i) set-current-state ON
}

# Specifies the Server to model the inter-DC communications and its physical resources
set serverOut [new Pm 0]
$serverOut add-resource [new Cpu 1 CPU_4CORE]
$serverOut add-resource [new Memory 2 MEMORY_8GB]
$serverOut add-resource [new Storage 3 STORAGE_1TB]
$serverOut add-resource [new Networking 4 NET_1GbE]
# Turn on the created Server
$serverOut set-current-state ON

# Assigns each Server to the corresponding Rack
for {set i 0} {$i < $serverN} {incr i} {
  set j [expr ($i%$rackN)]
  $rack($j) add-pm $server($i)
}

# Checks server allocation
for {set i 0} {$i < $rackN} {incr i} {
  set serverList [$rack($i) get-server-list]
  set nSrv [llength $serverList]
  puts -nonewline "Rack [expr $i+1]: Pm [[lindex $serverList 0] get-id]"
  for {set j 1} {$j < $nSrv} {incr j} {
    set pmj [lindex $serverList $j]
    puts -nonewline ", Pm [$pmj get-id]"
  }
  puts " "
}

####################################################################
###################### VIRTUAL MACHINES CREATION ###################
####################################################################

# Virtual Machines in Data Center
for {set i 0} {$i < $vmN} {incr i} {
  set id [expr $i+1]
  # Creates a new Virtual machine of type NANO
  set vm($i) [new Vm $id NANO]
}
# Creates a new Virtual machine of type LARGE to model the inter-DC communications
set vmOut [new Vm 0 LARGE]

# Assigns each Virtual machine to the corresponding Server
for {set i 0} {$i < $vmN} {incr i} {
  set j [expr ($i%$serverN)]
  $server($j) add-vm $vm($i)
}
$serverOut add-vm $vmOut

# Check the vm allocation
for {set i 0} {$i < $serverN} {incr i} {
  set vmList [$server($i) get-vm-list]
  set nVm [llength $vmList]
  puts -nonewline "Pm [expr $i+1]: Vm [[lindex $vmList 0] get-id]"
  for {set j 1} {$j < $nVm} {incr j} {
    set vmj [lindex $vmList $j]
    puts -nonewline ", Vm [$vmj get-id]"
  }
  puts " "
}

#####################################################################
############################ DCN Topology ###########################
#####################################################################

# Links between Core and Aggregation Switches:

# Connects the port 1 of Switch 1 (coreSwitch) with the port 10 of Switch 2 (aggSwitch(0))
$coreSwitch link-switch-to-port $aggSwitch(0) 1 10
puts "Port 1 of Switch [$coreSwitch get-id] connected to Port 10 of Switch [$aggSwitch(0) get-id]"
# Connects the port 2 of Switch 1 (coreSwitch) with the port 10 of Switch 3 (aggSwitch(1))
$coreSwitch link-switch-to-port $aggSwitch(1) 2 10
puts "Port 2 of Switch [$coreSwitch get-id] connected to Port 10 of Switch [$aggSwitch(1) get-id]"

# Links between Aggregation and Access Switches:

$aggSwitch(0) link-switch-to-port $accSwitch(0) 1 12
puts "Port 1 of Switch [$aggSwitch(0) get-id] connected to Port 12 of ToRSwitch [$accSwitch(0) get-id]"
$aggSwitch(0) link-switch-to-port $accSwitch(1) 2 12
puts "Port 2 of Switch [$aggSwitch(0) get-id] connected to Port 12 of ToRSwitch [$accSwitch(1) get-id]"

$aggSwitch(1) link-switch-to-port $accSwitch(2) 1 12
puts "Port 1 of Switch [$aggSwitch(1) get-id] connected to Port 12 of ToRSwitch [$accSwitch(2) get-id]"
$aggSwitch(1) link-switch-to-port $accSwitch(3) 2 12
puts "Port 2 of Switch [$aggSwitch(1) get-id] connected to Port 12 of ToRSwitch [$accSwitch(3) get-id]"

# Links between Access Switches and Servers:

# For each Rack
for {set i 0} {$i < $rackN} {incr i} {
  # Get a list of all Switches of the selected Rack 
  # (in this case the only Access switch assigned to the rack)
  set switchList [$rack($i) get-switch-list]
  # Get a list of all Servers of the selected Rack
  set serverList [$rack($i) get-server-list]
  # Get the number of Switches and Servers assigned to the selected Rack
  set nSwt [llength $switchList]
  set nSrv [llength $serverList]
  # For each ToR Switch
  for {set j 0} {$j < $nSwt} {incr j} {
    set torSwitch [lindex $switchList $j]
    set w 1
    # Connect each Rack Server to the ToRSwitch
    for {set k 0} {$k < $nSrv} {incr k} {
      set srv [lindex $serverList $k]
      $torSwitch link-pm-to-port $srv $w
      puts "Pm [$srv get-id] connected to port $w of ToRSwitch [$torSwitch get-id]"
      incr w
    }
  }
}

# Link the Physical Server Pm0 to the Core Switch (Port 10)
$coreSwitch link-pm-to-port $serverOut 10
puts "Pm [$serverOut get-id] connected to port 10 of Switch [$coreSwitch get-id]" 


#####################################################################
############################ Vm Connections #########################
#####################################################################

# Creates a connection between Vm1 and Vm5 (Intra-Rack communication)
set traffic_vm1vm5 [new Application/Traffic/CBR]
$traffic_vm1vm5 set packetSize_ 512Bytes ;# 4096b
$traffic_vm1vm5 set rate_ 1Mbps ;# 0.125MBps
$vm(0) create-connection $vm(4) $traffic_vm1vm5

########################################################################
########### Scheduling of de/activation and migration events ###########
########################################################################

# Creates and schedules an "activation event" and a "stop event" for each vm
for {set i 0} {$i < $vmN} {incr i} {
  $ns at [expr $sim(start_time)+1] "$vm($i) start"
  $ns at [expr $sim(end_time)+1] "$vm($i) stop"
}
$ns at [expr $sim(start_time)+1] "$vmOut start"
$ns at [expr $sim(end_time)+1] "$vmOut stop"

# Creates and schedules a new "migration event" for Vm1 (vm(0)): 
# The virtual machine will be transferred on Pm13 (server(12)) at t=1801s
$ns at 1801 "$dcManager migrate-vm-precopy $vm(0) $server(12)"
#$ns at 1801 "$dcManager migrate-vm-postcopy $vm(0) $server(12)"

#####################################################################
#################### Scheduling of fault events #####################
#####################################################################

# Creates and schedules a new "fault event":
# Link 1-2 (Port 1, coreSwitch) at t = 1801s
#$coreSwitch link-fault-at 1801 1

#####################################################################
######################## Utility procedures #########################
#####################################################################

proc finish {} {

  global ns tracefile namfile dir
  global dcManager

  set now [$ns now]
  puts "Stats at [expr $now-1] seconds:"
  $dcManager print-rack-list
  $dcManager print-switch-list
  $dcManager print-pm-list
  $dcManager print-vm-list
  
  puts "...STOP SIMULATION"
  # End the simulation and return the number 0 as status to the system
  exit 0

}

#####################################################################
##################### Scheduling of main events #####################
#####################################################################

# Stop the simulation
$ns at [expr $sim(end_time)+2] "finish"
# Start the simulation
puts "START SIMULATION..."
$ns run