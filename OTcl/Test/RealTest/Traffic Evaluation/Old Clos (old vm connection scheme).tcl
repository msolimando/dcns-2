#source "/home/sabatino/ns-allinone-2.35/ns-2.35/myFiles/OTcl/Test/RealTest/Traffic Evaluation/Clos.tcl"
  
# Create a new Simulator object
set ns [new Simulator]

# Create the DcManager object
set dcManager [new DcManager]

####################################################################
####################### Simulation parameters ######################
####################################################################

# In case you want to define a trace file
set dir(traces) "/home/sabatino/Desktop/Traces"
set tracefile [open "$dir(traces)/main.tr" w]
set testfile [open "$dir(traces)/testOutput.tr" w]
#$ns trace-all $tracefile
set namfile [open "$dir(traces)/main.nam" w]
#$ns namtrace-all $namfile

# Simulation start time 
set sim(start_time) 0.0
# Simulation end time  
set sim(end_time) 3600.0 ; # 1h
# Simulation total time
set sim(tot_time) [expr $sim(end_time) - $sim(start_time)]
puts "\nSimulation time: $sim(tot_time)"

# Routing protocol (Link State)
$ns rtproto LS

# Multipath routing
Node set multiPath_ 1

# Switches, racks, servers and vms number
set coreSwitchN 4
set aggSwitchN 8
set accSwitchN 16
set rackN $accSwitchN
set serverN 128
set vmN 1024

####################################################################
########################## RACKS CREATION ##########################
####################################################################

#puts "DC RACKS:"
# Create racks
for {set i 0} {$i < $rackN} {incr i} {
  set rack($i) [new Rack [expr $i+1] 2 10]
  #puts "Rack [$rack($i) get-id] created"
  #$rack($i) print ;puts ""
}
puts "RACKS CREATION COMPLETE"

####################################################################
######################## SWITCHES CREATION #########################
####################################################################

#puts "CORE SWITCHES:"
# Create core switches
for {set i 0} {$i < $coreSwitchN} {incr i} {
  set coreSwitch($i) [new Switch [expr $i+1] SWITCH_10Gbps]
  # Turn on core switch
  $coreSwitch($i) start
  #puts "Switch [$coreSwitch($i) get-id] created and started"
  #$coreSwitch($i) print ;puts ""
  #set coreSwitch_Node($i) [$coreSwitch($i) get-node]
  #$coreSwitch_Node($i) color Red
  #$coreSwitch_Node($i) label "CoreSwitch $i" 
}

#puts "AGGREGATION SWITCHES:"
# Create aggregation switches
for {set i 0} {$i < $aggSwitchN} {incr i} {
  set id [expr $coreSwitchN+$i+1]
  set aggSwitch($i) [new Switch $id SWITCH_10Gbps]
  # Turn on aggregation switch
  $aggSwitch($i) start
  #puts "Switch [$aggSwitch($i) get-id] created and started"
  #$aggSwitch($i) print ;puts ""
  #set aggSwitch_Node($i) [$aggSwitch($i) get-node]
  #$aggSwitch_Node($i) color Blue
  #$aggSwitch_Node($i) label "AggSwitch $i" 
}

#puts "ACCESS SWITCHES:"
# Create access switches
for {set i 0} {$i < $accSwitchN} {incr i} {
  set id [expr $coreSwitchN+$aggSwitchN+$i+1]
  set accSwitch($i) [new ToRSwitch $id SWITCH_1Gbps]
  # Turn on access switch
  $accSwitch($i) start
  #puts "ToRSwitch [$accSwitch($i) get-id] created and started"
  #$accSwitch($i) print ;puts ""
  #set accSwitch_Node($i) [$accSwitch($i) get-node]
  #$accSwitch_Node($i) color Green
  #$accSwitch_Node($i) label "AccSwitch $i"

}
puts "SWITCHES CREATION COMPLETE"

# Assign each access switch to the corresponding rack
for {set i 0} {$i < $accSwitchN} {incr i} {
  set j [expr ($i%$rackN)]
  $rack($j) add-switch $accSwitch($i)
  #puts "ToRSwitch [$accSwitch($i) get-id] assigned to Rack [$rack($j) get-id]"
}

puts "SWITCHES ASSIGNED TO RACKS"

####################################################################
######################### SERVERS CREATION #########################
####################################################################

#puts "DC SERVERS:"
# Create servers
for {set i 0} {$i < $serverN} {incr i} {
  set server($i) [new Pm [expr $i+1]]
  $server($i) add-resource [new Cpu 1 CPU_4CORE]
  $server($i) add-resource [new Cpu 2 CPU_4CORE]
  $server($i) add-resource [new Memory 3 MEMORY_8GB]
  $server($i) add-resource [new Storage 4 STORAGE_1TB]
  $server($i) add-resource [new Networking 5 NET_1GbE]
  # Turn on server
  $server($i) set-current-state ON
  #puts "Pm [$server($i) get-id] created and started"
  #$server($i) print ;puts "" 
  #set server_Node($i) [$server($i) get-node]
  #$server_Node($i) color Black
  #$server_Node($i) label "Server $i"
}

# Server 0 (Inter-DC communications)
set serverOut [new Pm 0]
$serverOut add-resource [new Cpu 1 CPU_4CORE]
$serverOut add-resource [new Memory 2 MEMORY_8GB]
$serverOut add-resource [new Storage 3 STORAGE_1TB]
$serverOut add-resource [new Networking 4 NET_1GbE]
$serverOut set-current-state ON
#puts "Pm [$serverOut get-id] created and started"
#$serverOut print ;puts "" 
#set serverOut_Node [$serverOut get-node]
#$serverOut_Node color Black
#$serverOut_Node label "Out"

puts "SERVERS CREATION COMPLETE"

# Add servers to racks
for {set i 0} {$i < $serverN} {incr i} {
  set j [expr ($i%$rackN)]
  $rack($j) add-pm $server($i)
  #puts "Pm [$server($i) get-id] assigned to Rack [$rack($j) get-id]"
}

# Check server allocation
for {set i 0} {$i < $rackN} {incr i} {
  set serverList [$rack($i) get-server-list]
  set nSrv [llength $serverList]
  puts -nonewline "Rack [expr $i+1]: Pm [[lindex $serverList 0] get-id]"
  for {set j 1} {$j < $nSrv} {incr j} {
    set pmj [lindex $serverList $j]
    puts -nonewline ", Pm [$pmj get-id]"
  }
  puts " "
}

puts "SERVERS ASSIGNED TO RACKS"

####################################################################
###################### VIRTUAL MACHINES CREATION ###################
####################################################################

#puts "VIRTUAL MACHINES:"
# Create virtual machines
for {set i 0} {$i < $vmN} {incr i} {
  set vm($i) [new Vm [expr $i+1] NANO]
  #puts "Vm [$vm($i) get-id] created"
  #$vm($i) print ;puts ""
}

# Vm 0 (Inter-DC communications)
set vmOut [new Vm 0 LARGE]
#puts "Vm [$vmOut get-id] created"

puts "VMS CREATION COMPLETE"

# Spread the Vm between the servers
for {set i 0} {$i < $vmN} {incr i} {
  set j [expr ($i%$serverN)]
  $server($j) add-vm $vm($i)
  #puts "Vm [$vm($i) get-id] added to Pm [$server($j) get-id]"
}

# Add vmOut on serverOut
$serverOut add-vm $vmOut
#puts "Vm [$vmOut get-id] added to Pm [$serverOut get-id]"

# Check the vm allocation
for {set i 0} {$i < $serverN} {incr i} {
  set vmList [$server($i) get-vm-list]
  set nVm [llength $vmList]
  puts -nonewline "Pm [expr $i+1]: Vm [[lindex $vmList 0] get-id]"
  for {set j 1} {$j < $nVm} {incr j} {
    set vmj [lindex $vmList $j]
    puts -nonewline ", Vm [$vmj get-id]"
  }
  puts " "
}

#####################################################################
############################ DCN Topology ###########################
#####################################################################

puts "DCN TOPOLOGY:"
# Links between Core and Aggregation switches: 
# each aggregation switch is connected with all the core switches
set w 10
for {set i 0} {$i < $coreSwitchN} {incr i} {
  set k 1
  for {set j 0} {$j < $aggSwitchN} {incr j} {
    $coreSwitch($i) link-switch-to-port $aggSwitch($j) $k $w
    puts "Port $k of Switch [$coreSwitch($i) get-id] connected to Port $w of Switch [$aggSwitch($j) get-id]"
    incr k
  }
  incr w -1
}

# Links between Aggregation and Access switches:
set podAgg 2
set podAcc 4
for {set q 0} {$q < [expr $aggSwitchN/$podAgg]} {incr q} {
  set w 12
  for {set i [expr $q*$podAgg]} {$i < [expr ($q+1)*$podAgg]} {incr i} {
    set k 1
    for {set j [expr $q*$podAcc]} {$j < [expr ($q+1)*$podAcc]} {incr j} {
     $aggSwitch($i) link-switch-to-port $accSwitch($j) $k $w
     puts "Port $k of Switch [$aggSwitch($i) get-id] connected to Port $w of ToRSwitch [$accSwitch($j) get-id]"
     incr k
    }
  incr w -1
  }
}

# Links between Access switch and Servers
for {set i 0} {$i < $rackN} {incr i} {
  set switchList [$rack($i) get-switch-list]
  set serverList [$rack($i) get-server-list]
  set nSwt [llength $switchList]
  set nSrv [llength $serverList]
  # For each rack switch
  for {set j 0} {$j < $nSwt} {incr j} {
    set torSwitch [lindex $switchList $j]
    set w 1
    # For each rack server
    for {set k 0} {$k < $nSrv} {incr k} {
      set srv [lindex $serverList $k]
      $torSwitch link-pm-to-port $srv $w
      puts "Pm [$srv get-id] connected to port $w of ToRSwitch [$torSwitch get-id]"
      incr w
    }
  }
}

# Links between serverOut and Core switches
for {set i 0} {$i < $coreSwitchN} {incr i} {
  $coreSwitch($i) link-pm-to-port $serverOut 10
  puts "Pm [$serverOut get-id] connected to port 10 of Switch [$coreSwitch($i) get-id]"
}

puts "DCN CREATION COMPLETE"

#####################################################################
############################ Vm Connections #########################
#####################################################################

puts "VM CONNECTIONS:"
# The list of virtual machines for which is defined a connection 
set vmToStart { }

# IntraDC-traffic
set k 0
# For each data center rack
for {set i 0} {$i < $rackN} {incr i} {
  puts "Rack [expr $i+1]"
  # Get the list of rack servers
  set serverListSource [$rack($i) get-server-list]
  # Get the number of rack servers
  set nSrv [llength $serverListSource]
  # Select one of the rack servers
  set serverSource [lindex $serverListSource [expr ($i%$nSrv)]]
  puts "Server [$serverSource get-id]"
  # Get the list of server virtual machines
  set vmListSource [$serverSource get-vm-list]
  # Get the number of rack servers
  set nVm [llength $vmListSource]
  # Select one of the server virtual machines
  set vmSource [lindex $vmListSource [expr ($i%$nVm)]]
  # Add the selected Vm to vmToStart
  lappend vmToStart $vmSource
  puts "Vm [$vmSource get-id]:"

  # For each data center rack
  for {set j 0} {$j < $rackN} {incr j} {
    # Get the list of rack servers
    set serverListSink [$rack($j) get-server-list]
    # Select the last rack server
    set serverSink [lindex $serverListSink end]
    # Get the list of server virtual machines
    set vmListSink [$serverSink get-vm-list]
    # Select the last server virtual machine
    set vmSink [lindex $vmListSink end]
    #
    if {[$vmSource get-id] == [$vmSink get-id]} {
      set vmSink [lindex $vmListSink [expr [llength $vmListSink]-2]]
    }
    # Add the selected Vm to vmToStart
    lappend vmToStart $vmSink
    set connections($k) [new Application/Traffic/Pareto] 
    $connections($k) set packetSize_ 512Bytes ;# 4096b
    $connections($k) set burst_time_ 1s
    $connections($k) set idle_time_ 5s
    $connections($k) set rate_ 1Mbps ;# 0.125MBps
    $vmSource create-connection $vmSink $connections($k)
    puts -nonewline "Connection between Vm [$vmSource get-id] (Pm [$serverSource get-id], Rack [expr $i+1]) " 
    puts "and Vm [$vmSink get-id] (Pm [$serverSink get-id], Rack [expr $j+1]) created"
    incr k
  }
}

# In-Out traffic
# each Vm is connected to virtual machines Vm0
#for {set i 0} {$i < $vmN} {incr i} {
#  set connections($i) [new Application/Traffic/Pareto] 
#  $connectionsOut($i) set packetSize_ 512Bytes ;# 4096b
#  $connectionsOut($i) set burst_time_ 1s
#  $connectionsOut($i) set idle_time_ 5s
#  $connectionsOut($i) set rate_ 1Mbps ;# 0.125MBps
#  $vm($i) create-connection $vmOut $connections($i)
#  puts "Connection between vm($i) and vmOut created"
#  }
#}

# Out-In traffic
# the virtual machine Vm0 is connected to each vm
#for {set i 0} {$i < $vmN} {incr i} {
#  set connections($i) [new Application/Traffic/Pareto] 
#  $connectionsOut($i) set packetSize_ 512Bytes ;# 4096b
#  $connectionsOut($i) set burst_time_ 1s
#  $connectionsOut($i) set idle_time_ 5s
#  $connectionsOut($i) set rate_ 1Mbps ;# 0.125MBps
#  $vmOut create-connection $vm($i) $connections($i)
#  puts "Connection between vmOut and vm($i) created"
#  }
#}

puts "VMS CONNECTION COMPLETED ($k CONNECTION CREATED)"

exit 0

#####################################################################
########### Scheduling of activation and migration events ###########
#####################################################################

#Remove duplicates from vmToStart list
set vmToStartNoDup [lsort -unique $vmToStart]
# Vms activation and stop
for {set i 0} {$i < [llength $vmToStartNoDup]} {incr i} {
  set vmT [lindex $vmToStartNoDup $i]
  $ns at [expr $sim(start_time)+1] "$vmT start"
  puts -nonewline "Vm [$vmT get-id] starts at t = [expr $sim(start_time)+1] and "
  $ns at [expr $sim(end_time)+1] "$vmT stop"
  puts "stops at t = [expr $sim(end_time)+1]"
}

# Start and Stop vmOut
$ns at [expr $sim(start_time)+1] "$vmOut start"
puts -nonewline "Vm [$vmOut get-id] starts at t = [expr $sim(start_time)+1] and "
$ns at [expr $sim(end_time)+1] "$vmOut stop"
puts "stops at t = [expr $sim(end_time)+1]"

# Migration events
#$ns at 1801.0 "$dcManager migrate-vm-postcopy $vm(0) $server(6)"

#####################################################################
#################### Scheduling of link fault events ################
#####################################################################

# Fault port 1 coreSwitch at 1801s
#$coreSwitch link-fault-at 1801 1
# Reactivation port 12 accSwitch(0) at 41s
#$accSwitch(0) link-reactivation-at 41.0 12

#####################################################################
######################## Utility procedures #########################
#####################################################################

proc record {} {
  
  global ns dcManager rack coreSwitch aggSwitch accSwitch server vm
  global testfile

  #Set the time after which the procedure should be called again
  set time 10 ;#10s
  set now [$ns now]
  puts "Stats at [expr $now-1] seconds:"
  #$dcManager print-rack-list
  #$dcManager print-switch-list
  #$dcManager print-pm-list
  #$dcManager print-vm-list
  $coreSwitch(0) print

  #
  puts $testfile "[expr $now-1] [$coreSwitch(0) get-rx-linkLoad 1]"

  #Re-schedule the procedure
  $ns at [expr $now+$time] "record"

}

proc finish {} {

  global ns tracefile namfile testfile dir
  global dcManager rack coreSwitch aggSwitch accSwitch server vm

  set now [$ns now]
  puts "Stats at [expr $now-1] seconds:"
  #$dcManager print-rack-list
  $dcManager print-switch-list
  $dcManager print-pm-list
  #$dcManager print-vm-list
  
  # The Simulator instproc flush-trace will dump the traces on 
  # the respectives files
  #$ns flush-trace

  puts $testfile "[expr $now-1] [$coreSwitch(0) get-rx-linkLoad 1]"
  puts $testfile "$now 0.000000"

  # Close the trace files previously defined
  close $tracefile
  close $namfile
  close $testfile

  # Execute the nam program (unix command) in background 
  # on the trace file "out.nam" for visualization
  #exec nam "$dir(traces)/main.nam" &
  
  puts "...STOP SIMULATION"
  # End the simulation and return the number 0 as status to the system
  exit 0

}

#####################################################################
##################### Scheduling of main events #####################
#####################################################################

# Show the DC status before start time
#puts "Stats at $sim(start_time) seconds:"
#$dcManager print-rack-list
#$dcManager print-pm-list
#$dcManager print-switch-list
#$dcManager print-vm-list

# Start the statistics recording
$ns at [expr $sim(start_time)+2] "record"
# Stop the simulation
$ns at [expr $sim(end_time)+2] "finish"
# Start the simulation
puts "START SIMULATION..."
$ns run