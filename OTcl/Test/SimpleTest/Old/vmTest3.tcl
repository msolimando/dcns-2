#source "/home/sabatino/ns-allinone-2.35/ns-2.35/myFiles/OTcl/Test/vmTest3.tcl"

############################################################
############################################################

# Create a new Simulator object

set ns [new Simulator]

# Set the chosen source directory

set dir(sources) "/home/sabatino/ns-allinone-2.35/ns-2.35/myFiles/OTcl/"

# Set the trace directory 

set dir(traces) "/home/sabatino/Desktop/Traces"

# Create the trace file "main.tr" and attach it to the Tcl file channel variable "tracefile"

set tracefile [open "$dir(traces)/main.tr" w]

# The instaproc trace-all of class Simulator defines the file where the simulation traces will go

#$ns trace-all $tracefile

# Create a NAM trace file

set namfile [open "$dir(traces)/main.nam" w]

#$ns namtrace-all $namfile

# Set the simulation start time 

set sim(start_time) 0.0

# Set the simulation end time   

set sim(end_time) 60.0

# Compute the simulation total time

set sim(tot_time) [expr $sim(end_time) - $sim(start_time)]

puts "\nTotal simulation time: $sim(tot_time) s"

# ----------------------------------------------------------------------------------
# -------------------------- Start building topology here --------------------------
# ----------------------------------------------------------------------------------

# ======================================================================
# Racks, Switches and hosts
# ======================================================================

# Create the DcManager object
set dcManager [new DcManager]

# Create rack (Rack1 and Rack2)
for {set i 1} {$i < 3} {incr i} {
  set rack_cpp($i) [new Rack $i 2 10]
  #$rack_cpp($i) print ;puts ""
}
puts "RACKS CREATION COMPLETE"

# Create aggregation switch (Agg. Switch1)
set switchL2_cpp [new Switch 1 SWITCH_10Gbps]
$switchL2_cpp start

#set switchL2_tcl [$switchL2_cpp get-node]
#$switchL2_tcl color Green
#$switchL2_tcl label "AggSwitch 1"

# Create access switches (ToR Switch1 and Tor Switch2)
for {set i 1} {$i < 3} {incr i} {
  set switchL1_cpp($i) [new ToRSwitch $i SWITCH_1Gbps]
  set switchL1_tcl($i) [$switchL1_cpp($i) get-node]
  $switchL1_cpp($i) start

  #$switchL1_tcl($i) color Red
  #$switchL1_tcl($i) label "ToRSwitch $i"
  #$switchL1_cpp($i) print ;puts ""
}
puts "SWITCHES CREATION COMPLETE"

# Create hosts (Server 1,2,3,4)
for {set i 1} {$i < 5} {incr i} {
  set servers_cpp($i) [new Pm $i]
  $servers_cpp($i) add-resource [new Cpu 1 CPU_4CORE]
  $servers_cpp($i) add-resource [new Cpu 2 CPU_4CORE]
  $servers_cpp($i) add-resource [new Memory 3 MEMORY_8GB]
  $servers_cpp($i) add-resource [new Storage 4 STORAGE_1TB]
  $servers_cpp($i) add-resource [new Networking 5 NET_1GbE]
  $servers_cpp($i) set-current-state ON
  #$servers_cpp($i) print ;puts ""
  
  #set servers_tcl($i) [$servers_cpp($i) get-node]
  #$servers_tcl($i) color Black
  #$servers_tcl($i) label "Server $i"
}
puts "HOSTS CREATION COMPLETE"

# Add pms and switches to racks
$rack_cpp(1) add-pm $servers_cpp(1)
$rack_cpp(1) add-pm $servers_cpp(2)
$rack_cpp(1) add-pm $servers_cpp(3)
$rack_cpp(1) add-switch $switchL1_cpp(1)

$rack_cpp(2) add-pm $servers_cpp(4)
$rack_cpp(2) add-switch $switchL1_cpp(2)

puts "HOSTS AND SWITCHES ADDED TO RACKS"

$rack_cpp(1) print ;puts ""
$rack_cpp(2) print ;puts ""

# ======================================================================
# Links between switches and hosts
# ======================================================================

# Links between ACCESS and AGGREGATION switches
$switchL2_cpp link-switch-to-port $switchL1_cpp(1) 1 12
$switchL2_cpp link-switch-to-port $switchL1_cpp(2) 2 12

# Links between HOSTS and ACCESS SWITCH 1
$switchL1_cpp(1) link-pm-to-port $servers_cpp(1) 1
$switchL1_cpp(1) link-pm-to-port $servers_cpp(2) 2
$switchL1_cpp(1) link-pm-to-port $servers_cpp(3) 3

# Link between HOSTS and ACCESS SWITCH 1
$switchL1_cpp(2) link-pm-to-port $servers_cpp(4) 1

puts "LINKS CREATION COMPLETE"

# ======================================================================
# Vms and apps
# ======================================================================

set vm1 [new Vm 1 MEDIUM]
set vm2 [new Vm 2 MEDIUM]
set vm3 [new Vm 3 LARGE]
set vm4 [new Vm 4 LARGE]

puts "VMS CREATION COMPLETE"

# Add vm to servers
$servers_cpp(1) add-vm $vm1
$servers_cpp(2) add-vm $vm2
$servers_cpp(3) add-vm $vm3
$servers_cpp(4) add-vm $vm4

puts "VMS ADDED TO PMS"

$dcManager print-pm-list

#$vm1 print; puts ""
#$vm2 print; puts ""
#$vm3 print; puts ""
#$vm4 print; puts ""

# ======================================================================

#Create the traffic generator and sets configuration parameters
set traffic_vm1vm2 [new Application/Traffic/CBR]
$traffic_vm1vm2 set packetSize_ 512Bytes ;# 4096b
$traffic_vm1vm2 set rate_ 100Mbps ; #12,5MBps
# Link the traffic generator with the vm
$vm1 create-connection $vm2 $traffic_vm1vm2

#Create the traffic generator and sets configuration parameters
set traffic_vm3vm2 [new Application/Traffic/CBR]
$traffic_vm3vm2 set packetSize_ 512Bytes ;# 4096b
$traffic_vm3vm2 set rate_ 10Mbps ;# 1,25MBps
#Link the traffic generator with the vm2
$vm3 create-connection $vm2 $traffic_vm3vm2

# Vm1(60s) = 750MB, Vm3(60s) = 75MB --> Vm2(60s) = 825MB <-- !!!! 

# ======================================================================
# ======================================================================

proc record {} {
  
  global ns dcManager vm1 vm2 vm3 vm4 switchL2_cpp switchL1_cpp

  #Set the time after which the procedure should be called again
  set time 10 ;#10s
  
  set now [$ns now]
  puts "Stats at [expr $now-1] seconds:"
  $switchL1_cpp(1) print ;puts ""
  #$switchL2_cpp print ;puts ""
  #$switchL1_cpp(2) print ;puts ""
  
  #set bytesRecVM1 [[$vm1 get-sink-agent] get-bytesRx] 
  #puts "Vm1: Bytes received = $bytesRecVM1"
  #set bytesRecVM2 [[$vm2 get-sink-agent] get-bytesRx] 
  #puts "Vm2: Bytes received = $bytesRecVM2"
  #set bytesRecVM3 [[$vm3 get-sink-agent] get-bytesRx] 
  #puts "Vm3: Bytes received = $bytesRecVM3"
  #set bytesRecVM4 [[$vm4 get-sink-agent] get-bytesRx] 
  #puts "Vm4: Bytes received = $bytesRecVM4"
  
  #$dcManager print-pm-list

  #Re-schedule the procedure
  $ns at [expr $now+$time] "record"

}

proc finish {} {

  global ns dcManager vm1 vm2 vm3 vm4 switchL2_cpp switchL1_cpp tracefile namfile dir

  set now [$ns now]
  puts "Stats at [expr $now-1] seconds:"
  $switchL1_cpp(1) print ;puts ""
  #$switchL2_cpp print ;puts ""
  #$switchL1_cpp(2) print ;puts ""
  
  #set bytesRecVM1 [[$vm1 get-sink-agent] get-bytesRx] 
  #puts "Vm1: Bytes received = $bytesRecVM1"
  #set bytesRecVM2 [[$vm2 get-sink-agent] get-bytesRx] 
  #puts "Vm2: Bytes received = $bytesRecVM2"
  #set bytesRecVM3 [[$vm3 get-sink-agent] get-bytesRx] 
  #puts "Vm3: Bytes received = $bytesRecVM3"
  #set bytesRecVM4 [[$vm4 get-sink-agent] get-bytesRx] 
  #puts "Vm4: Bytes received = $bytesRecVM4"

  $dcManager print-pm-list
  #$dcManager print-vm-list
  
  # The Simulator instproc flush-trace will dump the traces on 
  # the respectives files
  #$ns flush-trace
  
  # Close the trace files previously defined
  close $tracefile
  close $namfile
  
  # Execute the nam program (unix command) in background 
  # on the trace file "out.nam" for visualization
  #exec nam "$dir(traces)/main.nam" &
  
  puts "NS EXITING..."

  # End the application and return the number 0 as status to the system
  exit 0

}

# ======================================================================
# Scheduling of the main events

$ns at 1.0 "record"
$ns at 1.0 "$dcManager print-pm-list"
$ns at 1.0 "$dcManager print-vm-list"

$ns at $sim(start_time) "$vm1 start"
$ns at $sim(start_time) "$vm2 start"
$ns at $sim(start_time) "$vm3 start"
$ns at $sim(start_time) "$vm4 start"
$ns at $sim(end_time) "$vm1 stop"
$ns at $sim(end_time) "$vm2 stop"
$ns at $sim(end_time) "$vm3 stop"
$ns at $sim(end_time) "$vm4 stop"

#$ns at 10.0 "puts \"START MIGRATION 1\""
#$ns at 10.0 "$dcManager migrate-vm $vm1 $servers_cpp(2)"

#$ns at 10.0 "puts \"START MIGRATION 1\""
#$ns at 10.0 "$dcManager migrate-vm $vm1 $servers_cpp(3)"

$ns at [expr $sim(end_time)+1] "finish"

puts "Starting simulation..."
$ns run