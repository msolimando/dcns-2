/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#include "dcmanager.h"

DcManager* DcManager::dcM_instance = NULL;

static class DcManagerClass : public TclClass {
public:
  DcManagerClass() : TclClass("DcManager") {}
	TclObject* create(int argc, const char*const*argv) {
	  //OTcl domain: set newDcManager [new DcManager]
		return DcManager::Instance();
	}
} class_dcmanager;

////////////

DcManager* DcManager::Instance(){

  // Only allow one instance of class to be generated
  if (dcM_instance == NULL)
    dcM_instance = new DcManager();
  return dcM_instance;

}

std::vector<Pm*> DcManager::getPmList(){

  return this->pmList;

}

void DcManager::addPmToPmList(Pm* pm){

  this->pmList.push_back(pm);

}

void DcManager::printPmList(){

  // Print PmList
  std::vector<Pm*>::iterator it;
  std::cout << "==============================================================";
  std::cout << "==============================================================\n Pm List\n";
  std::cout << "==============================================================";
  std::cout << "==============================================================\n";
  for(it = pmList.begin(); it != pmList.end(); it++){
    (*it)->print();
    std::cout << "==============================================================";
    std::cout << "==============================================================\n\n";
  }

}

std::vector<Rack*> DcManager::getRackList(){

  return this->rackList;

}

void DcManager::addRackToRackList(Rack* rack){

  this->rackList.push_back(rack);

}

void DcManager::printRackList(){

  // Print RackList
  std::vector<Rack*>::iterator it;
  std::cout << "|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||";
  std::cout << "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n Rack List\n";
  std::cout << "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||";
  std::cout << "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n";
  for(it = rackList.begin(); it != rackList.end(); it++){
    (*it)->print();
    std::cout << "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||";
    std::cout << "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n\n";
  }

}

std::vector<Switch*> DcManager::getSwitchList(){

  return this->switchList;

}

void DcManager::addSwitchToSwitchList(Switch* sw){

  this->switchList.push_back(sw);

}

void DcManager::printSwitchList(){

  // Print SwitchList
  std::vector<Switch*>::iterator it;
  std::cout << "##############################################################";
  std::cout << "##########################################################\n Switch List\n";
  std::cout << "##############################################################";
  std::cout << "##############################################################\n";
  for(it = switchList.begin(); it != switchList.end(); it++){
    (*it)->print();
    std::cout << "##############################################################";
    std::cout << "##############################################################\n\n";
  }

}

std::vector<Vm*> DcManager::getVmList(){

  return this->vmList;

}

void DcManager::addVmToVmList(Vm* vm){

  this->vmList.push_back(vm);

}

void DcManager::printVmList(){

  // Print VmList
  std::vector<Vm*>::iterator it;
  std::cout << "--------------------------------------------------------------";
  std::cout << "--------------------------------------------------------------\n Vm List\n";
  std::cout << "--------------------------------------------------------------";
  std::cout << "--------------------------------------------------------------\n";
  for(it = vmList.begin(); it != vmList.end(); it++){
    (*it)->print();
    std::cout << "--------------------------------------------------------------";
    std::cout << "--------------------------------------------------------------\n\n";
  }

}

bool DcManager::migrateVm(Vm* vm, Pm* newHost, bool preCopy){

  // Check if the vm is actually assigned to any server
  if(vm->getHostPmId() == -1){
    std::cout << "The vm " << vm->getId() << " is not assigned to any server; "
        "if you want to add the vm to server "<< newHost->getId() << " use the instproc "
        "add-vm of class Pm\n";
    return false;
  }
  // If newHost is equals to the current vm host and the vm is not migrating
  if((vm->getHostPmId() == newHost->getId()) && (vm->getMigrationState() == false)){
    std::cout << "The vm " << vm->getId() << "is already assigned to server " << newHost->getId()
          <<"; if you want to migrate the vm from this server select a different host\n";
    return false;
  }
  // Invoke the migrateVm procedure of the vm host
  if(vm->getVmMonitor()->startMigration(vm, newHost, preCopy) == true)
    return true;
  // Migration Failed
  else{
    std::cout << "\n********************************************************************\n"
        "Migration of Vm " << vm->getId() << " on Pm " << newHost->getId() <<
        " failed at t = " << Scheduler::instance().clock() <<
        "s\n********************************************************************\n\n";
    return false;
  }

}

/** OTcl commands:
 * commands-list {}
 * print-pm-list {} -
 * print-rack-list {} -
 * print-switch-list {} -
 * print-vm-list {} -
 * migrate-vm-postcopy {Vm* Pm*} -
 * migrate-vm-precopy {Vm* Pm*} -
 **/
int DcManager::command(int argc, const char*const* argv){

  //
  if(argc == 2) {
    if(strcmp(argv[1], "commands-list") == 0) {
      std::cout << "- commands-list {}\n"
                   "- print-pm-list {}\n"
                   "- print-rack-list {}\n"
                   "- print-switch-list {}\n"
                   "- print-vm-list {}\n"
                   "- migrate-vm-postcopy {Vm* Pm*}\n"
                   "- migrate-vm-precopy {Vm* Pm*}\n";
      return (TCL_OK);
    } else if (strcmp(argv[1], "print-pm-list") == 0) {
      printPmList();
      return (TCL_OK);
    } else if (strcmp(argv[1], "print-rack-list") == 0) {
      printRackList();
      return (TCL_OK);
    } else if (strcmp(argv[1], "print-switch-list") == 0) {
      printSwitchList();
      return (TCL_OK);
    } else if (strcmp(argv[1], "print-vm-list") == 0) {
      printVmList();
      return (TCL_OK);
    }
  //
  } else if(argc == 4) {
    //
    if(strcmp(argv[1], "migrate-vm-postcopy") == 0) {
      Vm* vm = (Vm*) (TclObject::lookup(argv[2]));
      Pm* pm = (Pm*) (TclObject::lookup(argv[3]));
      if((vm != 0) && (pm != 0)){
        migrateVm(vm, pm, false);
        return (TCL_OK);
      }
      return (TCL_ERROR);
    //
    }else if(strcmp(argv[1], "migrate-vm-precopy") == 0) {
      Vm* vm = (Vm*) (TclObject::lookup(argv[2]));
      Pm* pm = (Pm*) (TclObject::lookup(argv[3]));
      if((vm != 0) && (pm != 0)){
        migrateVm(vm, pm, true);
        return (TCL_OK);
      }
      return (TCL_ERROR);
    }
  }// end if (argc == 4)

  return (TCL_ERROR);

}

