/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PRESOURCEUNIT_H_
#define PRESOURCEUNIT_H_

#include <iostream>

#include "resourceunit.h"
#include "util.h"

/** */
class PResourceUnit : public ResourceUnit {

public:

  /** Class constructor */
  PResourceUnit(int id, double nomValue);

  /** Class destructor */
  ~PResourceUnit();

  /** Prints the virtual resource unit characteristics*/
  void print(util::format_type fType) override;

};

#endif /* PRESOURCEUNIT_H_ */
