/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#include "vmemory.h"

/** */
static class VMemoryClass : public TclClass {
public:
	VMemoryClass() : TclClass("VMemory") {}
	TclObject* create(int argc, const char*const* argv) {
	  return (new VMemory(atoi(argv[4]), argv[5]));
	}
} class_vmemory;

std::map<std::string, VMemory::vmem_type> VMemory::vmemTypeCheck = {
   {"MEMORY_1GB", VMemory::MEMORY_1GB},
   {"MEMORY_2GB", VMemory::MEMORY_2GB},
   {"MEMORY_4GB", VMemory::MEMORY_4GB},
   {"MEMORY_8GB", VMemory::MEMORY_8GB},
};

////////////

VMemory::VMemory(int vmemId, std::string vmemType) {

  this->resId = vmemId;
  this->resType = Resource::MEMORY;
  this->vmemType = stringToVMemType(vmemType);
  this->nominalCap = 0;
  this->hostId = -1;
  //
  switch(this->vmemType){
  case MEMORY_1GB:{
    this->addResUnit(new VResourceUnit(1, 1000000000)); //1GB
    break;
  }
  case MEMORY_2GB:{
    this->addResUnit(new VResourceUnit(1, 1000000000)); //1GB
    this->addResUnit(new VResourceUnit(2, 1000000000)); //1GB
    break;
  }
  case MEMORY_4GB:{
    this->addResUnit(new VResourceUnit(1, 2000000000)); //2GB
    this->addResUnit(new VResourceUnit(2, 2000000000)); //2GB
    break;
  }
  case MEMORY_8GB:{
    this->addResUnit(new VResourceUnit(1, 4000000000)); //4GB
    this->addResUnit(new VResourceUnit(2, 4000000000)); //4GB
    break;
  }
  default:
    //throw std::runtime_error("The chosen virtual memory type is not defined\n");
    break;
  }
  //
  this->nomCapToUse = this->getNominalCapacity();
}

VMemory::~VMemory(){

}

VMemory::vmem_type VMemory::getVMemType(){

  return this->vmemType;

}

void VMemory::print(){

  //
  std::string nCap = getFormattedString(getNominalCapacity(), util::BYTE);
  std::string aCap = getFormattedString(getAvailableCapacity(), util::BYTE);
  std::string uCap = getFormattedString(getUsedCapacity(), util::BYTE);
  //
  double nCapToUse = (getNominalCapacityToUse()/getNominalCapacity())*100;

  std::cout << "VRes " << getId() << ": ";
	std::cout << "Type = " << Resource::resTypeToString(getResType());
	std::cout << " [" << vmemTypeToString(getVMemType()) << "], ";
	std::cout << "Vm_Id " << getHostId() << ", ";
	std::cout << "nomCap = " << nCap << " (" << nCapToUse << "%), ";
	std::cout << "usedCap = " << uCap  << ", ";
	std::cout << "availCap = " << aCap << "\n";

	std::vector <ResourceUnit*>::iterator iter;
	for(iter=resUnitList.begin(); iter!=resUnitList.end(); iter++)
			(*iter)->print(util::BYTE);

}

/** OTcl commands:
 * print -
 * get-available-cap -
 * update-id NEWID -
 **/
int VMemory::command(int argc, const char*const* argv) {

  Tcl& tcl = Tcl::instance();
  //
  if(argc == 2) {
    //
    if(strcmp(argv[1], "print") == 0) {
      this->print();
      return (TCL_OK);
    //
    } else if (strcmp(argv[1], "get-available-cap") == 0) {
       double result = 0;
       result= getAvailableCapacity();
       char out[100];
       sprintf(out, "set vmem_Acap %.0f", result);
       tcl.eval(out);
       return (TCL_OK);
    }
  //
  } else if(argc == 3){
    //
    if(strcmp(argv[1], "update-id") == 0) {
      int newId = atoi(argv[2]);
      if(this->updateId(newId) == false){
        std::cerr << "A virtual resource can change its id only if it's not attached to any vm\n";
        return (TCL_ERROR);
      }
      return (TCL_OK);
    }
  }
  return (TCL_ERROR);

}

VMemory::vmem_type VMemory::stringToVMemType(std::string type){

  std::map<std::string, VMemory::vmem_type>::iterator it = vmemTypeCheck.find(type);
  if (it == vmemTypeCheck.end())
    throw std::runtime_error("The chosen virtual memory type is not defined\n");
  return it->second;

}

std::string VMemory::vmemTypeToString(VMemory::vmem_type type){

  std::map<std::string, VMemory::vmem_type>::iterator it;
  for(it = vmemTypeCheck.begin(); it!= vmemTypeCheck.end(); it++)
    if (it->second == type)
      return it->first;
  throw std::runtime_error("The chosen virtual memory type is not defined\n");

}
