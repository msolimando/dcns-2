/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#include "ullqueuemonitor.h"

#define MAXINT 2147483647

/** */
static class ULLQueueMonitorClass : public TclClass {
public:
  ULLQueueMonitorClass(): TclClass("QueueMonitor/ULLQueueMonitor") {}
  TclObject* create(int argc, const char*const* argv) {
    //OTcl domain: set newULLQueueMonitor [new QueueMonitor/ULLQueueMonitor]
    return (new ULLQueueMonitor());
  }
} class_ullqueuemonitor;

////////////

ULLQueueMonitor::ULLQueueMonitor() : QueueMonitor(), ullbarrivals_(0), ullparrivals_(0),
    ullbdepartures_(0), ullpdepartures_(0), ullbdrops_(0), ullpdrops_(0) {

}

ULLQueueMonitor::~ULLQueueMonitor(){

}

unsigned long long ULLQueueMonitor::getBarrivals(){

  return this->ullbarrivals_;

}

unsigned long long ULLQueueMonitor::getParrivals(){

  return this->ullparrivals_;

}

unsigned long long ULLQueueMonitor::getBdepartures(){

  return this->ullbdepartures_;

}

unsigned long long ULLQueueMonitor::getPdepartures(){

  return this->ullpdepartures_;

}

unsigned long long ULLQueueMonitor::getBdrops(){

  return this->ullbdrops_;

}

unsigned long long ULLQueueMonitor::getPdrops(){

  return this->ullpdrops_;

}

void ULLQueueMonitor::in(Packet* p){

  // Retrieve the common packet header and update the number of received bytes
  this->ullbarrivals_ += hdr_cmn::access(p)->size();
  // Update the number of received packets
  this->ullparrivals_++;
  // Call the member function "in" defined in the parent class (QueueMonitor)
  QueueMonitor::in(p);
  // To avoid OVERFLOW
  if(ullbarrivals_ - barrivals_ != 0)
    barrivals_ = MAXINT;
  if(ullparrivals_ - parrivals_ != 0)
    parrivals_ = MAXINT;

}

void ULLQueueMonitor::out(Packet* p){

  // Retrieve the common packet header and update the number of departed bytes
  this->ullbdepartures_ += hdr_cmn::access(p)->size();
  // Update the number of departed packets
  this->ullpdepartures_++;
  // Call the member function "out" defined in the parent class (QueueMonitor)
  QueueMonitor::out(p);
  // To avoid OVERFLOW
  if(ullbdepartures_ - bdepartures_ != 0)
    bdepartures_ = MAXINT;
  if(ullpdepartures_ - pdepartures_ != 0)
    pdepartures_ = MAXINT;

}

void ULLQueueMonitor::drop(Packet* p){

  // Retrieve the common packet header and update the number of dropped bytes
  this->ullbdrops_ += hdr_cmn::access(p)->size();
  // Update the number of dropped packets
  this->ullpdrops_++;
  // Call the member function "drop" defined in the parent class (QueueMonitor)
  QueueMonitor::drop(p);
  // To avoid OVERFLOW
  if(ullbdrops_ - bdrops_ != 0)
    bdrops_ = MAXINT;
  if(ullpdrops_ - pdrops_ != 0)
    pdrops_ = MAXINT;

}

/** OTcl commands:
 * commands-list {} -
 * get-barrivals {}
 * get-parrivals {}
 * get-bdepartures {}
 * get-pdepartures {}
 * get-bdrops {}
 * get-pdrops {}
 **/
int ULLQueueMonitor::command(int argc, const char*const* argv){

  Tcl& tcl = Tcl::instance();
  //
  if(argc == 2) {
    if(strcmp(argv[1], "commands-list") == 0){
      std::cout << "- commands-list {}\n"
                   "- get-barrivals {}\n"
                   "- get-parrivals {}\n"
                   "- get-bdepartures {}\n"
                   "- get-pdepartures {}\n"
                   "- get-bdrops {}\n"
                   "- get-pdrops {}\n";
      return (TCL_OK);
    //
    }else if(strcmp(argv[1], "get-barrivals") == 0) {
      tcl.evalf("set barrivals %llu", getBarrivals());
      return (TCL_OK);
    //
    }else if(strcmp(argv[1], "get-parrivals") == 0) {
      tcl.evalf("set parrivals %llu", getParrivals());
      return (TCL_OK);
    //
    }else if(strcmp(argv[1], "get-bdepartures") == 0) {
      tcl.evalf("set bdepartures %llu", getBdepartures());
      return (TCL_OK);
    //
    }else if(strcmp(argv[1], "get-pdepartures") == 0) {
      tcl.evalf("set pdepartures %llu", getPdepartures());
      return (TCL_OK);
    //
    }else if(strcmp(argv[1], "get-bdrops") == 0) {
      tcl.evalf("set bdrops %llu", getBdrops());
      return (TCL_OK);
    //
    }else if(strcmp(argv[1], "get-pdrops") == 0) {
      tcl.evalf("set pdrops %llu", getPdrops());
      return (TCL_OK);
    }
  }
  return (QueueMonitor::command(argc, argv));

}
