/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PM_H_
#define PM_H_

#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include <node.h>
#include <tclcl.h>

#include "host.h"
#include "presource.h"
#include "ullqueuemonitor.h"
#include "vm.h"
#include "vmmonitor.h"
#include "util.h"

class DcManager;

/** Each object of this class represents a physical host into the network topology */
class Pm : public Host, public TclObject {

  /** */
  friend class VmMonitor;

public:

  // The set of host possible states
	enum pm_state {ON, OFF, SLEEP};

	/** Pm constructor */
	Pm(int pmId);

	/** Pm destructor */
	~Pm();

	/** */
	int getRackId();

	/** */
	void setRackId(int rackId);

	/** */
	pm_state getPmState();

	/** */
	bool setPmState(pm_state newState);

	/** */
	VmMonitor* getVmMonitor();

	/** */
	void setTxMonitor(ULLQueueMonitor* qm);

	/** */
	ULLQueueMonitor* getTxMonitor();

	/** */
	void setRxMonitor(ULLQueueMonitor* qm);

	/** */
	ULLQueueMonitor* getRxMonitor();

	/** */
	std::string getNode_OTcl();

	/** */
	bool addResource(PResource* pres);

	/** */
	bool removeResource(int presId);

	/** */
	unsigned long long getDataSent() override;

	/** */
	unsigned long long getDataReceived() override;

	/** */
	std::vector<Vm*> getHostedVms();

	/** */
	void printNoDetails() override;

	/** */
	void print() override;

	/** */
	int command(int argc, const char*const* argv) override;

	/** */
	static pm_state stringToPmState(std::string type);

	/** */
	static std::string pmStateToString(pm_state state);

private:

	/** The rack in which the host is installed */
	int rackId;

	/** The current host state */
	pm_state pmState;

	/** */
	VmMonitor* hyperV;

	/** */
	ULLQueueMonitor* rxMonitor;

	/** */
	ULLQueueMonitor* txMonitor;

	/** Hosted vm list */
	std::vector<Vm*> hostedVMs;

	/** */
	std::string node_OTcl;

	/** */
	static std::map<std::string, pm_state> pmStateCheck;

};

#endif  /* PM_H_ */
