/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef VM_H_
#define VM_H_

#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include <tclcl.h>

#include "host.h"
#include "util.h"
#include "vcpu.h"
#include "vmemory.h"
#include "vmsourceagent.h"
#include "vmsinkagent.h"
#include "vstorage.h"
#include "vnetworking.h"

class DcManager;
class VmMonitor;

/** Each object of this class represents a virtual machine */
class Vm : public Host, public TclObject {

public:

	// The set of states that a virtual machine can assume
	enum vm_state {RUNNING, INACTIVE, PAUSED, SUSPENDED};

	// The set of virtual machine type
	enum vm_type{NANO, MICRO, SMALL, MEDIUM, LARGE};

	/** Vm constructor */
	Vm(int vmId, std::string vmType);
	
	/** Vm destructor */
	~Vm();
	
	/** */
	VmMonitor* getVmMonitor();

	/** */
	void setVmMonitor(VmMonitor* vmm);

	/** */
	int getHostPmId();

	/** Returns the vm type */
	vm_type getVmType();

	/** */
	vm_state getVmState();

	/** */
	bool setVmState(vm_state newState, bool freeStorage);

	/** */
	bool getMigrationState();

	/** */
	void setMigrationState(bool migState);

	/** */
	double getNominalCapacityToUse(Resource::res_type type);

	/** */
	unsigned long long getDataSent() override;

	/** */
	unsigned long long getDataReceived() override;

  /** */
  bool addResource(VResource* vres);

  /** */
  bool removeResource(int vresId);

  /** */
  bool updateVResourceTypeUse(Resource::res_type type, int perc);

  /** */
  std::vector<std::string> getSourceAgentList_OTcl();

  /** */
  std::string getSinkAgent_OTcl();

	/** */
	void printNoDetails() override;

	/** */
	void print() override;

	/** */
	int command(int argc, const char*const* argv) override;

	/** */
	static vm_state stringToVmState(std::string type);

	/** */
	static std::string vmStateToString(vm_state state);

private:

	/** */
	bool createConnection_OTcl(Vm* destVm, std::string trafficGen);

	/** */
	vm_type stringToVmType(std::string type);

	/** */
	std::string vmTypeToString(vm_type type);

	/** The virtual machine monitor that controls this vm */
	VmMonitor* hypervisor;

	/** The virtual machine type */
	vm_type vmType;

	/** The current vm state */
	vm_state vmState;

	/** Migration state */
	bool migrationState;

	/** */
	std::vector<std::string> sourceAgents_OTcl;

	/** */
	std::string sinkAgent_OTcl;

	/** */
	static std::map<std::string, vm_state> vmStateCheck;
	
	/** */
	static std::map<std::string, vm_type> vmTypeCheck;

};

#endif /* VM_H_ */
