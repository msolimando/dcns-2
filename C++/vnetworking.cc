/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#include "vnetworking.h"

/** */
static class VNetworkingClass : public TclClass {
public:
	VNetworkingClass() : TclClass("VNetworking") {}
	TclObject* create(int argc, const char*const* argv) {
	  return (new VNetworking(atoi(argv[4]), argv[5]));
	}
} class_vnetworking;

std::map<std::string, VNetworking::vnet_type> VNetworking::vnetTypeCheck = {
    {"NET_10MbE", VNetworking::NET_10MbE},
    {"NET_100MbE", VNetworking::NET_100MbE},
    {"NET_1GbE", VNetworking::NET_1GbE}
};

////////////

VNetworking::VNetworking(int vnetId, std::string vnetType) {

  this->resId = vnetId;
  this->resType = Resource::NETWORKING;
  this->vnetType = stringToVNetType(vnetType);
  this->nominalCap = 0;
  this->hostId = -1;
  //
  switch(this->vnetType){
  case NET_10MbE:{
    this->addResUnit(new VResourceUnit(1, 1250000)); // 1,25MB = 10Mb
    break;
  }
  case NET_100MbE:{
    this->addResUnit(new VResourceUnit(1, 12500000)); // 12,5MB = 100Mb
    break;
  }
  case NET_1GbE:{
    this->addResUnit(new VResourceUnit(1, 125000000)); // 125MB = 1Gb
    break;
  }
  default:
    //throw std::runtime_error("The chosen virtual networking type is not defined\n");
    break;
  }
  //
  this->nomCapToUse = this->getNominalCapacity();
}

VNetworking::~VNetworking(){

}

VNetworking::vnet_type VNetworking::getVNetType(){

  return this->vnetType;

}

void VNetworking::print(){

  //
  std::string nCap = getFormattedString(getNominalCapacity(), util::BYTEPS);
  std::string aCap = getFormattedString(getAvailableCapacity(), util::BYTEPS);
  std::string uCap = getFormattedString(getUsedCapacity(), util::BYTEPS);
  //
  double nCapToUse = (getNominalCapacityToUse()/getNominalCapacity())*100;

  std::cout << "VRes " << getId() << ": ";
  std::cout << "Type = " << Resource::resTypeToString(getResType());
  std::cout << " [" << vnetTypeToString(getVNetType()) << "], ";
  std::cout << "Vm_Id " << getHostId() << ", ";
  std::cout << "nomCap = " << nCap << " (" << nCapToUse << "%), ";
  std::cout << "usedCap = " << uCap  << ", ";
  std::cout << "availCap = " << aCap << "\n";

	std::vector <ResourceUnit*>::iterator iter;
	for(iter=resUnitList.begin(); iter!=resUnitList.end(); iter++)
			(*iter)->print(util::BYTEPS);

}

/** OTcl commands:
 * print -
 * get-available-cap -
 * update-id NEWID -
 **/
int VNetworking::command(int argc, const char*const* argv) {

  Tcl& tcl = Tcl::instance();
  //
	if(argc == 2) {
	  //
		if(strcmp(argv[1], "print") == 0) {
			this->print();
			return (TCL_OK);
		//
		} else if (strcmp(argv[1], "get-available-cap") == 0) {
		  double result = 0;
		  result= getAvailableCapacity();
		  char out[100];
		  sprintf(out, "set vnet_Acap %.0f", result);
		  tcl.eval(out);
			return (TCL_OK);
		}
	//
	} else if(argc == 3){
	  //
	  if(strcmp(argv[1], "update-id") == 0) {
	    int newId = atoi(argv[2]);
	    if(this->updateId(newId) == false){
	      std::cerr << "A virtual resource can change its id only if it's not attached to any vm\n";
	      return (TCL_ERROR);
	    }
	    return (TCL_OK);
	  }
	}
	return (TCL_ERROR);

}

VNetworking::vnet_type VNetworking::stringToVNetType(std::string type){

  std::map<std::string, VNetworking::vnet_type>::iterator it = vnetTypeCheck.find(type);
  if (it == vnetTypeCheck.end())
    throw std::runtime_error("The chosen virtual networking type is not defined\n");
  return it->second;

}

std::string VNetworking::vnetTypeToString(VNetworking::vnet_type type){

  std::map<std::string, VNetworking::vnet_type>::iterator it;
  for(it = vnetTypeCheck.begin(); it != vnetTypeCheck.end(); it++)
    if (it->second == type)
      return it->first;
  throw std::runtime_error("The chosen virtual networking type is not defined\n");

}
