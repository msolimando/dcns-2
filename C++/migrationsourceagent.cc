/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#include "migrationsourceagent.h"

/** */
static class MigrationSourceAgentClass : public TclClass {
public:
  MigrationSourceAgentClass() : TclClass("Agent/TCP/MigrationSourceAgent") {}
	TclObject* create(int argc, const char*const* argv) {
	  //OTcl domain: set newMigrationSource [new Agent/TCP/MigrationSourceAgent]
		return (new MigrationSourceAgent());
	}
} class_migrationsourceagent;

////////////

MigrationSourceAgent::MigrationSourceAgent() : TcpAgent(), dataToTransfer(0), rate(0),
    packetSize(0), lastPacketSeqNo(0), bytesTx(0), packetsTx(0), nextPktTime(-1),
    interval(0), running(false) {

}

MigrationSourceAgent::~MigrationSourceAgent(){

}

void MigrationSourceAgent::init(unsigned long long data, int pktSize, double bitrate){

  this->dataToTransfer = data;
  this->packetSize = pktSize;
  this->rate = bitrate;
  // Compute the inter-burst transmission interval
  this->interval = (packetSize*8.0)/rate;
  // Estimate the number of packets necessary to complete the vm transfer
  if((dataToTransfer % packetSize) == 0)
    lastPacketSeqNo = dataToTransfer/packetSize;
  else
    lastPacketSeqNo = (dataToTransfer/packetSize) + 1;
  ///////////////////////////// REMOVE ////////////////////////////
  //std::cout << "\n dataToTransfer = "<< dataToTransfer << ", packetSize = " <<
  //    packetSize << " lastPacketSeqNo = " << lastPacketSeqNo << "\n";
  ///////////////////////////// REMOVE ////////////////////////////

}

void MigrationSourceAgent::setDataToTransfer(unsigned long long data){

  this->dataToTransfer = data;

}

unsigned long long MigrationSourceAgent::getDataToTransfer(){

  return this->dataToTransfer;

}

void MigrationSourceAgent::setRate(double rate){

  this->rate = rate;

}

double MigrationSourceAgent::getRate(){

  return this->rate;

}

void MigrationSourceAgent::setPacketSize(int pktSize){

  this->packetSize = pktSize;

}

int MigrationSourceAgent::getPacketSize(){

  return this->packetSize;

}

unsigned long long MigrationSourceAgent::getLastPacketSeqNo(){

  return this->lastPacketSeqNo;

}

unsigned long long MigrationSourceAgent::getTxBytes(){

  return this->bytesTx;

}

unsigned long long MigrationSourceAgent::getTxPackets(){

  return this->packetsTx;

}

void MigrationSourceAgent::startDataTransfer(){

  // Recompute interval in case rate or packetSize has changed
  this->interval = (packetSize*8.0)/rate;
  //
  this->running = true;
  //
  this->expire(new Event());

}

void MigrationSourceAgent::stopDataTransfer(){

  // Stop the timer if the MigrationSourceAgent is currently running
  if (this->running == true)
    this->cancel();
  this->running = false;

}

void MigrationSourceAgent::expire(Event* e){

  // Does nothing if the MigrationSourceAgent object is not running
  if(this->running == false)
    return;
  // Send a packet to destination Node
  this->sendmsg(packetSize);
  // Update the bytes sent by the Agent
  bytesTx += packetSize;
  // Update the packets sent by the Agent
  packetsTx++;
  /////////////////////// REMOVE ////////////////////////////////
  //  std::cout << "PACKET SENT = " << packetsTx << "\n";
  /////////////////////// REMOVE ////////////////////////////////
  // Figure out when to send the next packet
  nextPktTime = nextInterval(packetSize);
  // Schedule the next send
  if(nextPktTime > 0)
    this->resched(nextPktTime);
  else
    this->running = false;

}

double MigrationSourceAgent::nextInterval(int pktSize){

  // Recompute interval in case rate or packetSize has changed
  this->interval = (packetSize*8.0)/rate;
  double t = interval;
  //
  this->packetSize = pktSize;
  //
  if(packetsTx < lastPacketSeqNo)
    return(t);
  else
    return(-1);

}
