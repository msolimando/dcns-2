/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ULLQUEUEMONITOR_H_
#define ULLQUEUEMONITOR_H_

#include <iostream>

#include <packet.h>
#include <queue-monitor.h>
#include <tclcl.h>

class ULLQueueMonitor : public QueueMonitor {

public:

  /** */
  ULLQueueMonitor();

  /** */
  ~ULLQueueMonitor();

  /** */
  unsigned long long getBarrivals();

  /** */
  unsigned long long getParrivals();

  /** */
  unsigned long long getBdepartures();

  /** */
  unsigned long long getPdepartures();

  /** */
  unsigned long long getBdrops();

  /** */
  unsigned long long getPdrops();

  /** */
  void in(Packet* p) override;

  /** */
  void out(Packet* p) override;

  /** */
  void drop(Packet* p) override;

  /** */
  int command(int argc, const char*const* argv) override;

private:

  /** */
  unsigned long long ullbarrivals_;

  /** */
  unsigned long long ullparrivals_;

  /** */
  unsigned long long ullbdepartures_;

  /** */
  unsigned long long ullpdepartures_;

  /** */
  unsigned long long ullbdrops_;

  /** */
  unsigned long long ullpdrops_;
	
};

#endif /* ULLQUEUEMONITOR_H_ */

