/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef VSTORAGE_H_
#define VSTORAGE_H_

#include <iostream>
#include <map>
#include <stdexcept>
#include <cstdlib>
#include <string>
#include <vector> 

#include <tclcl.h>

#include "util.h"
#include "vresource.h"
#include "vresourceunit.h"

/** */
class VStorage : public VResource, public TclObject {

public:

  //
  enum vstor_type {STORAGE_10GB, STORAGE_100GB, STORAGE_250GB, STORAGE_500GB, STORAGE_1TB};
	
	/** Class constructor */
  VStorage(int vstorId, std::string vstorType);
	
	/** Class destructor */
	~VStorage();

	/** Returns the virtual storage type */
	vstor_type getVStorType();

	/** */
	void print() override;

	/** Defines the commands available for the class objects in the OTcl domain */
	int command(int argc, const char*const* argv);

private:

	/** */
	vstor_type stringToVStorType(std::string type);

	/** */
	std::string vstorTypeToString(vstor_type type);

	/** */
	vstor_type vstorType;

	/** */
	static std::map<std::string, vstor_type> vstorTypeCheck;

};

#endif /* VSTORAGE_H_ */
