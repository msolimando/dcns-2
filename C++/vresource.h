/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef VRESOURCE_H_
#define VRESOURCE_H_

#include "resource.h"

/** */
class VResource : public Resource {

public:

  /** */
  //VResource(int vresId, res_type type);

  /** */
  virtual ~VResource();

  /**  */
  double getNominalCapacityToUse();

  /** */
  bool setNominalCapacityToUse(double perc);

protected:

  /** */
  double nomCapToUse;

};

#endif /* VRESOURCE_H_ */
