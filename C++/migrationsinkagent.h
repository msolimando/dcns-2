/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MIGRATIONSINKAGENT_H_
#define MIGRATIONSINKAGENT_H_

#include <iostream>

#include <tcp-sink.h>

#include "vm.h"

class Pm;

/** */
class MigrationSinkAgent : public TcpSink {

public:

  /** */
  MigrationSinkAgent(Acker* acker);

  /** */
  ~MigrationSinkAgent();

  /** */
  void setVmToMigrate(Vm* vm);

  /** */
  Vm* getVmToMigrate();

  /** */
  void setVmPreMigrationState(Vm::vm_state vmstate);

  /** */
  Vm::vm_state getVmPreMigrationState();

  /** */
  void setHostDestination(Pm* pm);

  /** */
  Pm* getHostDestination();

  /** */
  void setPreCopyState(bool state);

  /** */
  bool getPreCopyState();

  /** */
  void setLastPacketSeqNo(int seqno);

  /** */
  int getLastPacketSeqNo();

  /** */
  unsigned long long getRxBytes();

  /** */
  unsigned long long getRxPkts();

  /** */
  void recv(Packet* pkt, Handler* hdl) override;

private:

  /** */
  Vm* vmToMigrate;

  /** */
  Vm::vm_state vmPreMigState;

  /** */
  Pm* hostDestination;

  /** */
  bool preCopy;

  /** */
  unsigned long long bytesRx;

  /** */
  unsigned long long packetsRx;

  /** */
  int seqno;

  /** */
  int lastPacketSeqNo;

};

#endif /* MIGRATIONSINKAGENT_H_ */
