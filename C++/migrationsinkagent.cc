/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#include "migrationsinkagent.h"
#include "pm.h"

/** */
static class MigrationSinkAgentClass : public TclClass {
public:
  MigrationSinkAgentClass(): TclClass("Agent/TCPSink/MigrationSinkAgent") {}
	TclObject* create(int argc, const char*const* argv) {
	  //OTcl domain: set newMigrationSink [new Agent/TCPSink/MigrationSinkAgent]
		return (new MigrationSinkAgent(new Acker()));
	}
} class_migrationsinkagent;

////////////

MigrationSinkAgent::MigrationSinkAgent(Acker* acker) : TcpSink(acker), vmToMigrate(NULL),
    vmPreMigState(Vm::INACTIVE), hostDestination(NULL), preCopy(false), bytesRx(0),
    packetsRx(0), seqno(0), lastPacketSeqNo(-1) {

}

MigrationSinkAgent::~MigrationSinkAgent(){

}

void MigrationSinkAgent::setVmToMigrate(Vm* vm){

  this->vmToMigrate = vm;

}

Vm* MigrationSinkAgent::getVmToMigrate(){

  return this->vmToMigrate;

}

void MigrationSinkAgent::setVmPreMigrationState(Vm::vm_state vmstate){

  this->vmPreMigState = vmstate;

}

Vm::vm_state MigrationSinkAgent::getVmPreMigrationState(){

  return this->vmPreMigState;

}

void MigrationSinkAgent::setHostDestination(Pm* pm){

  this->hostDestination = pm;

}

Pm* MigrationSinkAgent::getHostDestination(){

  return this->hostDestination;

}

void MigrationSinkAgent::setPreCopyState(bool state){

  this->preCopy = state;

}

bool MigrationSinkAgent::getPreCopyState(){

  return this->preCopy;

}

void MigrationSinkAgent::setLastPacketSeqNo(int seqno){

  this->lastPacketSeqNo = seqno;

}

int MigrationSinkAgent::getLastPacketSeqNo(){

  return this->lastPacketSeqNo;

}


unsigned long long MigrationSinkAgent::getRxBytes(){

  return this->bytesRx;

}

unsigned long long MigrationSinkAgent::getRxPkts(){

  return this->packetsRx;

}

void MigrationSinkAgent::recv(Packet* pkt, Handler* hdl){

  // Retrieve the common packet header and update the number of bytes received
  bytesRx += hdr_cmn::access(pkt)->size();
  // Update the number of packets received
  packetsRx++;
  // Retrieve the protocol-specific packet header (i.e. tcp) to update seqno
  hdr_tcp* p = hdr_tcp::access(pkt);
  seqno = p->seqno();
  /////////////////////// REMOVE ////////////////////////////////
  //std::cout << "PACKET RECEIVED = " << seqno << "\n";
  /////////////////////// REMOVE ////////////////////////////////
  // After receiving the last data packet from MigrationSourceAgent
  // complete the vm migration
  if(seqno == lastPacketSeqNo){
    hostDestination->getVmMonitor()->
          completeMigration(vmToMigrate, vmPreMigState, hostDestination, this->preCopy);
  }
  //
  TcpSink::recv(pkt, hdl);

}
