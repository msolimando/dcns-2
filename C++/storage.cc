/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#include "storage.h"

/** */
static class StorageClass : public TclClass {
public:
  StorageClass() : TclClass("Storage") {}
	TclObject* create(int argc, const char*const* argv) {
	  return (new Storage(atoi(argv[4]), argv[5]));
	}
} class_storage;

std::map<std::string, Storage::stor_type> Storage::storTypeCheck = {
   {"STORAGE_1TB", Storage::STORAGE_1TB},
   {"STORAGE_10TB", Storage::STORAGE_10TB},
   {"STORAGE_25TB", Storage::STORAGE_25TB},
   {"STORAGE_100TB", Storage::STORAGE_100TB}
};

////////////

Storage::Storage(int storId, std::string storType) {

  this->resId = storId;
  this->resType = Resource::STORAGE;
  this->storType = stringToStorType(storType);
  this->nominalCap = 0;
  this->hostId = -1;
  //
  switch(this->storType){
  case STORAGE_1TB:{
    this->addResUnit(new PResourceUnit(1, 500000000000)); // 500GB
    this->addResUnit(new PResourceUnit(2, 500000000000)); // 500GB
    break;
  }
  case STORAGE_10TB:{
    this->addResUnit(new PResourceUnit(1, 1000000000000)); // 1TB
    this->addResUnit(new PResourceUnit(2, 1000000000000)); // 1TB
    this->addResUnit(new PResourceUnit(3, 1000000000000)); // 1TB
    this->addResUnit(new PResourceUnit(4, 1000000000000)); // 1TB
    this->addResUnit(new PResourceUnit(5, 1000000000000)); // 1TB
    this->addResUnit(new PResourceUnit(6, 1000000000000)); // 1TB
    this->addResUnit(new PResourceUnit(7, 1000000000000)); // 1TB
    this->addResUnit(new PResourceUnit(8, 1000000000000)); // 1TB
    this->addResUnit(new PResourceUnit(9, 1000000000000)); // 1TB
    this->addResUnit(new PResourceUnit(10, 1000000000000)); // 1TB
    break;
  }
  case STORAGE_25TB:{
    this->addResUnit(new PResourceUnit(1, 2500000000000)); // 2.5TB
    this->addResUnit(new PResourceUnit(2, 2500000000000)); // 2.5TB
    this->addResUnit(new PResourceUnit(3, 2500000000000)); // 2.5TB
    this->addResUnit(new PResourceUnit(4, 2500000000000)); // 2.5TB
    this->addResUnit(new PResourceUnit(5, 2500000000000)); // 2.5TB
    this->addResUnit(new PResourceUnit(6, 2500000000000)); // 2.5TB
    this->addResUnit(new PResourceUnit(7, 2500000000000)); // 2.5TB
    this->addResUnit(new PResourceUnit(8, 2500000000000)); // 2.5TB
    this->addResUnit(new PResourceUnit(9, 2500000000000)); // 2.5TB
    this->addResUnit(new PResourceUnit(10, 2500000000000)); // 2.5TB
    break;
  }
  case STORAGE_100TB:{
    this->addResUnit(new PResourceUnit(1, 4000000000000)); // 4TB
    this->addResUnit(new PResourceUnit(2, 4000000000000)); // 4TB
    this->addResUnit(new PResourceUnit(3, 4000000000000)); // 4TB
    this->addResUnit(new PResourceUnit(4, 4000000000000)); // 4TB
    this->addResUnit(new PResourceUnit(5, 4000000000000)); // 4TB
    this->addResUnit(new PResourceUnit(6, 4000000000000)); // 4TB
    this->addResUnit(new PResourceUnit(7, 4000000000000)); // 4TB
    this->addResUnit(new PResourceUnit(8, 4000000000000)); // 4TB
    this->addResUnit(new PResourceUnit(9, 4000000000000)); // 4TB
    this->addResUnit(new PResourceUnit(10, 4000000000000)); // 4TB
    this->addResUnit(new PResourceUnit(11, 4000000000000)); // 4TB
    this->addResUnit(new PResourceUnit(12, 4000000000000)); // 4TB
    this->addResUnit(new PResourceUnit(13, 4000000000000)); // 4TB
    this->addResUnit(new PResourceUnit(14, 4000000000000)); // 4TB
    this->addResUnit(new PResourceUnit(15, 4000000000000)); // 4TB
    this->addResUnit(new PResourceUnit(16, 4000000000000)); // 4TB
    this->addResUnit(new PResourceUnit(17, 4000000000000)); // 4TB
    this->addResUnit(new PResourceUnit(18, 4000000000000)); // 4TB
    this->addResUnit(new PResourceUnit(19, 4000000000000)); // 4TB
    this->addResUnit(new PResourceUnit(20, 4000000000000)); // 4TB
    this->addResUnit(new PResourceUnit(21, 4000000000000)); // 4TB
    this->addResUnit(new PResourceUnit(22, 4000000000000)); // 4TB
    this->addResUnit(new PResourceUnit(23, 4000000000000)); // 4TB
    this->addResUnit(new PResourceUnit(24, 4000000000000)); // 4TB
    this->addResUnit(new PResourceUnit(25, 4000000000000)); // 4TB
    break;
  }
  default:
    //throw std::runtime_error("The chosen Storage type is not defined\n");
    break;
  }

}

Storage::~Storage(){

}

Storage::stor_type Storage::getStorType(){

  return this->storType;

}

void Storage::print(){

  //
  std::string nCap = getFormattedString(getNominalCapacity(), util::BYTE);
  std::string aCap = getFormattedString(getAvailableCapacity(), util::BYTE);
  std::string uCap = getFormattedString(getUsedCapacity(), util::BYTE);

  std::cout << "Res " << getId() << ": ";
  std::cout << "Type = " << Resource::resTypeToString(getResType());
  std::cout << " [" << storTypeToString(getStorType()) << "], ";
  std::cout << "Pm_Id = " << getHostId() << ", ";
	std::cout << "nomCap = " << nCap << ", ";
	std::cout << "usedCap = " << uCap << ", ";
	std::cout << "availCap = " << aCap << "\n";

	std::vector <ResourceUnit*>::iterator iter;
	for(iter=resUnitList.begin(); iter!=resUnitList.end(); iter++)
			(*iter)->print(util::BYTE);

}

/** OTcl commands:
 * print -
 * get-available-cap -
 * update-id NEWID -
 **/
int Storage::command(int argc, const char*const* argv) {

  Tcl& tcl = Tcl::instance();
  //
	if(argc == 2) {
		if(strcmp(argv[1], "print") == 0) {
			this->print();
			return (TCL_OK);
		} else if (strcmp(argv[1], "get-available-cap") == 0) {
		  double result = 0;
		  result= getAvailableCapacity();
		  char out[100];
		  sprintf(out, "set stor_Acap %.0f", result);
		  tcl.eval(out);
			return (TCL_OK);
		}
	//
	} else if(argc == 3){
	  //
	  if(strcmp(argv[1], "update-id") == 0) {
	    int newId = atoi(argv[2]);
	    if(this->updateId(newId) == false){
	      std::cerr << "A resource can change its id only if it's not attached to any server\n";
	      return (TCL_ERROR);
	    }
	    return (TCL_OK);
	  }
	}
	return (TCL_ERROR);

}

Storage::stor_type Storage::stringToStorType(std::string type){

  std::map<std::string, Storage::stor_type>::iterator it = storTypeCheck.find(type);
  if (it == storTypeCheck.end())
    throw std::runtime_error("The chosen Storage type is not defined\n");
  return it->second;

}

std::string Storage::storTypeToString(Storage::stor_type type){

  std::map<std::string, Storage::stor_type>::iterator it;
  for(it = storTypeCheck.begin(); it!= storTypeCheck.end(); it++)
    if (it->second == type)
      return it->first;
  throw std::runtime_error("The chosen Storage type is not defined\n");

}
