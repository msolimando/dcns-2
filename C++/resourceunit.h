/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RESOURCEUNIT_H_
#define RESOURCEUNIT_H_

#include <iostream>

#include "util.h"

/** */
class ResourceUnit {

public:

  /** Class destructor */
  virtual ~ResourceUnit();

  /** Returns the resource id */
  int getId();

  /** Returns the resource unit nominal capacity */
  double getNominalCapacity();

  /** Returns the currently used capacity for this resource unit */
  double getUsedCapacity();

  /** */
  void setUsedCapacity(double value);

  /** Returns the resource unit available capacity */
  double getAvailableCapacity();

  /** To specify the  id of resource to which this unit belongs */
  void setHostResourceId(int resourceId);

  /** Returns the id of the resource to which this unit belongs */
  int getHostResourceId();

  /** Prints the resource unit characteristics*/
  virtual void print(util::format_type fType) = 0;

protected:

  /** Resource Unit id*/
  int unitId;

  /** */
  double nominalCap;

  /** */
  double usedCap;

  /** The id of the resource to which this unit belongs*/
  int hostResId;

};

#endif /* RESOURCEUNIT_H_ */
