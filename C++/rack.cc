/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#include "dcmanager.h"
#include "rack.h"

static class RackClass : public TclClass {
public:
	RackClass() : TclClass("Rack") {}
	TclObject* create(int argc, const char*const*argv) {
	  //OTcl domain: set newRack [new Rack id nServerSlots nSwitchSlots]
	  return (new Rack(atoi(argv[4]), atoi(argv[5]), atoi(argv[6])));
	}
} class_rack;

////////////

Rack::Rack(int rackId, int switchSlots, int serverSlots) :
    serversInstalled(0), switchesInstalled(0){

  this->id = rackId;
  this->serverSlots = serverSlots;
  this->switchSlots = switchSlots;

  //
  DcManager::Instance()->addRackToRackList(this);

}

Rack::~Rack(){

  serversList.~vector();
  switchesList.~vector();

}

int Rack::getId(){

  return this->id;

}

int Rack::getServerSlots(){

  return this->serverSlots;

}

int Rack::getSwitchSlots(){

  return this->switchSlots;

}

int Rack::getServersInstalled(){

  return this->serversInstalled;

}

int Rack::getSwitchesInstalled(){

  return this->switchesInstalled;

}

bool Rack::checkServerInstallationFeasibility(){

  if(serversInstalled < serverSlots)
    return true;
  return false;

}

bool Rack::checkSwitchInstallationFeasibility(){

  if(switchesInstalled < switchSlots)
    return true;
  return false;

}

std::vector<Pm*> Rack::getServersList(){

  return this->serversList;

}

std::vector<std::string> Rack::getServersList_OTcl(){

  std::vector<std::string> sList;
  std::vector<Pm*>::iterator it;
  for(it = serversList.begin(); it != serversList.end(); it++)
    sList.push_back(std::string((*it)->name()));
  return sList;

}

std::vector<ToRSwitch*> Rack::getSwitchesList(){

  return this->switchesList;

}

std::vector<std::string> Rack::getSwitchesList_OTcl(){

  std::vector<std::string> sList;
  std::vector<ToRSwitch*>::iterator it;
  for(it = switchesList.begin(); it != switchesList.end(); it++)
    sList.push_back(std::string((*it)->name()));
  return sList;

}

bool Rack::addPm(Pm* pm){

  // Check if the server is already assigned to another rack
  int pmRackId = pm->getRackId();
  if(pmRackId != -1){
    if(pmRackId == this->getId())
      std::cerr << "The server " << pm->getId() << " is already assigned to "
          "rack " << pmRackId <<"\n";
    else
      std::cerr << "The server " << pm->getId() << " is already assigned to another rack "
          "(rack " << pmRackId << "); remove the server from rack " << pmRackId <<
          " if you want to assign it to rack " << this->getId() << " or select another pm\n";
    return false;
  }
  // If the server is currently not assigned to any rack check if this
  // rack has enough available slot to allocate the new machine
  if(checkServerInstallationFeasibility() == true){
    serversList.push_back(pm);
    this->serversInstalled++;
    pm->setRackId(this->getId());
    return true;
  }
  return false;

}

bool Rack::removePm(int pmId){

  std::vector<Pm*>::iterator it;
  for(it = serversList.begin(); it != serversList.end(); it++){
    // If the server has been assigned to this rack
    if((*it)->getId() == pmId){
      Pm::pm_state pmState = (*it)->getPmState();
      // A server can be removed from the rack only is its state is OFF
      if(pmState != Pm::OFF){
        std::cerr << "The server " << pmId << " must be turned off before removing it"
            " from rack " << this->getId() << "\n";
        return false;
      }
      serversList.erase(it);
      this->serversInstalled--;
      (*it)->setRackId(-1);
      return true;
    }
  }
  std::cerr << "There isn't any server with id equals to " << pmId
        << " in rack " << this->getId() << "\n";
  return false;

}

bool Rack::addSwitch(ToRSwitch* sw){

  // Check if the switch is already assigned to another rack
   int swRackId = sw->getRackId();
   if(swRackId != -1){
     if(swRackId == this->getId())
       std::cerr << "The TOR switch " << sw->getId() << " is already assigned to "
           "rack " << swRackId <<"\n";
     else
       std::cerr << "The TOR switch " << sw->getId() << " is already assigned to another rack "
           "(rack " << swRackId << "); remove the switch from rack " << swRackId <<
           " if you want to assign it to rack " << this->getId() << " or select another "
           "access switch\n";
     return false;
   }
   // If the switch is currently not assigned to any rack, check if this
   // rack has enough available slot to allocate the new device
   if(checkSwitchInstallationFeasibility() == true){
     switchesList.push_back(sw);
     this->switchesInstalled++;
     sw->setRackId(this->getId());
     return true;
   }
   return false;

}

bool Rack::removeSwitch(int switchId){

  std::vector<ToRSwitch*>::iterator it;
  for(it = switchesList.begin(); it != switchesList.end(); it++){
    // If the switch has been assigned to this rack
    if((*it)->getId() == switchId){
      Switch::switch_state swState = (*it)->getSwitchState();
      // A switch can be removed from the rack only is its state is OFF
      if(swState != Switch::OFF){
        std::cerr << "The switch " << switchId << " must be turned off before removing it"
            " from rack " << this->getId() << "\n";
        return false;
      }
      switchesList.erase(it);
      this->switchesInstalled--;
      (*it)->setRackId(-1);
      return true;
    }
  }
  std::cerr << "There isn't any switch with id equals to " << switchId
        << " in rack " << this->getId() << "\n";
  return false;

}

void Rack::print(){

  std::cout << "##############################################################"
       << "##############################################################\n";
  std::cout << "Rack " << getId() << ": ";
  std::cout << " Switch = [" << getSwitchesInstalled() << "/" << getSwitchSlots() <<"], ";
  std::cout << " Server = [" << getServersInstalled() << "/" << getServerSlots() <<"]\n";
  // Print Switches List
  std::cout << "########################################################### Switches"
       << " #######################################################\n";
  std::vector<ToRSwitch*>::iterator itSw;
  for(itSw = switchesList.begin(); itSw != switchesList.end(); itSw++){
    std::cout << "=============================================================="
        << "==============================================================\n"
        << "ToRSwitch " << (*itSw)->getId() << ": "
        << "Rack_Id = " << (*itSw)->getRackId() << ", "
        << "State = " << Switch::switchStateToString((*itSw)->getSwitchState()) << "\n"
        << "             nPorts = " << (*itSw)->getNumberOfSwitchPorts();
    //
    std::map<Switch::port_type, int>::iterator itP;
    std::map<Switch::port_type, int> portMap = (*itSw)->getPortMap();
    for(itP = portMap.begin(); itP != portMap.end(); itP++)
      std::cout << " [" << Switch::portTypeToString((*itP).first) << " = " << (*itP).second << "]";
    std::cout << "\n=============================================================="
         << "==============================================================\n";
  }
  // Print Servers List
  std::cout << "############################################################ Servers"
       << " #######################################################\n";
  std::vector<Pm*>::iterator itPm;
  for(itPm = serversList.begin(); itPm != serversList.end(); itPm++){
    std::cout << "==============================================================";
    std::cout << "==============================================================\n";
    (*itPm)->printNoDetails();
    std::cout << "==============================================================";
    std::cout << "==============================================================\n";
  }
  std::cout << "##############################################################"
       << "##############################################################\n";

}

/** OTcl commands:
 * commands-list -
 * print {} -
 * get-id {} -
 * get-server-list {} -
 * get-switch-list {} -
 * add-pm {PM*} -
 * remove-pm {PM_ID} -
 * add-switch {SWITCH*} -
 * remove-switch {SWITCH_ID} -
 **/
int Rack::command(int argc, const char*const* argv){

  Tcl& tcl = Tcl::instance();
  //
  if(argc == 2) {
    if(strcmp(argv[1], "commands-list") == 0) {
      std::cout << "- commands-list\n"
                   "- print {}\n"
                   "- get-id {}"
                   "- get-server-list {}\n"
                   "- get-switch-list {}\n"
                   "- add-pm {PM*}\n"
                   "- remove-pm {PM_ID}\n"
                   "- add-switch {SWITCH*}\n"
                   "- remove-switch {SWITCH_ID}\n";
      return (TCL_OK);
    //
    }else if(strcmp(argv[1], "print") == 0) {
      print();
      return (TCL_OK);
    //
    } else if(strcmp(argv[1], "get-id") == 0) {
      // Pass the rack id to the OTCl domain
      tcl.result(std::to_string(this->getId()).c_str());
      return (TCL_OK);
    //
    }else if(strcmp(argv[1], "get-server-list") == 0){
      std::vector<std::string> sList = getServersList_OTcl();
      tcl.evalc("set serverListOTcL { }");
      std::vector<std::string>::iterator it;
      for(it = sList.begin(); it != sList.end(); it++){
        tcl.evalf("lappend serverListOTcL %s", (*it).c_str());
      }
      tcl.evalc("lrange $serverListOTcL 0 end");
      return (TCL_OK);
    //
    }else if(strcmp(argv[1], "get-switch-list") == 0){
      std::vector<std::string> sList = getSwitchesList_OTcl();
      tcl.evalc("set switchListOTcl { }");
      std::vector<std::string>::iterator it;
      for(it = sList.begin(); it != sList.end(); it++){
        tcl.evalf("lappend switchListOTcl %s", (*it).c_str());
      }
      tcl.evalc("lrange $switchListOTcl 0 end");
      return (TCL_OK);
    }
  //
  }else if (argc == 3) {
    //
    if(strcmp(argv[1], "add-pm") == 0) {
      Pm* pm = (Pm*) (TclObject::lookup(argv[2]));
      if(pm == 0) {
        tcl.resultf("The server %s is not defined", argv[2]);
        return (TCL_ERROR);
      }
      if(addPm(pm) == false)
        tcl.resultf("The operation \"add-pm\" cannot be completed. ");
      return (TCL_OK);
    //
    }else if(strcmp(argv[1], "remove-pm") == 0){
      int pmId = atoi(argv[2]);
      if(removePm(pmId)==false)
        tcl.resultf("The operation \"remove-pm\" cannot be completed. ");
      return (TCL_OK);
    //
    }else if(strcmp(argv[1], "add-switch") == 0) {
      ToRSwitch* sw = (ToRSwitch*) (TclObject::lookup(argv[2]));
      if(sw == 0) {
        tcl.resultf("The switch %s is not defined", argv[2]);
          return (TCL_ERROR);
      }
      if(addSwitch(sw) == false)
        tcl.resultf("The operation \"add-switch\" cannot be completed. ");
      return (TCL_OK);
    //
    }else if(strcmp(argv[1], "remove-switch") == 0){
      int switchId = atoi(argv[2]);
      if(removeSwitch(switchId)==false)
        tcl.resultf("The operation \"remove-switch\" cannot be completed. ");
      return (TCL_OK);
    }
	}
	return (TCL_ERROR);

}
