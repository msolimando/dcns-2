/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RACK_H_
#define RACK_H_

#include <vector>

#include "pm.h"
#include "torswitch.h"
#include "util.h"

class DcManager;

/** Each object of this class represents a rack into the datacenter architecture */
class Rack : public TclObject {

public:
	
  /** Rack constructor */
  Rack(int rackId, int switchSlots, int serverSlots);

  /** DcRack destructor */
  ~Rack();

  /** */
  int getId();

  /** */
  int getServerSlots();

  /** */
  int getSwitchSlots();

  /** */
  int getServersInstalled();

  /** */
  int getSwitchesInstalled();

  /** */
  bool checkServerInstallationFeasibility();

  /** */
  bool checkSwitchInstallationFeasibility();

  /** */
  std::vector<Pm*> getServersList();

  /** */
  std::vector<std::string> getServersList_OTcl();

  /** */
  std::vector<ToRSwitch*> getSwitchesList();

  /** */
  std::vector<std::string> getSwitchesList_OTcl();

  /** */
  bool addPm(Pm* pm);

  /** */
  bool removePm(int pmId);

  /** */
  bool addSwitch(ToRSwitch* sw);

  /** */
  bool removeSwitch(int accSwitchId);

  /** */
  void print();

  /** */
  int command(int argc, const char*const* argv) override;

private:

	/** Rack id */
	int id;

	/** */
	int serverSlots;

	/** */
	int switchSlots;

	/** */
	int serversInstalled;

	/** */
	int switchesInstalled;

	/** Hosted servers list */
	std::vector<Pm*> serversList;

	/** Hosted switches list */
	std::vector<ToRSwitch*> switchesList;

};

#endif /* RACK_H_ */
