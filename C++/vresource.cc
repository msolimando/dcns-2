/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#include "vresource.h"


VResource::~VResource() {

}

double VResource::getNominalCapacityToUse(){

  return this->nomCapToUse;

}

bool VResource::setNominalCapacityToUse(double perc){

  // The percentage of use must be between 0 and 100
  if((perc < 0) || (perc>100)){
    std::cerr << "The percent of use of the virtual resource must be between 0% and 100%\n";
    return false;
  }
  this->nomCapToUse = (this->getNominalCapacity()*perc/100);
  return true;

}
