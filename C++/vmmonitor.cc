/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#include "vmmonitor.h"
#include "pm.h"

VmMonitor::VmMonitor() : pm(NULL) {

}

VmMonitor::~VmMonitor(){

}

Pm* VmMonitor::getPm(){

  return this->pm;

}

int VmMonitor::getPmId(){

  return pm->getId();

}

bool VmMonitor::setPm(Pm* pm){

  if(this->pm != NULL){
    std::cerr << "The hypervisor is already assigned to server " << pm->getId() << "\n";
    return false;
  }
  this->pm = pm;
  return true;

}

bool VmMonitor::checkAllocationFeasibility(Vm* vm){

  // Check if the host is currently sleeping or powered off
  if(pm->getPmState() != Pm::ON){
    std::cout << "\nThe vm " << vm->getId() << " cannot be assigned to server " << pm->getId()
        << " because this host is currently powered off or in a sleep state\n";
    return false;
  }
  // If the host is currently powered on, check if the vm is already assigned to this host
  std::vector<Vm*>::iterator it;
  for(it = pm->hostedVMs.begin(); it != pm->hostedVMs.end(); it++){
    if((*it)->getId()==vm->getId()){
      std::cout << "\nThe vm " << vm->getId() << " is already assigned to server "
          << pm->getId() << "\n";
      return false;
    }
  }
  // If the virtual machine is currently not assigned to this host
  // check each vm resource demand against each available host resource
  double vmUsedVCpu = vm->getNominalCapacityToUse(Resource::CPU);
  double vmUsedVMem = vm->getNominalCapacityToUse(Resource::MEMORY);
  double vmUsedVStor = vm->getNominalCapacityToUse(Resource::STORAGE);
  double hostACpu = pm->getAvailableCapacity(Resource::CPU);
  double hostAMem = pm->getAvailableCapacity(Resource::MEMORY);
  double hostAStor = pm->getAvailableCapacity(Resource::STORAGE);
  //
  if((hostACpu < vmUsedVCpu) || (hostAMem < vmUsedVMem) || (hostAStor < vmUsedVStor)){
    std::cout << "\nThe vm " << vm->getId() << " cannot be assigned to server " << pm->getId()
        << " because the host hasn't enough available resources\n";
    return false;
  }
  return true;

}

bool VmMonitor::addVm(Vm* vm){

  // Check if the vm is already assigned to another host
  if(vm->getHostPmId() != -1){
    std::cerr << "The vm " << vm->getId() << "is already assigned to server " << vm->getHostPmId()
        <<"; if you want to migrate the vm from this host use the instproc migrate-vm"
          " of class DcManager\n";
    return false;
  }
  //
  if(vm->getVmState() != Vm::INACTIVE){
    //TODO Remove this check if always true
    std::cerr << "addVm ERROR: vm state not INACTIVE";
    return false;
  }
  // If the vm is currently not assigned to any server, check if the host linked
  // with this vmmonitor has enough available resources to satisfy the request
  if(checkAllocationFeasibility(vm) == true){
    // If the host linked with the vmmonitor has enough available resources to satisfy
    // the request, update the storage resource consumption of the host according to
    // the vm request (the other resource consumptions - i.e. CPU and MEMORY - will be
    // updated when the vm will change its state to RUNNING or PAUSED)
    /*
    bool op1 = pm->increaseResourceTypeUse(Resource::CPU,
       vm->getNominalCapacityToUse(Resource::CPU));
    bool op2 = pm->increaseResourceTypeUse(Resource::MEMORY,
        vm->getNominalCapacityToUse(Resource::MEMORY));
    bool op3 = pm->increaseResourceTypeUse(Resource::STORAGE,
        vm->getNominalCapacityToUse(Resource::STORAGE));
    // If the three update operations succeed, add the vm to the host
    if(op1 && op2 && op3){
    */
    if(pm->increaseResourceTypeUse(Resource::STORAGE,
        vm->getNominalCapacityToUse(Resource::STORAGE)) == true){
      // Add the vm to the vm list of pm
      pm->hostedVMs.push_back(vm);
      // Update the vmm reference of vm
      vm->setVmMonitor(this);
      // Attach the vm sink agent with the OTcl node linked with the host
      this->attachAgentToPm_OTcl(vm->getSinkAgent_OTcl());
      // If the vm has also a list of attached source agents, attach all of
      // these source agents with the OTcl node linked with the host
      if(vm->getSourceAgentList_OTcl().empty() == false){
        std::vector<std::string>::iterator it;
        std::vector<std::string> sourceAgents = vm->getSourceAgentList_OTcl();
        for(it = sourceAgents.begin(); it != sourceAgents.end(); it++)
          this->attachAgentToPm_OTcl(*it);
      }
      // The double update operation is used as a trick to update the vm virtual storage
      // TODO: Try to avoid the double calling
      updateVmState(vm->getId(), Vm::RUNNING, false);
      updateVmState(vm->getId(), Vm::INACTIVE, false);
      return true;
    }
  }
  return false;

}

bool VmMonitor::removeVm(int vmId){

  std::vector<Vm*>::iterator it;
  for(it = pm->hostedVMs.begin(); it != pm->hostedVMs.end(); it++){
    // If the vm has been previously assigned to this host
    if((*it)->getId() == vmId){
      Vm* vmToRemove = (*it);
      Vm::vm_state vmState = vmToRemove->getVmState();
      if(vmState != Vm::INACTIVE){
        // Update the vm state freeing also its virtual used storage
        updateVmState(vmId, Vm::INACTIVE, true);
      }
      // If the storage deallocation succeed, remove the vm from the host
      if(pm->decreaseResourceTypeUse(Resource::STORAGE,
          vmToRemove->getUsedCapacity(Resource::STORAGE))) {
        //
        pm->hostedVMs.erase(it);
        //
        vmToRemove->setVmMonitor(NULL);
        // Detach the vm sink agent from the OTcl node linked with the host
        std::string sinkAgent = vmToRemove->getSinkAgent_OTcl();
        this->detachAgentFromPm_OTcl(sinkAgent);
        // If the vm has also a list of attached source agents, detach all these agents
        // from the OTcl node linked with the host
        if (vmToRemove->getSourceAgentList_OTcl().empty() == false){
          std::vector<std::string>::iterator itSource;
          std::vector<std::string> sourceAgents = vmToRemove->getSourceAgentList_OTcl();
          for(itSource = sourceAgents.begin(); itSource != sourceAgents.end(); itSource++)
            this->detachAgentFromPm_OTcl(*itSource);
        }
        return true;
      }
      return false;
    }
  }
  std::cout << "\nThere isn't any vm with id equals to " << vmId << " in dcHost "
      << pm->getId() << "\n";
  return false;

}

bool VmMonitor::startMigration(Vm* vmToMigrate, Pm* newHost, bool preCopy){

  bool liveMig = false;
  Pm* sourceHost = this->pm;
  // Check if the vm is currently migrating between two hosts
  if (vmToMigrate->getMigrationState() == true){
    std::cout << "\nThe vm " << vmToMigrate->getId() << " is currently migrating from server "
        << sourceHost->getId() << " to another server. Wait for the completion of that migration "
        "before you start a new one:";
    return false;
  }
  // Check if the vm is actually assigned to sourceHost
  if(std::find(sourceHost->hostedVMs.begin(), sourceHost->hostedVMs.end(), vmToMigrate) ==
      sourceHost->hostedVMs.end()) {
    std::cout << "\nThe vm " << vmToMigrate->getId() << " is currently not assigned to server "
        << sourceHost->getId() << " (use migrateVm() of class DcManager to perform a "
        "proper migration)\n";
    return false;
  }

  ///////////////////////////// REMOVE ////////////////////////////
  std::cout << "Precopy = " << preCopy << "\n";
  std::cout << "\nPREMIGRATION STATE at "<< Scheduler::instance().clock() << "s\n";
  sourceHost->print();
  std::cout << "\n";
  newHost->print();
  std::cout << "\n";
  vmToMigrate->print();
  /////////////////////////////////////////////////////////////////////

  // Check if the host "newHost" has enough available resources to satisfy the migration request
  if(newHost->getVmMonitor()->checkAllocationFeasibility(vmToMigrate) == true){
    // Save the virtual machine pre-migration state
    Vm::vm_state vmPreMigrationState = vmToMigrate->getVmState();
    // If the vm pre-migration state is equals to RUNNING or PAUSED, perform a live migration
    if((vmPreMigrationState == Vm::RUNNING) || (vmPreMigrationState == Vm::PAUSED)){
      liveMig = true;
      // If a post-copy memory migration must be perform, suspend the vm without
      // freeing its virtual used storage
      if(preCopy == false)
        this->updateVmState(vmToMigrate->getId(), Vm::SUSPENDED, false);
    }

    ///////////////////////////////////////////////////////////////////////////////
    // If preCopy == false:                                                      //
    //    The vm is now SUSPENDED, with liveMig == true/false according to       //
    //    vmPreMigrationState, or INACTIVE, with liveMig == false, on sourceHost //
    // If preCopy == true:                                                       //
    //   The vm is now RUNNING/PAUSED, with liveMig == true or                   //
    //   INACTIVE/SUSPENDED, with liveMig == false, on sourceHost                //
    ///////////////////////////////////////////////////////////////////////////////

    // Reserve resources on newHost according to the vm pre-migration state
    bool op1 = newHost->increaseResourceTypeUse(Resource::STORAGE,
        vmToMigrate->getNominalCapacityToUse(Resource::STORAGE));

    ///////////////////////////////////////////////////////////////////////////////////
    // Cold migration                                                                //
    ///////////////////////////////////////////////////////////////////////////////////
    if((liveMig == false) && (op1 == true)){
      //
      std::cout << "\n********************************************************************\n"
          "Migration of Vm " << vmToMigrate->getId() << " on host " << newHost->getId() <<
          " started at " << Scheduler::instance().clock() <<
          "s\n********************************************************************\n\n";
      //
      vmToMigrate->setMigrationState(true);
      //
      if(completeMigration(vmToMigrate, vmPreMigrationState, newHost, preCopy) == true)
        return true;
    }

    ///////////////////////////////////////////////////////////////////////////////////
    // Live migration                                                                //
    ///////////////////////////////////////////////////////////////////////////////////
    else if(liveMig == true){
      //
      std::cout << "\n********************************************************************\n"
          "(Live) Migration of Vm " << vmToMigrate->getId() << " on Pm " << newHost->getId() <<
          " started at t = " << Scheduler::instance().clock() <<
          "s\n********************************************************************\n\n";
      //
      bool op2 = newHost->increaseResourceTypeUse(Resource::CPU,
          vmToMigrate->getNominalCapacityToUse(Resource::CPU));
      bool op3 = newHost->increaseResourceTypeUse(Resource::MEMORY,
          vmToMigrate->getNominalCapacityToUse(Resource::MEMORY));

      // If the resources reservation completes successfully
      if((op1 == true) && (op2 == true) && (op3 == true)){
        //
        Tcl& tcl = Tcl::instance();
        // Create a new MigrationSourceAgent
        tcl.evalc("new Agent/TCP/MigrationSourceAgent");
        MigrationSourceAgent* migSourceAgent =
            (MigrationSourceAgent*) TclObject::lookup(tcl.result());
        // Create a new MigrationSinkAgent
        tcl.evalc("new Agent/TCPSink/MigrationSinkAgent");
        MigrationSinkAgent* migSinkAgent =
            (MigrationSinkAgent*) TclObject::lookup(tcl.result());
        // Attach the MigrationSourceAgent to the Node linked with sourceHost
        tcl.evalf("[Simulator instance] attach-agent %s %s",
            sourceHost->getNode_OTcl().c_str(), migSourceAgent->name());
        // Attach the MigrationSinkAgent to the Node linked with newHost
        tcl.evalf("[Simulator instance] attach-agent %s %s",
            newHost->getNode_OTcl().c_str(), migSinkAgent->name());
        // Connect MigrationSourceAgent and MigrationSinkAgent
        tcl.evalf("[Simulator instance] connect %s %s", migSourceAgent->name(),
              migSinkAgent->name());
        // Set the migSinkAgent parameters util to complete migration
        migSinkAgent->setVmToMigrate(vmToMigrate);
        migSinkAgent->setVmPreMigrationState(vmPreMigrationState);
        migSinkAgent->setHostDestination(newHost);
        migSinkAgent->setPreCopyState(preCopy);
        // To avoid moving an excessive amount of data or clogging the links between
        // sourceHost and newHost, the assumption here is that only one tenth of virtual
        // memory (or one twentieth of virtual memory, in case of post-copy memory migration)
        // must be transferred to restore the vm state on its new destination and that this
        // transfer will be performed using a CBR transmission (on TCP) of 10Mb/s
        unsigned long long dataToTransfer;
        if(preCopy == true)
          dataToTransfer = (unsigned long long) (vmToMigrate->getNominalCapacityToUse(Resource::MEMORY)/10);
        else
          dataToTransfer = (unsigned long long) (vmToMigrate->getNominalCapacityToUse(Resource::MEMORY)/20);
        // Init migSourceAgent with the following transmission parameters:
        // dataToTransfer, 1000Byte, 10Mb/s
        migSourceAgent->init(dataToTransfer, 1000, 10000000);
        // Inform the migSinkAgent of the LastPacketSeqNo to expect
        migSinkAgent->setLastPacketSeqNo(migSourceAgent->getLastPacketSeqNo());
        //
        vmToMigrate->setMigrationState(true);
        // Start the vm memory transfer
        migSourceAgent->startDataTransfer();
        return true;
      }
    } // liveMig == true
  }
  // newHost hasn't enough available resources to satisfy the migration request
  std::cout << "The host " << newHost->getId() << " hasn't enough available resources "
      "to satisfy the migration request of Vm " << vmToMigrate->getId() << "\n";
  return false;

}


bool VmMonitor::completeMigration(Vm* vmToMigrate, Vm::vm_state vmPreMigState,
    Pm* newHost, bool preCopy){

  ///////////////////////////// REMOVE ////////////////////////////
  //std::cout << "\nsourceHost "<< vmToMigrate->getVmMonitor()->getPm()->getId()
  //    << "\nnewHost " << newHost->getId() <<"\npreCopy "<< preCopy << "\nON SOURCE HOST\n";
  //vmToMigrate->print();
  /////////////////////////////////////////////////////////////////////

  Pm* sourceHost = vmToMigrate->getVmMonitor()->getPm();
  // Add the vm to newHost
  newHost->hostedVMs.push_back(vmToMigrate);
  // Update the vmm reference for the migrated vm
  vmToMigrate->setVmMonitor(newHost->getVmMonitor());
  // If a pre-copy memory migration was performed and vmPreMigState is equals to
  // RUNNING or PAUSED, suspend the vm on sourceHost before detach its agents
  if((preCopy == true) && ((vmPreMigState==Vm::RUNNING) || (vmPreMigState==Vm::PAUSED)))
    sourceHost->getVmMonitor()->updateVmState(vmToMigrate->getId(), Vm::SUSPENDED, false);
  // Detach the vm sink agent from the OTcl node linked with sourceHost
  std::string sinkAgent = vmToMigrate->getSinkAgent_OTcl();
  sourceHost->getVmMonitor()->detachAgentFromPm_OTcl(sinkAgent);
  // Attach the vm sink agent with the OTcl node linked with the newHost
  newHost->getVmMonitor()->attachAgentToPm_OTcl(sinkAgent);
  // If the vm has also a list of attached source agents, detach all of these agents from the
  // OTcl node linked with sourceHost and attach them to the OTcl node linked with newHost
  if (vmToMigrate->getSourceAgentList_OTcl().empty() == false){
    std::vector<std::string>::iterator itSource;
    std::vector<std::string> sourceAgents = vmToMigrate->getSourceAgentList_OTcl();
    for(itSource = sourceAgents.begin(); itSource != sourceAgents.end(); itSource++){
      sourceHost->getVmMonitor()->detachAgentFromPm_OTcl(*itSource);
      newHost->getVmMonitor()->attachAgentToPm_OTcl(*itSource);
    }
  }

  // Now that the vm has been correctly added to newHost, decrease the storage resources
  // consumption for sourceHost (if the virtual machine pre-migration state was
  // RUNNING or PAUSED, the other sourceHost resource consumptions have already been
  // updated when the vm state changed to SUSPENDED; in the other cases - i.e. when
  // vmPreMigrationState = SUSPENDED/INACTIVE - they shouldn't be updated)
  if(sourceHost->decreaseResourceTypeUse(Resource::STORAGE,
      vmToMigrate->getUsedCapacity(Resource::STORAGE)) == true) {

    // Remove the vm from sourceHost
    sourceHost->hostedVMs.erase(std::remove(sourceHost->hostedVMs.begin(),
        sourceHost->hostedVMs.end(), vmToMigrate), sourceHost->hostedVMs.end());

    // If the vm pre-migration state was RUNNING or PAUSED, update
    // the vm state using the VMM of newHost (if the vm pre-migration state was
    // equals to SUSPENDED or INACTIVE the vm will retains its pre-migration
    // state (i.e. SUSPENDED or INACTIVE) on its new host (i.e. newHost)
    if((vmPreMigState == Vm::RUNNING) || (vmPreMigState == Vm::PAUSED)){
      // Decrease the resource previously reserved for migration
      bool op1 = newHost->decreaseResourceTypeUse(Resource::CPU,
          vmToMigrate->getNominalCapacityToUse(Resource::CPU));
      bool op2 = newHost->decreaseResourceTypeUse(Resource::MEMORY,
          vmToMigrate->getNominalCapacityToUse(Resource::MEMORY));
      // If the two operations completes successfully
      if((op1 == true) && (op2 == true))
        // Update the vm state on newHost (otherwise the vm will retain its pre-migration state)
        newHost->getVmMonitor()->updateVmState(vmToMigrate->getId(), vmPreMigState, false);
      else
        return false;
    }
    //
    vmToMigrate->setMigrationState(false);
    //
    std::cout << "\n********************************************************************\n"
        "Migration of Vm " << vmToMigrate->getId() << " on host " << newHost->getId() <<
        " completed at t = " << Scheduler::instance().clock() <<
        "s\n********************************************************************\n\n";

    ///////////////////////////// REMOVE ////////////////////////////
    std::cout << "\nON DESTINATION HOST\n";
    vmToMigrate->print();
    sourceHost->print();
    newHost->print();
    /////////////////////////////////////////////////////////////////////

    return true;
  }
  return false;

}


bool VmMonitor::updateVmState(int vmId, Vm::vm_state newState, bool freeStorage){

  std::vector<Vm*>::iterator it;
  double hostAvailCpu = pm->getAvailableCapacity(Resource::CPU);
  double hostAvailMemory = pm->getAvailableCapacity(Resource::MEMORY);
  //
  for(it = pm->hostedVMs.begin(); it != pm->hostedVMs.end(); it++){
    // If the vm has been assigned to this host
    if((*it)->getId()==vmId){
      Vm::vm_state currentState = (*it)->getVmState();
      double vmNomVCpuToUse = (*it)->getNominalCapacityToUse(Resource::CPU);
      double vmNomVMemToUse = (*it)->getNominalCapacityToUse(Resource::MEMORY);
      double vmUsedVCpu = (*it)->getUsedCapacity(Resource::CPU);
      double vmUsedVMem = (*it)->getUsedCapacity(Resource::MEMORY);
      // The vm state won't be updated if it's equal to the current one
      if((*it)->getVmState() == newState){
        std::cout << "\nThe state of vm " << vmId << " is already "
            << Vm::vmStateToString(newState) << "\n";
        return false;

      /////////////////////////////////////////////////////////////////////////
      // If the request is to change from a state where memory and cpu are   //
      // not being used to a state where there is resource consumption       //
      /////////////////////////////////////////////////////////////////////////
      }else if((currentState == Vm::INACTIVE && newState != Vm::SUSPENDED) ||
           (currentState == Vm::SUSPENDED && newState != Vm::INACTIVE)){
        // ... check if the host has enough available resources to satisfy the request
        if((vmNomVCpuToUse <= hostAvailCpu) && (vmNomVMemToUse <= hostAvailMemory)){
          bool op1 = pm->increaseResourceTypeUse(Resource::CPU, vmNomVCpuToUse);
          bool op2 = pm->increaseResourceTypeUse(Resource::MEMORY, vmNomVMemToUse);
          // If both the operations succeed, the state of the vm will be updated
          // and all its applications (if any) re/activated
          if(op1 && op2){
            // At this point its always safe to call setVmState;
            // for this reason the method is called without checking its return value
            (*it)->setVmState(newState, freeStorage);
            //
            std::string sinkAgent = (*it)->getSinkAgent_OTcl();
            // Re/Start the Sink monitoring
            this->activateVmSourceAgent_OTcl(sinkAgent);
            // If the vm has also a list of attached source agents, re/start all
            // the TrafficGenerators linked with these agents
            if ((*it)->getSourceAgentList_OTcl().empty() == false){
              std::vector<std::string>::iterator itSource;
              std::vector<std::string> sourceAgents = (*it)->getSourceAgentList_OTcl();
              for(itSource = sourceAgents.begin(); itSource != sourceAgents.end(); itSource++)
                this->startTrafficGenerator_OTcl(*itSource);
            }
            return true;
          }
        }else
          std::cerr << "The host hasn't enough available resources to update the vm state; "
              << "suspend or remove another vm before try again\n";
        return false;

      ///////////////////////////////////////////////////////////////////////
      // If the request is to change from a state where memory and cpu are //
      // actively used to a state where there is no resource consumption   //
      ///////////////////////////////////////////////////////////////////////
      }else if((currentState == Vm::RUNNING && newState != Vm::PAUSED) ||
            (currentState == Vm::PAUSED && newState != Vm::RUNNING)) {
        //
        bool op1 = pm->decreaseResourceTypeUse(Resource::CPU, vmUsedVCpu);
        bool op2 = pm->decreaseResourceTypeUse(Resource::MEMORY, vmUsedVMem);
        // If both the operations succeed, the state of the vm will be updated
        if(op1 && op2){
          // At this point it's always safe to call setVmState;
          // for this reason the method is called without checking its return value
          (*it)->setVmState(newState, freeStorage);
          //
          std::string sinkAgent = (*it)->getSinkAgent_OTcl();
          // Stop the Sink Monitoring (only for the vm, not the sinkAgent)
          this->deactivateVmSourceAgent_OTcl(sinkAgent);
          // If the vm has also a list of attached source agents, stop all
          // the TrafficGenerators linked with these agents
          if ((*it)->getSourceAgentList_OTcl().empty() == false){
            std::vector<std::string>::iterator itSource;
            std::vector<std::string> sourceAgents = (*it)->getSourceAgentList_OTcl();
            for(itSource = sourceAgents.begin(); itSource != sourceAgents.end(); itSource++)
              this->stopTrafficGenerator_OTcl(*itSource);
          }
          return true;
        }
        return false;

      ///////////////////////////////////////////////////////////////////////////////////
      // If the request is to change from a state where memory and cpu are             //
      // not being used to another state where there is no resource consumption or     //
      // from a state where memory and cpu are actively used to another active state   //
      // simply update the vm state                                                    //
      ///////////////////////////////////////////////////////////////////////////////////
      }else{
        // At this point its always safe to call setVmState;
        // for this reason the method is called without checking its return value
        (*it)->setVmState(newState, freeStorage);
        return true;
      }
    } // end if((*it)->getId()==vmId)
  } // end for
  std::cerr << "There isn't any vm with id equals to " << vmId << " in host "
      << pm->getId() << "\n";
  return false;

}

bool VmMonitor::updateVmResourceTypeUse(int vmId, Resource::res_type vresType, int perc){

  std::vector<Vm*>::iterator it;
  for(it = pm->hostedVMs.begin(); it != pm->hostedVMs.end(); it++){
    // If the vm has been previously assigned to this host
    if((*it)->getId()==vmId){
      // Get the current vm used capacity for vresType
      double previouslyUsedCap = (*it)->getUsedCapacity(vresType);
      // Try to update the percentage of use of the chosen virtual resources
      if((*it)->updateVResourceTypeUse(vresType, perc) == false)
        return false;
      // If the update completes successfully, check the vm state
      // and if the vm is currently active, update the used resource
      // of the host pm, according to the previously update
      if(((*it)->getVmState() == Vm::RUNNING) || ((*it)->getVmState() == Vm::PAUSED)){
        // TODO: Check the security of the following operations
        pm->decreaseResourceTypeUse(vresType, previouslyUsedCap);
        double newUsedCap = (*it)->getUsedCapacity(vresType);
        pm->increaseResourceTypeUse(vresType, newUsedCap);
      }
      return true;
    }
  }
  std::cerr << "There isn't any vm with id equals to " << vmId << " in server "
      << pm->getId() << "\n";
  return false;

}


void VmMonitor::attachAgentToPm_OTcl(std::string agent){

  //
  Tcl::instance().evalf("[Simulator instance] attach-agent %s %s",
      pm->getNode_OTcl().c_str(), agent.c_str());

}

void VmMonitor::detachAgentFromPm_OTcl(std::string agent){

  //
  Tcl::instance().evalf("[Simulator instance] detach-agent %s %s",
      pm->getNode_OTcl().c_str(), agent.c_str());

}

void VmMonitor::activateVmSourceAgent_OTcl(std::string agent){

  Tcl::instance().evalf("%s set active 1", agent.c_str());

}

void VmMonitor::deactivateVmSourceAgent_OTcl(std::string agent){

  Tcl::instance().evalf("%s set active 0", agent.c_str());

}

void VmMonitor::startTrafficGenerator_OTcl(std::string agent){

  Tcl::instance().evalf("[%s get-tg] start", agent.c_str());

}

void VmMonitor::stopTrafficGenerator_OTcl(std::string agent){

  Tcl::instance().evalf("[%s get-tg] stop", agent.c_str());

}
