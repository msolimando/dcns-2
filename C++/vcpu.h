/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef VCPU_H_
#define VCPU_H_

#include <iostream>
#include <map>
#include <stdexcept>
#include <cstdlib>
#include <string>
#include <vector> 

#include <tclcl.h>

#include "util.h"
#include "vresource.h"
#include "vresourceunit.h"

/** */
class VCpu : public VResource, public TclObject {

public:

  //
  enum vcpu_type {CPU_1CORE, CPU_2CORE};

  /** Class constructor */
  VCpu(int vcpuId, std::string vcpuType);

  /** Class destructor */
  ~VCpu();

  /** Returns the virtual cpu type */
  vcpu_type getVCpuType();

  /** */
  void print() override;

  /** Defines the commands available for the class objects in the OTcl domain */
  int command(int argc, const char*const* argv);

private:

  /** */
  vcpu_type stringToVCpuType(std::string type);

  /** */
  std::string vcpuTypeToString(vcpu_type type);

  /** */
  vcpu_type vcpuType;

  /** */
  static std::map<std::string, vcpu_type> vcpuTypeCheck;

};

#endif /* VCPU_H_ */
