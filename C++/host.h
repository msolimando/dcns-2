/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef HOST_H_
#define HOST_H_

#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "resource.h"
#include "util.h"

/** */
class Host {

public:

  //
  typedef std::multimap<Resource::res_type, Resource*> ResMultimap;

	/** Host destructor */
	virtual ~Host();

	/** */
	int getId();

	/** */
	double getNominalCapacity(Resource::res_type type);

	/** */
	double getUsedCapacity(Resource::res_type type);

	/** */
	double getAvailableCapacity(Resource::res_type type);

	/** */
	virtual unsigned long long getDataSent() = 0;

	/** */
	virtual unsigned long long getDataReceived() = 0;

	/** */
	virtual void printNoDetails() = 0;

	/** */
	virtual void print() = 0;

protected:

	/** */
	virtual bool setResourceTypeUse(Resource::res_type resType, double update);

	/** */
	bool increaseResourceTypeUse(Resource::res_type resType, double increase);

	/** */
	bool decreaseResourceTypeUse(Resource::res_type resType, double decrease);

	/** ID associated with this host */
	int id;

	/** */
	ResMultimap resourcesList;

};

#endif  /* HOST_H_ */
