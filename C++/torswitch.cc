/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#include "torswitch.h"

static class ToRSwitchClass : public TclClass {
public:
  ToRSwitchClass() : TclClass("ToRSwitch") {}
	TclObject* create(int argc, const char*const*argv) {
	  //OTcl domain: set newToRSwitch [new ToRSwitch {id switch_type}]
	  return (new ToRSwitch(atoi(argv[4]), argv[5]));
	}
} class_torswitch;

////////////

ToRSwitch::ToRSwitch(int switchId, std::string switchType) :
    Switch(switchId, switchType), rackId(-1) {

}

ToRSwitch::~ToRSwitch(){

}

int ToRSwitch::getRackId(){

  return this->rackId;

}

void ToRSwitch::setRackId(int rackId){

  this->rackId = rackId;

}

void ToRSwitch::print(){

  std::stringstream rxLoadPerc;
  std::stringstream txLoadPerc;
  // To print the percentages of RxLinkLoad and TxLinkLoad with only two digits
  // after the decimal point
  rxLoadPerc << std::setprecision(2);
  txLoadPerc << std::setprecision(2);
  //
  std::string intervalBeg = getFormattedString(this->lastStatsInterval.first, util::SECONDS);
  std::string intervalEnd = getFormattedString(this->lastStatsInterval.second, util::SECONDS);
  //
  std::cout << "=============================================================="
      << "==============================================================\n"
      << "ToRSwitch " << getId() << ": "
      << "Rack_Id = " << getRackId() << ", "
      << "State = " << switchStateToString(getSwitchState()) << ", "
      << "Stats Interval = [" << intervalBeg << "," << intervalEnd << "]\n"
      << "             nPorts = " << nPorts;
  //
  std::map<port_type, int>::iterator itP;
  for(itP = portMap.begin(); itP != portMap.end(); itP++)
    std::cout << " [" << portTypeToString((*itP).first) << " = " << (*itP).second << "]";
  std::cout << "\n============================================================= Ports"
      << " ========================================================\n";
  //
  std::vector<Port*>::iterator it;
  // Iterate over all switch ports
  for(it = switchPorts.begin(); it != switchPorts.end(); it++){
    //
    rxLoadPerc.str("");
    rxLoadPerc << ((*it)->rxLinkLoad/(*it)->bandwidth)*100;
    txLoadPerc.str("");
    txLoadPerc << ((*it)->txLinkLoad/(*it)->bandwidth)*100;
    //
    std::cout << "- [" << portTypeToString((*it)->ptype) << "] "
        << "Port " << (*it)->portId
        << ": Bandwidth = " << getFormattedString(((*it)->bandwidth)*8, util::BITPS)
        << ", RxLinkLoad = " << getFormattedString(((*it)->rxLinkLoad*8), util::BITPS)
        << "(" << rxLoadPerc.str() << "%)"
        << ", TxLinkLoad = " << getFormattedString(((*it)->txLinkLoad*8), util::BITPS)
        << "(" << txLoadPerc.str() << "%)\n"
        << "\t\t     RxBytes = " << getFormattedString((*it)->rxBytes, util::BYTE)
        << ", RxByteDrops = " << getFormattedString((*it)->rxByteDrops, util::BYTE)
        << ", RxPackets = "<< (*it)->rxPackets
        << ", RxPacketDrops = " << (*it)->rxPacketDrops <<",\n"
        << "\t\t     TxBytes = "<< getFormattedString((*it)->txBytes, util::BYTE)
        << ", TxByteDrops = " << getFormattedString((*it)->txByteDrops, util::BYTE)
        << ", TxPackets = "<< (*it)->txPackets
        << ", TxPacketDrops = " << (*it)->txPacketDrops <<"\n"
        << "--------------------------------------------------------------"
        << "--------------------------------------------------------------\n";
  }
  std::cout << "=============================================================="
      << "==============================================================\n";

}
