/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RESOURCE_H_
#define RESOURCE_H_

#include <iostream>
#include <map>
#include <vector>
#include <string>

#include "resourceunit.h"

/** */
class Resource {

public:

  //
  enum res_type {CPU, MEMORY, STORAGE, NETWORKING};

	/** */
	virtual ~Resource();

	/** Returns the id of the resource */
	int getId();

	/** */
	bool updateId(int newId);

	/** Returns the type of the resource (e.g. CPU, MEMORY, etc.)*/
	res_type getResType();

	/** Returns the nominal capacity of the resource */
	double getNominalCapacity();

	/** Returns the currently used capacity for this resource */
	double getUsedCapacity();

	/** */
	bool setUsedCapacity(double cap);

	/** */
	bool increaseUsedCapacity(double increase);

	/** */
	bool decreaseUsedCapacity(double decrease);

	/** Returns the available capacity of the resource */
	double getAvailableCapacity();

	/** Returns the percentage of used computational power */
	double getUtilization();

	/** Returns the id of the machine to which this resource belongs */
	int getHostId();

	/** Specifies the id of the machine to which this resource belongs */
	void setHostId(int id);

	/** Returns the list of unit resources */
	std::vector<ResourceUnit*> getResUnitList();

	/** Adds a new ResourceUnit to the list of unit resources */
	void addResUnit(ResourceUnit* unit);

	/** */
	virtual void print() = 0;

	/** */
	static res_type stringToResType(std::string type);

	/** */
	static std::string resTypeToString(res_type type);

protected:

	/** Resource id*/
	int resId;

	/** */
	res_type resType;

	/** */
	double nominalCap;

	/** List of  */
	std::vector<ResourceUnit*> resUnitList;

	/** The host to which this resource belongs */
	int hostId;

private:

	/** */
	static std::map<std::string, res_type> resTypeCheck;

};

#endif /* RESOURCE_H_ */
