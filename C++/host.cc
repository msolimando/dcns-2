/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */

#include "host.h"

Host::~Host(){
	
  this->resourcesList.~multimap();

}

int Host::getId(){

  return this->id;

}

double Host::getNominalCapacity(Resource::res_type type){

  double res = 0;
  ResMultimap::iterator it;
  // Get the iterators to the beginning and the end of the group with key equals to "type"
  std::pair<ResMultimap::iterator, ResMultimap::iterator> range = resourcesList.equal_range(type);
  // Iterate over all map elements with key equals to "type"
  for(it = range.first; it != range.second; it++)
    res += (*it).second->getNominalCapacity();
  return res;

}

double Host::getUsedCapacity(Resource::res_type type){

  double res = 0;
  ResMultimap::iterator it;
  // Get the iterators to the beginning and the end of the group with key equals to "type"
  std::pair<ResMultimap::iterator, ResMultimap::iterator> range = resourcesList.equal_range(type);
  // Iterate over all map elements with key equals to "type"
  for(it = range.first; it != range.second; it++)
    res += (*it).second->getUsedCapacity();
  return res;

}

double Host::getAvailableCapacity(Resource::res_type type){

  return (getNominalCapacity(type)-getUsedCapacity(type));

}

bool Host::setResourceTypeUse(Resource::res_type resType, double update){

  ResMultimap::iterator it;
  // Get the iterators to the beginning and the end of the group with key equals to "resType"
  std::pair<ResMultimap::iterator, ResMultimap::iterator> range = resourcesList.equal_range(resType);
  //
  if(update < 0){
    std::cerr << "Error in updating the " << Resource::resTypeToString(resType)
        << " used capacity: the new capacity value must be greater than or equal to zero";
    return false;
  // Set all used capacity of the Resources of type resType to zero
  }else if (update == 0){
    // Iterate over all map elements with key equals to "resType"
    for(it = range.first; it != range.second; it++)
      (*it).second->setUsedCapacity(0);
    return true;
  // If the "update" value is greater than zero, update the used capacity
  // of the Resources of type resType only if together they have enough nominal capacity
  }else if(this->getNominalCapacity(resType) >= update){
    for(it = range.first; it != range.second; it++){
      double nomCap = (*it).second->getNominalCapacity();
      // If the nominal capacity of the resource is greater than
      // the "update" value to be assigned
      if(nomCap >= update){
        (*it).second->setUsedCapacity(update);
        return true;
      }else{
        (*it).second->setUsedCapacity(nomCap);
        update-=nomCap;
      }
    }
  }
  std::cerr << "Error in updating the "<< Resource::resTypeToString(resType)
      << " used capacity: the update value is greater than its nominal capacity";
  return false;

}

bool Host::increaseResourceTypeUse(Resource::res_type resType, double increase){

  ResMultimap::iterator it;
  // Get the iterators to the beginning and the end of the group with key equals to "resType"
  std::pair<ResMultimap::iterator, ResMultimap::iterator> range = resourcesList.equal_range(resType);
  // Increase the used capacity of the Resources of type resType only if
  // together they have enough available capacity
  if(this->getAvailableCapacity(resType) >= increase){
    // Iterate over all map elements with key equals to "type"
    for(it = range.first; it != range.second; it++){
      double availCap = (*it).second->getAvailableCapacity();
      // If the available capacity of the current resource is greater than
      // the "increase" value, update only the used capacity of this resource...
      if(availCap >= increase)
        return (*it).second->increaseUsedCapacity(increase);
      // ...otherwise update also the used capacity of the following resource
      else if((*it).second->increaseUsedCapacity(availCap)==true)
        increase-=availCap;
    }
  }
  std::cerr << "Error in updating the "<< Resource::resTypeToString(resType)
        << " used capacity: the update value is greater than its available capacity";
  return false;

}

bool Host::decreaseResourceTypeUse(Resource::res_type resType, double decrease){

  ResMultimap::iterator it;
  // Get the iterators to the beginning and the end of the group with key equals to "resType"
  std::pair<ResMultimap::iterator, ResMultimap::iterator> range = resourcesList.equal_range(resType);
  // Decrease the used capacity of the Resources of type resType only if
  // the current used capacity of the host for that resource type  is greater than zero
  if(this->getUsedCapacity(resType) > 0){
    // Iterate over all map elements with key equals to "type"
    for(it = range.first; it != range.second; it++){
      double usedCap = (*it).second->getUsedCapacity();
      // If the used capacity of the current resource is greater than zero decrease
      // its value, otherwise check the following resource (remember that a dcHost
      // can have multiple resources of the same type and some of them may be inactive)
      if(usedCap>0){
        // If the used capacity of the current resource is greater than
        // the "decrease" value, update only the used capacity of this resource...
        if(usedCap >= decrease)
          return (*it).second->decreaseUsedCapacity(decrease);
        // ...otherwise update also the used capacity of the following resource
        else if((*it).second->decreaseUsedCapacity(usedCap)==true)
          decrease-=usedCap;
      }
    }
  }
  return false;

}
