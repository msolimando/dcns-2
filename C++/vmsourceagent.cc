/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#include "vmsourceagent.h"

/** */
static class VmSourceAgentClass : public TclClass {
public:
  VmSourceAgentClass() : TclClass("Agent/UDP/VmSourceAgent") {}
	TclObject* create(int argc, const char*const* argv) {
	  //OTcl domain: set newVmSource [new Agent/UdpAgent/VmSourceAgent]
		return (new VmSourceAgent());
	}
} class_vmsourceagent;

////////////

VmSourceAgent::VmSourceAgent() : UdpAgent(), vmId(-1), sinkVmId(-1), bytesTx(0) {

  bind("vmId", &vmId);
  bind("sinkVmId", &sinkVmId);

}

VmSourceAgent::VmSourceAgent(packet_t type) : UdpAgent(type), vmId(-1), sinkVmId(-1), bytesTx(0) {

  bind("vmId", &vmId);
  bind("sinkVmId", &sinkVmId);

}

VmSourceAgent::~VmSourceAgent(){

}

void VmSourceAgent::setVmId(int vmId){

  this->vmId = vmId;

}

int VmSourceAgent::getVmId(){

  return this->vmId;

}

void VmSourceAgent::setSinkVmId(int sinkVmId){

  this->sinkVmId = sinkVmId;

}

int VmSourceAgent::getSinkVmId(){

  return this->sinkVmId;

}

unsigned long long VmSourceAgent::getTxBytes(){

  return this->bytesTx;

}

void VmSourceAgent::sendmsg(int nbytes, AppData* data, const char* flags){

  // Update the bytes sent by the Agent
  bytesTx += nbytes;
  // Call the member function defined in the parent class (Agent) to complete the send
  UdpAgent::sendmsg(nbytes, data, flags);

}

/** OTcl commands:
 * commands-list {} -
 * get-tg {}
 * get-bytesTx {}
 **/
int VmSourceAgent::command(int argc, const char*const* argv){

  Tcl& tcl = Tcl::instance();
  //
  if(argc == 2) {
    if(strcmp(argv[1], "commands-list") == 0){
      std::cout << "- commands-list {}\n"
                   "- get-tg {}\n"
                   "- get-bytesTx {}\n";
      return (TCL_OK);
    //
    }else if(strcmp(argv[1], "get-tg") == 0) {
      const char * tgName_OTcl = this->app_->name();
      if(tgName_OTcl != NULL){
        tcl.evalf("set tg %s", tgName_OTcl);
        return (TCL_OK);
      }else{
        std::cerr << "There is no TrafficGenerator attached to this Agent";
        return (TCL_ERROR);
      }
    //
    }else if(strcmp(argv[1], "get-bytesTx") == 0) {
      tcl.evalf("set txBytes %llu", getTxBytes());
      return (TCL_OK);
    }
  //
  }
  return (UdpAgent::command(argc, argv));

}
