/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef UTIL_H_
#define UTIL_H_

#include <iomanip>
#include <map>
#include <sstream>
#include <string>

/** */
namespace util{

  /** */
  enum format_type {BIT, BITPS, BYTE, BYTEPS, FLOPS, MIPS, SECONDS};

  /** */
  const std::map<format_type, std::string> baseUnits = {
      {BIT, "b"},
      {BITPS, "bps"},
      {BYTE, "B"},
      {BYTEPS, "Bps"},
      {FLOPS, "FLOPS"},
      {MIPS, "MIPS"},
      {SECONDS, "s"}
  };

  /** */
  template<typename T>
  std::string getFormattedString(T value, format_type type){

    std::stringstream st;
    //
    if(type == MIPS){
      st << ((double)value)/1000000 << "MIPS";
      // Change std::setprecision() value to display more or less digits after the decimal point
      //st << std::fixed << std::setprecision(2) << ((double)value)/1000000 << "MIPS";
    //
    }else if(type==FLOPS || type==BIT || type==BITPS || type==BYTE || type==BYTEPS){
      // KILO, MEGA, GIGA, TERA
      if((value/1000) < 1000){
        st << std::fixed << std::setprecision(2) << ((double)value)/1000 << "k"
           << baseUnits.find(type)->second;
      }else if((value/1000000) < 1000){
        st << std::fixed << std::setprecision(2) << ((double)value)/1000000 << "M"
           << baseUnits.find(type)->second;
      }else if((value/1000000000) < 1000){
        st << std::fixed << std::setprecision(2) << ((double)value)/1000000000 << "G"
           << baseUnits.find(type)->second;
      }else{
        st << std::fixed << std::setprecision(2) << ((double)value)/1000000000000 << "T"
           << baseUnits.find(type)->second;
      }
    //
    }else if (type == SECONDS){
      // MILLI, MICRO, NANO, PICO
      if ((value < 1) && (value != 0)){
        if((value*1000) >= 1)
          st << std::setprecision(3) << value*1000 << "ms";
        else if((value*1000000) >= 1)
          st << std::setprecision(3) << value*1000000 << "µs";
        else
          st << std::fixed << std::setprecision(4) << value*1000000000 << "ns";
      // SECONDS, MINUTES, HOURS
      }else{ // value >= 1
        unsigned int vl = (unsigned int) value;
        if ((value/60) < 1){  //SECONDS
          st << value << "s";
        } else if ((value/3600) < 1){ // MINUTES
          unsigned int minutes = vl/60;
          unsigned int seconds = vl % 60;
          st << minutes << "m:" << seconds << "s";
        } else {  // HOURS
          unsigned int hours = vl/3600;
          unsigned int minutes = (vl/60) % 60;
          unsigned int seconds = value - (hours*3600) - (minutes*60);
          st << hours << "h:" << minutes << "m:" << seconds << "s";
        }
      }
    // Return the value as it is
    }else{
      st << value;
    }
    return st.str();

  }// end getFormattedString()

}

#endif /* UTIL_H_ */
