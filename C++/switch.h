/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SWITCH_H_
#define SWITCH_H_

#include <iostream>
#include <string>
#include <vector>

#include <node.h>
#include <queue.h>
#include <tclcl.h>
#include <timer-handler.h>

#include "pm.h"
#include "ullqueuemonitor.h"
#include "util.h"

class DcManager;

/** Each object of this class represents a switch into the datacenter architecture */
class Switch : public TclObject, public TimerHandler {

public:

  //
  enum port_type {PT_10MbE, PT_100MbE, PT_1GbE, PT_10GbE, PT_40GbE, PT_100GbE};

  //
  typedef std::pair<double, double> StatsInterval;

  // The set of switch possible states
  enum switch_state {ON, OFF};

  // The set of switch supported types
  enum switch_type{SWITCH_1Gbps, SWITCH_10Gbps, SWITCH_40Gbps};

  //
  struct Port {

    /** */
    int portId;

    /** */
    port_type ptype;

    /** Bandwidth capacity in Bps */
    double bandwidth;

    /** */
    bool active;

    /** */
    ULLQueueMonitor* qmonRx;

    /** */
    ULLQueueMonitor* qmonTx;

    /** */
    unsigned long long rxPackets;

    /** */
    unsigned long long rxBytes;

    /** */
    unsigned long long rxPacketDrops;

    /** */
    unsigned long long rxByteDrops;

    /** */
    unsigned long long txPackets;

    /** */
    unsigned long long txBytes;

    /** */
    unsigned long long txPacketDrops;

    /** */
    unsigned long long txByteDrops;

    /** Ratio between the number of bytes received in the last stats interval
     *  and the size of this interval */
    double rxLinkLoad;

    /** Ratio between the number of bytes transmitted in the last stats interval
     *  and the size of this interval */
    double txLinkLoad;

    /** The Node object of the device connected to this port*/
    std::string node_OTcl;

  };
	
  /** Switch constructor */
  Switch(int switchId, std::string switchType);

  /** Switch destructor */
  virtual ~Switch();

  /** */
  int getId();

  /** */
  switch_type getSwitchType();

  /** */
  switch_state getSwitchState();

  /** */
  bool setSwitchState(switch_state newState);

  /** */
  std::string getNode_OTcl();

  /** */
  std::map<port_type, int> getPortMap();

  /** */
  int getNumberOfSwitchPorts();

  /** */
  void collectStatistics();

  /** */
  bool scheduleLinkFault(double time, int portId);

  /** */
  bool scheduleLinkReactivation(double time, int portId);

  /** */
  bool addLinkToPort(int portId, Pm* pm);

  /** */
  bool addLinkToPort(int portId, Switch* otherSwitch, int otherSwitchPortId);

  /** */
  virtual void print();

  /** */
  virtual int command(int argc, const char*const* argv);

  /** */
  static switch_state stringToSwitchState(std::string type);

  /** */
  static std::string switchStateToString(switch_state state);

  /** */
  static port_type stringToPortType(std::string type);

  /** */
  static std::string portTypeToString(port_type type);

protected:

  /** */
  void expire(Event* e) override;

  /** */
  void blockLinkQueue_OTcl(Port* p);

  /** */
  void unblockLinkQueue_OTcl(Port* p);

	/** */
	switch_type stringToSwitchType(std::string type);

	/** */
	std::string switchTypeToString(switch_type type);

	/** Switch id */
	int id;

	/** The switch type */
	switch_type switchType;

	/** */
	switch_state switchState;

	/** */
	std::string node_OTcl;

	/** */
	std::map<port_type, int> portMap;

	/** */
	int nPorts;

	/** */
	std::vector<Port*> switchPorts;

  /** Interval for updating switch statistics */
  double samplingPeriod;

  /** */
  StatsInterval lastStatsInterval;

	/** */
	static std::map<std::string, switch_state> switchStateCheck;

	/** */
	static std::map<std::string, switch_type> switchTypeCheck;

	/** */
	static std::map<std::string, port_type> portTypeCheck;

};

#endif /* SWITCH_H_ */
