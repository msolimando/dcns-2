/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef DCMANAGER_H_
#define DCMANAGER_H_

#include <iostream>
#include <vector>

#include <tclcl.h>

#include "pm.h"
#include "rack.h"
#include "switch.h"
#include "vm.h"

class DcManager : public TclObject {

public:

  static DcManager* Instance();

  /** */
	//~DcManager();
	
	/** */
	std::vector<Pm*> getPmList();

	/** */
	void addPmToPmList(Pm* pm);

	/** */
	void printPmList();

	/** */
	std::vector<Rack*> getRackList();

	/** */
	void addRackToRackList(Rack* rack);

	/** */
	void printRackList();

	/** */
	std::vector<Switch*> getSwitchList();

	/** */
	void addSwitchToSwitchList(Switch* sw);

	/** */
	void printSwitchList();

	/** */
	std::vector<Vm*> getVmList();

	/** */
	void addVmToVmList(Vm* vm);

	/** */
	void printVmList();

	/** */
	bool migrateVm(Vm* vm, Pm* newHost, bool preCopy);

	/** */
	int command(int argc, const char*const* argv) override;

private:
	
	/** */
	DcManager() {}

	/** */
	static DcManager* dcM_instance;

	/** */
	std::vector<Pm*> pmList;

	/** */
	std::vector<Rack*> rackList;

  /** */
	std::vector<Switch*> switchList;
	
	/** */
	std::vector<Vm*> vmList;

};

#endif /* DCMANAGER_H_ */
