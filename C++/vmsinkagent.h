/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef VMSINKAGENT_H_
#define VMSINKAGENT_H_

#include <iostream>
#include <vector>

#include <agent.h>
#include <packet.h>
#include <rtp.h>
#include <tclcl.h>

/** */
class VmSinkAgent : public Agent {

public:

  /** */
  VmSinkAgent();

  /** */
  ~VmSinkAgent();

  /** */
  void setVmId(int vmId);

  /** */
  int getVmId();

  /** */
  void setActive(bool state);

  /** */
  bool isActive();

  /** */
  unsigned long long getVmRxBytes();

  /** */
  unsigned long long getVmRxPkts();

  /** */
  unsigned long long getRxBytes();

  /** */
  unsigned long long getRxPkts();

  /** */
  unsigned long long getLostBytes();

  /** */
  void addSourceVmId(int sourceVmId);

  /** */
  std::vector<int> getSourceVmIds();

  /** */
  void recv(Packet* pkt, Handler* hdl) override;

  /** */
  int command(int argc, const char*const* argv) override;

private:

  /** */
  int vmId;

  /** */
  int active;

  /** */
  unsigned long long bytesRx;

  /** */
  unsigned long long pktsRx;

  /** */
  std::vector<int> sourceVmIds;

  // Loss Monitor variables

  unsigned long long bytes_;

  unsigned long long npkts_;

  unsigned long long nlost_;

  int expected_;

  int seqno_;

  double last_packet_time_;

};

#endif /* VMSINKAGENT_H_ */
