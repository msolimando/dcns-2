/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef VNETWORKING_H_
#define VNETWORKING_H_

#include <iostream>
#include <map>
#include <stdexcept>
#include <cstdlib>
#include <string>
#include <vector> 

#include <tclcl.h>

#include "util.h"
#include "vresource.h"
#include "vresourceunit.h"

/** */
class VNetworking : public VResource, public TclObject {

public:

  //
  enum vnet_type {NET_10MbE, NET_100MbE, NET_1GbE};
	
	/** Class constructor */
  VNetworking(int vnetId, std::string vnetType);
	
	/** Class destructor */
	~VNetworking();

	/** Returns the virtual networking type */
	vnet_type getVNetType();

	/** */
	void print() override;

	/** Defines the commands available for the class objects in the OTcl domain */
	int command(int argc, const char*const* argv);

private:

	/** */
	vnet_type stringToVNetType(std::string type);

	/** */
	std::string vnetTypeToString(vnet_type type);

	/** */
	vnet_type vnetType;

	/** */
	static std::map<std::string, vnet_type> vnetTypeCheck;

};

#endif /* VNETWORKING_H_ */
