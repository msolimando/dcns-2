/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef VMMONITOR_H_
#define VMMONITOR_H_

#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "host.h"
#include "migrationsourceagent.h"
#include "migrationsinkagent.h"
#include "presource.h"
#include "vm.h"
#include "util.h"

class Pm;

/** */
class VmMonitor {

public:

	/** VmMonitor constructor */
	VmMonitor();

	/** VmMonitor destructor */
	~VmMonitor();

	/** */
	Pm* getPm();

	/** */
	bool setPm(Pm* pm);

	/** */
	int getPmId();

	/** Check host spare resources for a new vm allocation */
	bool checkAllocationFeasibility(Vm* vm);

	/** */
	bool addVm(Vm* vm);

	/** */
	bool removeVm(int vmId);

	/** */
	bool startMigration(Vm* vmToMigrate, Pm* newHost, bool preCopy);

	/** */
	bool completeMigration(Vm* vmToMigrate, Vm::vm_state vmPreMigState, Pm* newHost, bool preCopy);

	/** */
	bool updateVmState(int vmId, Vm::vm_state newState, bool freeStorage);

	/** */
	bool updateVmResourceTypeUse(int vmId, Resource::res_type vresType, int perc);

private:

	/** */
	void attachAgentToPm_OTcl(std::string agent);

	/** */
	void detachAgentFromPm_OTcl(std::string agent);

	/** */
	void activateVmSourceAgent_OTcl(std::string agent);

	/** */
	void deactivateVmSourceAgent_OTcl(std::string agent);

	/** */
	void startTrafficGenerator_OTcl(std::string agent);

	/** */
	void stopTrafficGenerator_OTcl(std::string agent);

	/** The pm in which the hypervisor is installed */
	Pm* pm;

};

#endif  /* VMMONITOR_H_ */
