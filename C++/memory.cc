/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#include "memory.h"

/** */
static class MemoryClass : public TclClass {
public:
	MemoryClass() : TclClass("Memory") {}
	TclObject* create(int argc, const char*const* argv) {
	  return (new Memory(atoi(argv[4]), argv[5]));
	}
} class_memory;

std::map<std::string, Memory::mem_type> Memory::memTypeCheck = {
   {"MEMORY_8GB", Memory::MEMORY_8GB},
   {"MEMORY_16GB", Memory::MEMORY_16GB},
   {"MEMORY_32GB", Memory::MEMORY_32GB},
   {"MEMORY_64GB", Memory::MEMORY_64GB},
   {"MEMORY_128GB", Memory::MEMORY_128GB},
};

////////////

Memory::Memory(int memId, std::string memType) {

  this->resId = memId;
  this->resType = Resource::MEMORY;
  this->memType = stringToMemType(memType);
  this->nominalCap = 0;
  this->hostId = -1;
  //
  switch(this->memType){
  case MEMORY_8GB:{
    this->addResUnit(new PResourceUnit(1, 4000000000)); //4GB
    this->addResUnit(new PResourceUnit(2, 4000000000)); //4GB
    break;
  }
  case MEMORY_16GB:{
    this->addResUnit(new PResourceUnit(1, 8000000000)); //8GB
    this->addResUnit(new PResourceUnit(2, 8000000000)); //8GB
    break;
  }
  case MEMORY_32GB:{
    this->addResUnit(new PResourceUnit(1, 8000000000)); //8GB
    this->addResUnit(new PResourceUnit(2, 8000000000)); //8GB
    this->addResUnit(new PResourceUnit(3, 8000000000)); //8GB
    this->addResUnit(new PResourceUnit(4, 8000000000)); //8GB
    break;
  }
  case MEMORY_64GB:{
    this->addResUnit(new PResourceUnit(1, 8000000000)); //8GB
    this->addResUnit(new PResourceUnit(2, 8000000000)); //8GB
    this->addResUnit(new PResourceUnit(3, 8000000000)); //8GB
    this->addResUnit(new PResourceUnit(4, 8000000000)); //8GB
    this->addResUnit(new PResourceUnit(5, 8000000000)); //8GB
    this->addResUnit(new PResourceUnit(6, 8000000000)); //8GB
    this->addResUnit(new PResourceUnit(7, 8000000000)); //8GB
    this->addResUnit(new PResourceUnit(8, 8000000000)); //8GB
    break;
  }
  case MEMORY_128GB:{
    this->addResUnit(new PResourceUnit(1, 8000000000)); //8GB
    this->addResUnit(new PResourceUnit(2, 8000000000)); //8GB
    this->addResUnit(new PResourceUnit(3, 8000000000)); //8GB
    this->addResUnit(new PResourceUnit(4, 8000000000)); //8GB
    this->addResUnit(new PResourceUnit(5, 8000000000)); //8GB
    this->addResUnit(new PResourceUnit(6, 8000000000)); //8GB
    this->addResUnit(new PResourceUnit(7, 8000000000)); //8GB
    this->addResUnit(new PResourceUnit(8, 8000000000)); //8GB
    this->addResUnit(new PResourceUnit(9, 8000000000)); //8GB
    this->addResUnit(new PResourceUnit(10, 8000000000)); //8GB
    this->addResUnit(new PResourceUnit(11, 8000000000)); //8GB
    this->addResUnit(new PResourceUnit(12, 8000000000)); //8GB
    this->addResUnit(new PResourceUnit(13, 8000000000)); //8GB
    this->addResUnit(new PResourceUnit(14, 8000000000)); //8GB
    this->addResUnit(new PResourceUnit(15, 8000000000)); //8GB
    this->addResUnit(new PResourceUnit(16, 8000000000)); //8GB
    break;
  }
  default:
    //throw std::runtime_error("The chosen memory type is not defined\n");
    break;
  }

}

Memory::~Memory(){

}

Memory::mem_type Memory::getMemType(){

  return this->memType;

}

void Memory::print(){

  //
  std::string nCap = getFormattedString(getNominalCapacity(), util::BYTE);
  std::string aCap = getFormattedString(getAvailableCapacity(), util::BYTE);
  std::string uCap = getFormattedString(getUsedCapacity(), util::BYTE);

  std::cout << "Res " << getId() << ": ";
	std::cout << "Type = " << Resource::resTypeToString(getResType());
	std::cout << " [" << memTypeToString(getMemType()) << "], ";
	std::cout << "Pm_Id = " << getHostId() << ", ";
	std::cout << "nomCap = " << nCap << ", ";
	std::cout << "usedCap = " << uCap << ", ";
	std::cout << "availCap = " << aCap << "\n";

	std::vector <ResourceUnit*>::iterator iter;
	for(iter=resUnitList.begin(); iter!=resUnitList.end(); iter++)
			(*iter)->print(util::BYTE);

}

/** OTcl commands:
 * print -
 * get-available-cap -
 * update-id NEWID -
 **/
int Memory::command(int argc, const char*const* argv) {

  Tcl& tcl = Tcl::instance();
  //
	if(argc == 2) {
		if(strcmp(argv[1], "print") == 0) {
			this->print();
			return (TCL_OK);
		} else if (strcmp(argv[1], "get-available-cap") == 0) {
		  double result = 0;
		  result= getAvailableCapacity();
		  char out[100];
		  sprintf(out, "set mem_Acap %.0f", result);
		  tcl.eval(out);
			return (TCL_OK);
		}
	//
	} else if(argc == 3){
	  //
	  if(strcmp(argv[1], "update-id") == 0) {
	    int newId = atoi(argv[2]);
	    if(this->updateId(newId) == false){
	      std::cerr << "A resource can change its id only if it's not attached to any server\n";
	      return (TCL_ERROR);
	    }
	    return (TCL_OK);
		}
	}
	return (TCL_ERROR);

}

Memory::mem_type Memory::stringToMemType(std::string type){

  std::map<std::string, Memory::mem_type>::iterator it = memTypeCheck.find(type);
  if (it == memTypeCheck.end())
    throw std::runtime_error("The chosen Memory type is not defined\n");
  return it->second;

}

std::string Memory::memTypeToString(Memory::mem_type type){

  std::map<std::string, Memory::mem_type>::iterator it;
  for(it = memTypeCheck.begin(); it!= memTypeCheck.end(); it++)
    if (it->second == type)
      return it->first;
  throw std::runtime_error("The chosen Memory type is not defined\n");

}
