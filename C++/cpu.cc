/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#include "cpu.h"

/** */
static class CpuClass : public TclClass {
public:
	CpuClass() : TclClass("Cpu") {}
	TclObject* create(int argc, const char*const* argv) {
	  return (new Cpu(atoi(argv[4]), argv[5]));
	}
} class_cpu;

std::map<std::string, Cpu::cpu_type> Cpu::cpuTypeCheck = {
   {"CPU_2CORE", Cpu::CPU_2CORE},
   {"CPU_4CORE", Cpu::CPU_4CORE},
   {"CPU_8CORE", Cpu::CPU_8CORE},
   {"CPU_20CORE", Cpu::CPU_20CORE}
};

////////////

Cpu::Cpu(int cpuId, std::string cpuType) {

  this->resId = cpuId;
  this->resType = Resource::CPU;
  this->cpuType = stringToCpuType(cpuType);
  this->nominalCap = 0;
  this->hostId = -1;

  //
  switch(this->cpuType){
  case CPU_2CORE:{
    this->addResUnit(new PResourceUnit(1, 250000000));
    this->addResUnit(new PResourceUnit(2, 250000000));
    break;
  }
  case CPU_4CORE:{
    this->addResUnit(new PResourceUnit(1, 250000000));
    this->addResUnit(new PResourceUnit(2, 250000000));
    this->addResUnit(new PResourceUnit(3, 250000000));
    this->addResUnit(new PResourceUnit(4, 250000000));
    break;
  }
  case CPU_8CORE:{
    this->addResUnit(new PResourceUnit(1, 250000000));
    this->addResUnit(new PResourceUnit(2, 250000000));
    this->addResUnit(new PResourceUnit(3, 250000000));
    this->addResUnit(new PResourceUnit(4, 250000000));
    this->addResUnit(new PResourceUnit(5, 250000000));
    this->addResUnit(new PResourceUnit(6, 250000000));
    this->addResUnit(new PResourceUnit(7, 250000000));
    this->addResUnit(new PResourceUnit(8, 250000000));
    break;
  }
  case CPU_20CORE:{
    this->addResUnit(new PResourceUnit(1, 250000000));
    this->addResUnit(new PResourceUnit(2, 250000000));
    this->addResUnit(new PResourceUnit(3, 250000000));
    this->addResUnit(new PResourceUnit(4, 250000000));
    this->addResUnit(new PResourceUnit(5, 250000000));
    this->addResUnit(new PResourceUnit(6, 250000000));
    this->addResUnit(new PResourceUnit(7, 250000000));
    this->addResUnit(new PResourceUnit(8, 250000000));
    this->addResUnit(new PResourceUnit(9, 250000000));
    this->addResUnit(new PResourceUnit(10, 250000000));
    this->addResUnit(new PResourceUnit(11, 250000000));
    this->addResUnit(new PResourceUnit(12, 250000000));
    this->addResUnit(new PResourceUnit(13, 250000000));
    this->addResUnit(new PResourceUnit(14, 250000000));
    this->addResUnit(new PResourceUnit(15, 250000000));
    this->addResUnit(new PResourceUnit(16, 250000000));
    this->addResUnit(new PResourceUnit(17, 250000000));
    this->addResUnit(new PResourceUnit(18, 250000000));
    this->addResUnit(new PResourceUnit(19, 250000000));
    this->addResUnit(new PResourceUnit(20, 250000000));
    break;
  }
  default:
    //throw std::runtime_error("The chosen cpu type is not defined\n");
    break;
  }

}

Cpu::~Cpu(){

}

Cpu::cpu_type Cpu::getCpuType(){

  return this->cpuType;

}

void Cpu::print(){

  //
  std::string nCap = getFormattedString(getNominalCapacity(), util::MIPS);
  std::string aCap = getFormattedString(getAvailableCapacity(), util::MIPS);
  std::string uCap = getFormattedString(getUsedCapacity(), util::MIPS);

  std::cout << "Res " << getId() << ": ";
	std::cout << "Type = " << Resource::resTypeToString(getResType());
	std::cout << " [" << cpuTypeToString(getCpuType()) << "], ";
	std::cout << "Pm_Id = " << getHostId() << ", ";
	std::cout << "nomCap = " << nCap << ", ";
	std::cout << "usedCap = " << uCap << ", ";
	std::cout << "availCap = " << aCap << "\n";

	std::vector <ResourceUnit*>::iterator iter;
	for(iter=resUnitList.begin(); iter!=resUnitList.end(); iter++)
			(*iter)->print(util::MIPS);

}

/** OTcl commands:
 * print -
 * get-available-cap -
 * update-id NEWID -
 **/
int Cpu::command(int argc, const char*const* argv) {

  Tcl& tcl = Tcl::instance();
	//
  if(argc == 2) {
		if(strcmp(argv[1], "print") == 0) {
			this->print();
			return (TCL_OK);
		} else if (strcmp(argv[1], "get-available-cap") == 0) {
		  double result = 0;
		  result= getAvailableCapacity();
		  char out[100];
		  sprintf(out, "set cpu_Acap %.0f", result);
		  tcl.eval(out);
			return (TCL_OK);
		}
	//
	} else if(argc == 3){
	  //
		if(strcmp(argv[1], "update-id") == 0) {
		  int newId = atoi(argv[2]);
		  if(this->updateId(newId) == false){
		    std::cerr << "A resource can change its id only if it's not attached to any server\n";
		    return (TCL_ERROR);
		  }
		  return (TCL_OK);
		}
	}
	return (TCL_ERROR);

}

Cpu::cpu_type Cpu::stringToCpuType(std::string type){

  std::map<std::string, Cpu::cpu_type>::iterator it = cpuTypeCheck.find(type);
  if (it == cpuTypeCheck.end())
    throw std::runtime_error("The chosen cpu type is not defined\n");
  return it->second;

}

std::string Cpu::cpuTypeToString(Cpu::cpu_type type){

  std::map<std::string, Cpu::cpu_type>::iterator it;
  for(it = cpuTypeCheck.begin(); it!= cpuTypeCheck.end(); it++)
    if (it->second == type)
      return it->first;
  throw std::runtime_error("The chosen cpu type is not defined\n");

}
