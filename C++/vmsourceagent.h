/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef VMSOURCEAGENT_H_
#define VMSOURCEAGENT_H_

#include <iostream>

#include <tclcl.h>
#include <udp.h>

/** */
class VmSourceAgent : public UdpAgent {

public:

  /** */
  VmSourceAgent();

  /** */
  VmSourceAgent(packet_t);

  /** */
  ~VmSourceAgent();

  /** */
  void setVmId(int vmId);

  /** */
  int getVmId();

  /** */
  void setSinkVmId(int sinkVmId);

  /** */
  int getSinkVmId();

  /** */
  unsigned long long getTxBytes();

  /** */
  void sendmsg(int nbytes, AppData* data, const char* flags) override;

  /** */
  int command(int argc, const char*const* argv) override;

private:

  /** */
  int vmId;

  /** */
  int sinkVmId;

  /** */
  unsigned long long bytesTx;

};

#endif /* VMSOURCEAGENT_H_ */
