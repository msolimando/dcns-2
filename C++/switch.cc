/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#include "dcmanager.h"
#include "switch.h"

static class SwitchClass : public TclClass {
public:
  SwitchClass() : TclClass("Switch") {}
  TclObject* create(int argc, const char*const*argv) {
    //OTcl domain: set newSwitch [new Switch {id switch_type}]
    return (new Switch(atoi(argv[4]), argv[5]));
  }
} class_switch;

//
std::map<std::string, Switch::switch_state> Switch::switchStateCheck = {
  {"ON", Switch::ON},
  {"OFF", Switch::OFF}
};

std::map<std::string, Switch::switch_type> Switch::switchTypeCheck = {
   {"SWITCH_1Gbps", Switch::SWITCH_1Gbps},
   {"SWITCH_10Gbps", Switch::SWITCH_10Gbps},
   {"SWITCH_40Gbps", Switch::SWITCH_40Gbps}
};

std::map<std::string, Switch::port_type> Switch::portTypeCheck = {
   {"10MbE", Switch::PT_10MbE},
   {"100MbE", Switch::PT_100MbE},
   {"1GbE", Switch::PT_1GbE},
   {"10GbE", Switch::PT_10GbE},
   {"40GbE", Switch::PT_40GbE},
   {"100GbE", Switch::PT_100GbE},
};

////////////

Switch::Switch(int switchId, std::string switchType) : switchState(OFF), nPorts(0), samplingPeriod(1) {

  int i;
  //
  this->id = switchId;
  this->switchType = stringToSwitchType(switchType);
  this->lastStatsInterval = std::make_pair(0.0, 0.0);
  // Create a new Node object in OTcl domain and assign it to nodeOTcl
  Tcl::instance().evalc("[Simulator instance] node");
  this->node_OTcl = std::string(Tcl::instance().result());
  //
  switch(this->switchType){
  //
  case SWITCH_1Gbps:{
    // 10 x 100MbE ports
    for(i = 1; i <= 10; i++){
      Port* p = new Port();
      p->portId = i;
      p->bandwidth = 12500000; // 100Mbps = 12,5 MBps
      p->active = false;
      p->ptype = PT_100MbE;
      //
      this->switchPorts.push_back(p);
    }
    //
    portMap.insert(std::make_pair(PT_100MbE, 10));

    // 2 x 1GbE ports
    for(int j = i; j < i+2; j++){
      Port* p = new Port();
      p->portId = j;
      p->bandwidth = 125000000; // 1Gbps = 125 MBps
      p->active = false;
      p->ptype = PT_1GbE;
      //
      this->switchPorts.push_back(p);
    }
    portMap.insert(std::make_pair(PT_1GbE, 2));
    this->nPorts = 12;
    break;
  }
  //
  case SWITCH_10Gbps:{
    // 10 x 1GbE ports
    for(i = 1; i <= 10; i++){
      Port* p = new Port();
      p->portId = i;
      p->bandwidth = 125000000; // 1Gb = 125 MB
      p->active = false;
      p->ptype = PT_1GbE;
      //
      this->switchPorts.push_back(p);
    }
    portMap.insert(std::make_pair(PT_1GbE, 10));

    // 1 x 10GbE ports
    Port* p = new Port();
    p->portId = i;
    p->bandwidth = 1250000000; // 10Gb = 1,25 GB
    p->active = false;
    p->ptype = PT_10GbE;
    //
    this->switchPorts.push_back(p);
    //
    portMap.insert(std::make_pair(PT_10GbE, 1));
    this->nPorts = 11;
    break;
  }
  //
  case SWITCH_40Gbps:{
    // 48 x 1GbE ports
    for(i = 1; i <= 48; i++){
      Port* p = new Port();
      p->portId = i;
      p->bandwidth = 125000000; // 1Gb = 125 MB
      p->active = false;
      p->ptype = PT_1GbE;
      //
      this->switchPorts.push_back(p);
    }
    portMap.insert(std::make_pair(PT_1GbE, 48));

    // 4 x 10GbE ports
    for(int j = i; j < i+4; j++){
      Port* p = new Port();
      p->portId = j;
      p->bandwidth = 1250000000; // 10Gb = 1,25 GB
      p->active = false;
      p->ptype = PT_10GbE;
      //
      this->switchPorts.push_back(p);
    }
    portMap.insert(std::make_pair(PT_10GbE, 4));
    this->nPorts = 52;
    break;
  }
  default:
    break;
  }
  //
  DcManager::Instance()->addSwitchToSwitchList(this);

}

Switch::~Switch(){

  switchPorts.~vector();

}

int Switch::getId(){

  return this->id;

}

Switch::switch_type Switch::getSwitchType(){

  return this->switchType;

}

Switch::switch_state Switch::getSwitchState(){

  return this->switchState;

}

bool Switch::setSwitchState(switch_state newState){

  switch_state currentState = this->getSwitchState();
  if(currentState == newState){
    std::cerr << "The switch state" << this->getId() << " is already " << newState << "\n";
    return false;
  //
  }else if(newState == OFF){
    // Stop the statistics collection
    this->cancel();
    // Block all the switch links to avoid packet transmissions
    std::vector<Port*>::iterator it;
    for(it = switchPorts.begin(); it != switchPorts.end(); it++){
      // If the port is active (i.e. a node has been previously linked to this port)
      if((*it)->active == true)
        this->blockLinkQueue_OTcl((*it));
    }
    // Collect a last round of statistics before turn-off the switch
    this->collectStatistics();
    // Update the switch state to be OFF
    this->switchState = OFF;

    /*std::cout << "The switch " << this->getId() << " was powered off at "
        << getFormattedString(Scheduler::instance().clock(), util::SECONDS) << "\n";*/
  //
  }else if(newState == ON){
    // Update the switch state to ON
    this->switchState = ON;
    // Unblock all the switch links to resume (or start) packet transmissions
    std::vector<Port*>::iterator it;
    for(it = switchPorts.begin(); it != switchPorts.end(); it++){
      // If the port is active (i.e. a node has been previously linked to this port)
      if((*it)->active == true)
        this->unblockLinkQueue_OTcl((*it));
    }
    // Start the statistics collection (set the switch to invoke expire()
    // at "samplingPeriod" seconds in the future)
    sched(samplingPeriod);

    /*std::cout << "The switch " << this->getId() << " was powered on at "
                << getFormattedString(Scheduler::instance().clock(), util::SECONDS) << "\n\n";*/
  }
  return true;

}

std::string Switch::getNode_OTcl(){

  return this->node_OTcl;

}

std::map<Switch::port_type, int> Switch::getPortMap(){

  return this->portMap;

}

int Switch::getNumberOfSwitchPorts(){

  return this->nPorts;

}

void Switch::collectStatistics(){

  //
  double now = Scheduler::instance().clock();
  if(now <= 0)
    now = 1e-9; // 1ns
  //
  double oldStatsTime = this->lastStatsInterval.second;
  //
  double interval = now - oldStatsTime;
  // Iterate over all switch ports to update the switch stats
  std::vector<Port*>::iterator it;
  for(it = switchPorts.begin(); it != switchPorts.end(); it++){
    // If a node has been linked to the port (i.e. the port is active)
    if((*it)->active == true){
      //
      (*it)->rxLinkLoad = (((double)((*it)->qmonRx->getBdepartures()-(*it)->rxBytes))/interval);
      (*it)->txLinkLoad = (((double)((*it)->qmonTx->getBdepartures()-(*it)->txBytes))/interval);
      //
      (*it)->rxBytes = (*it)->qmonRx->getBdepartures();
      (*it)->rxPackets = (*it)->qmonRx->getPdepartures();
      (*it)->rxByteDrops = (*it)->qmonRx->getBdrops();
      (*it)->rxPacketDrops = (*it)->qmonRx->getPdrops();
      //
      (*it)->txBytes = (*it)->qmonTx->getBdepartures();
      (*it)->txPackets = (*it)->qmonTx->getPdepartures();
      (*it)->txByteDrops = (*it)->qmonTx->getBdrops();
      (*it)->txPacketDrops = (*it)->qmonTx->getPdrops();
    }
  }
  //
  this->lastStatsInterval.first = oldStatsTime;
  this->lastStatsInterval.second = now;

}

bool Switch::scheduleLinkFault(double time, int portId){

  Tcl& tcl = Tcl::instance();
  //
  std::vector<Port*>::iterator it;
  for(it = switchPorts.begin(); it != switchPorts.end(); it++){
    // If the port with id "portId" has been defined for this switch
    if((*it)->portId == portId){
      // Check if the port has been connected to a link
      if((*it)->active == true){
        // Schedule a link fault
        tcl.evalf("[Simulator instance] rtmodel-at %f down %s %s",
            time, this->getNode_OTcl().c_str(), (*it)->node_OTcl.c_str() );
        std::cout << "[Link fault scheduled at t = " << time << "]\n\n";
        return true;
      }else{
        std::cerr << "The port " << portId << " has no link attached; select"
            " another port if you want to simulate a link fault\n";
        return false;
      }
    }
  }
  std::cerr << "There isn't any port with id equals to " << portId
          << " in switch " << this->getId() << "\n";
  return false;

}

bool Switch::scheduleLinkReactivation(double time, int portId){

  Tcl& tcl = Tcl::instance();
  //
  std::vector<Port*>::iterator it;
  for(it = switchPorts.begin(); it != switchPorts.end(); it++){
    // If the port with id "portId" has been defined for this switch
    if((*it)->portId == portId){
      // Check if the port has been connected to a link
      if((*it)->active == true){
        // Schedule a link reactivation
        tcl.evalf("[Simulator instance] rtmodel-at %f up %s %s",
            time, this->getNode_OTcl().c_str(), (*it)->node_OTcl.c_str() );
        std::cout << "Link reactivation scheduled at t = " << time << std::endl;
        return true;
      }else{
        std::cerr << "The port " << portId << " has no link attached; select"
            " another port if you want to simulate a link reactivation\n";
        return false;
      }
    }
  }
  std::cerr << "There isn't any port with id equals to " << portId
          << " in switch " << this->getId() << "\n";
  return false;

}

bool Switch::addLinkToPort(int portId, Pm* pm){

  Tcl& tcl = Tcl::instance();
  //
  std::vector<Port*>::iterator it;
  for(it = switchPorts.begin(); it != switchPorts.end(); it++){
    // If the port with id "portId" has been defined for this switch
    if((*it)->portId == portId){
      // Check if the port is already connected to another link
      if((*it)->active == true){
        std::cerr << "The port " << portId << " is already occupied; select"
            " another port for connecting the link\n";
        return false;
      }
      // Set the link parameters
      const char* pmNode = pm->getNode_OTcl().c_str();
      const char* swNode = this->getNode_OTcl().c_str();
      double bw = (*it)->bandwidth*8;  // Bandwidth capacity in bps
      char* delay = "0.0033ms";
      char* type = "DropTail";
      // Create the link between the pm and the switch
      tcl.evalf("[Simulator instance] duplex-link %s %s %f %s %s",
          pmNode, swNode, bw, delay, type);
      // Define an ULLQueueMonitor for monitoring the traffic between the server and the switch
      tcl.evalf("[Simulator instance] create-and-attach-ullqueuemon %s %s \"\"", pmNode, swNode);
      ULLQueueMonitor* qmonRx = (ULLQueueMonitor*) (TclObject::lookup(tcl.result()));
      // Define an ULLQueueMonitor for monitoring the traffic between the switch and the server
      tcl.evalf("[Simulator instance] create-and-attach-ullqueuemon %s %s \"\"", swNode, pmNode);
      ULLQueueMonitor* qmonTx = (ULLQueueMonitor*) (TclObject::lookup(tcl.result()));
      //
      if((qmonRx != NULL) && (qmonTx != NULL)){
        // Connect the queue-monitors to switch port
        (*it)->qmonRx = qmonRx;
        (*it)->qmonTx = qmonTx;
        (*it)->active = true;
        (*it)->node_OTcl = std::string(pmNode);
        //
        if(this->switchState == OFF){
          // Block the links queue
          this->blockLinkQueue_OTcl((*it));
        }
        // Connect the queue-monitors to server
        pm->setRxMonitor(qmonTx);
        pm->setTxMonitor(qmonRx);
        return true;
      }
    }
  }
  std::cerr << "There isn't any port with id equals to " << portId
          << " in switch " << this->getId() << "\n";
  return false;

}

bool Switch::addLinkToPort(int portId, Switch* otherSwitch, int otherSwitchPortId){

  // The characteristics of the link that will be defined between the two switches
  const char* swNode1 = this->getNode_OTcl().c_str();
  const char* swNode2 = otherSwitch->getNode_OTcl().c_str();
  char* delay = "0.0033ms";
  char* type = "DropTail";
  double bw; // The link bdw is specified later according to the ports characteristics
  //
  Tcl& tcl = Tcl::instance();
  //
  std::vector<Port*>::iterator it;
  for(it = this->switchPorts.begin(); it != this->switchPorts.end(); it++){
    // If the port with id "portId" has been defined for the first switch...
    if((*it)->portId == portId){
      // ...check if the port is already connected to another link
      Port* p1 = (*it);
      if(p1->active == true){
        std::cerr << "The port " << portId << " of the switch " << this->getId()
            << " is already occupied; select another port for connecting the two switches\n";
        return false;
      }
      // If the port with id "portId" is currently not attached to any link,
      // do the same check, for the port "otherSwitchPortId", in the second switch
      for(it = otherSwitch->switchPorts.begin(); it != otherSwitch->switchPorts.end(); it++){
        if((*it)->portId == otherSwitchPortId){
          Port* p2 = (*it);
          if(p2->active == true){
            std::cerr << "The port " << otherSwitchPortId << " of the switch "
                << otherSwitch->getId() << " is already occupied; select another "
                "port for connecting the two switches\n";
            return false;
          }
          // If both the ports exists and are currently not attached to any link, check
          // their bandwidth and define a link between them according to the smallest value
          if(p1->bandwidth <= p2->bandwidth)
            bw = p1->bandwidth*8; // The link bandwidth in bps
          else
            bw = p2->bandwidth*8; // The link bandwidth in bps
          // Create the link according to the previously defined parameters
          tcl.evalf("[Simulator instance] duplex-link %s %s %f %s %s",
            swNode2, swNode1, bw, delay, type);
          // Define an ULLQueueMonitor for monitoring the traffic between the switch sw
          // and this switch
          tcl.evalf("[Simulator instance] create-and-attach-ullqueuemon %s %s \"\"", swNode2, swNode1);
          ULLQueueMonitor* qmonRx = (ULLQueueMonitor*) (TclObject::lookup(tcl.result()));
          // Define an ULLQueueMonitor for monitoring the traffic between this switch
          // and the switch sw
          tcl.evalf("[Simulator instance] create-and-attach-ullqueuemon %s %s \"\"", swNode1, swNode2);
          ULLQueueMonitor* qmonTx = (ULLQueueMonitor*) (TclObject::lookup(tcl.result()));
          // Link the monitors to the switch ports
          if((qmonRx != NULL) && (qmonTx != NULL)){
            // Connect the queue-monitors to port p1 of this switch
            p1->qmonRx = qmonRx;
            p1->qmonTx = qmonTx;
            p1->active = true;
            p1->node_OTcl = std::string(swNode2);
            // Connect the queue-monitors to port p2 of switch sw
            p2->qmonRx = qmonTx;
            p2->qmonTx = qmonRx;
            p2->active = true;
            p2->node_OTcl = std::string(this->getNode_OTcl().c_str());
            //
            if((this->switchState == OFF) || (otherSwitch->getSwitchState() == OFF)){
              // Block the links queue
              this->blockLinkQueue_OTcl(p1);
            }
            return true;
          }
        } // end if
      } // end for
      std::cerr << "There isn't any port with id equals to " << otherSwitchPortId
            << " in switch " << otherSwitch->getId() << "\n";
      return false;
    } // end if
  } // end for
  std::cerr << "There isn't any port with id equals to " << portId
      << " in switch " << this->getId() << "\n";
  return false;

}

void Switch::print(){

  std::stringstream rxLoadPerc;
  std::stringstream txLoadPerc;
  // To print the percentages of RxLinkLoad and TxLinkLoad with only two digits
  // after the decimal point
  rxLoadPerc << std::setprecision(2);
  txLoadPerc << std::setprecision(2);
  //
  std::string intervalBeg = getFormattedString(this->lastStatsInterval.first, util::SECONDS);
  std::string intervalEnd = getFormattedString(this->lastStatsInterval.second, util::SECONDS);
  //
  std::cout << "=============================================================="
      << "==============================================================\n"
      << "Switch " << getId() << ": "
      << "State = " << switchStateToString(getSwitchState()) << ", "
      << "Stats Interval = [" << intervalBeg << "," << intervalEnd << "]\n"
      << "          nPorts = " << nPorts;
  //
  std::map<port_type, int>::iterator itP;
  for(itP = portMap.begin(); itP != portMap.end(); itP++)
    std::cout << " [" << portTypeToString((*itP).first) << " = " << (*itP).second << "]";
  std::cout << "\n============================================================= Ports"
      << " ========================================================\n";
  //
  std::vector<Port*>::iterator it;
  // Iterate over all switch ports
  for(it = switchPorts.begin(); it != switchPorts.end(); it++){
    //
    rxLoadPerc.str("");
    rxLoadPerc << ((*it)->rxLinkLoad/(*it)->bandwidth)*100;
    txLoadPerc.str("");
    txLoadPerc << ((*it)->txLinkLoad/(*it)->bandwidth)*100;
    //
    std::cout << "- [" << portTypeToString((*it)->ptype) << "] "
        << "Port " << (*it)->portId
        << ": Bandwidth = " << getFormattedString(((*it)->bandwidth*8), util::BITPS)
        << ", RxLinkLoad = " << getFormattedString(((*it)->rxLinkLoad*8), util::BITPS)
        << "(" << rxLoadPerc.str() << "%)"
        << ", TxLinkLoad = " << getFormattedString(((*it)->txLinkLoad*8), util::BITPS)
        << "(" << txLoadPerc.str() << "%)\n"
        << "\t\t     RxBytes = " << getFormattedString((*it)->rxBytes, util::BYTE)
        << ", RxByteDrops = " << getFormattedString((*it)->rxByteDrops, util::BYTE)
        << ", RxPackets = "<< (*it)->rxPackets
        << ", RxPacketDrops = " << (*it)->rxPacketDrops <<",\n"
        << "\t\t     TxBytes = "<< getFormattedString((*it)->txBytes, util::BYTE)
        << ", TxByteDrops = " << getFormattedString((*it)->txByteDrops, util::BYTE)
        << ", TxPackets = "<< (*it)->txPackets
        << ", TxPacketDrops = " << (*it)->txPacketDrops <<"\n"
        << "--------------------------------------------------------------"
        << "--------------------------------------------------------------\n";
  }
  std::cout << "=============================================================="
      << "==============================================================\n";

}

/** OTcl commands:
 * commands-list {} -
 * print {} -
 * get-id {} -
 * get-node {} -
 * collect-statistics {} -
 * start {} -
 * stop {} -
 * get-rx-packets {port_id}
 * get-tx-packets {port_id}
 * get-rx-bytes {port_id}
 * get-tx-bytes {port_id}
 * get-rx-byteDrops {port_id}
 * get-tx-byteDrops {port_id}
 * get-rx-packetDrops {port_id}
 * get-tx-packetDrops {port_id}
 * get-rx-linkLoad {port_id}
 * get-tx-linkLoad {port_id}
 * get-tx-loadPerc {port_id}
 * get-rx-loadPerc {port_id}
 * change-sampling-period {newPeriod} -
 * link-fault-at {time port_id} -
 * link-reactivation-at {time port_id} -
 * link-pm-to-port {pm* portId} -
 * link-switch-to-port {switch* port_id otherSwitch_port_id} -
 **/
int Switch::command(int argc, const char*const* argv){

  //
  if(argc == 2) {
    if(strcmp(argv[1], "commands-list") == 0) {
      std::cout << "- commands-list {}\n"
                   "- print {}\n"
                   "- get-id {}\n"
                   "- get-node {}\n"
                   "- collect-statistics {}\n"
                   "- start {}\n"
                   "- stop {}\n"
                   "- get-rx-packets {port_id}\n"
                   "- get-tx-packets {port_id}\n"
                   "- get-rx-bytes {port_id}\n"
                   "- get-tx-bytes {port_id}\n"
                   "- get-rx-byteDrops {port_id}\n"
                   "- get-tx-byteDrops {port_id}\n"
                   "- get-rx-packetDrops {port_id}\n"
                   "- get-tx-packetDrops {port_id}\n"
                   "- get-rx-linkLoad {port_id}\n"
                   "- get-tx-linkLoad {port_id}\n"
                   "- get-rx-loadPerc {port_id}\n"
                   "- get-tx-loadPerc {port_id}\n"
                   "- change-sampling-period {newPeriod}\n"
                   "- link-fault-at {time port_id}\n"
                   "- link-reactivation-at {time port_id}\n"
                   "- link-pm-to-port {pm* port_id}"
                   "- link-switch-to-port {switch* port_id otherSwitch_port_id}\n";
      return (TCL_OK);
    //
    }else if(strcmp(argv[1], "print") == 0) {
      print();
      return (TCL_OK);
    //
    } else if(strcmp(argv[1], "get-id") == 0) {
      // Pass the switch id to the OTCl domain
      Tcl::instance().result(std::to_string(this->getId()).c_str());
      return (TCL_OK);
    //
    }else if(strcmp(argv[1], "get-node") == 0) {
      // Pass the node object to the OTCl domain
      Tcl::instance().result(this->getNode_OTcl().c_str());
      return (TCL_OK);
    //
    }else if (strcmp(argv[1], "collect-statistics") == 0) {
      collectStatistics();
      return (TCL_OK);
    //
    }else if(strcmp(argv[1], "start") == 0) {
      if(setSwitchState(ON) == true)
        return (TCL_OK);
      return (TCL_ERROR);
    //
    }else if(strcmp(argv[1], "stop") == 0) {
      if(setSwitchState(OFF) == true)
        return (TCL_OK);
      return (TCL_ERROR);
    }
  //
  }else if (argc == 3) {
    //
    if(strcmp(argv[1], "get-rx-packets") == 0) {
      int portId = atoi(argv[2]);
      std::vector<Port*>::iterator it;
      for(it = switchPorts.begin(); it != switchPorts.end(); it++){
        // If the port with id "portId" has been defined for this switch
        if((*it)->portId == portId){
          // Check if the port is connected to a link
          if((*it)->active == true){
            // Pass the requested parameter to the OTCl domain
            Tcl::instance().result(std::to_string((*it)->rxPackets).c_str());
            return (TCL_OK);
          }
        }
      }
      return (TCL_ERROR);
    }
    //
    else if(strcmp(argv[1], "get-tx-packets") == 0) {
      int portId = atoi(argv[2]);
      std::vector<Port*>::iterator it;
      for(it = switchPorts.begin(); it != switchPorts.end(); it++){
        // If the port with id "portId" has been defined for this switch
        if((*it)->portId == portId){
          // Check if the port is connected to a link
          if((*it)->active == true){
            // Pass the requested parameter to the OTCl domain
            Tcl::instance().result(std::to_string((*it)->txPackets).c_str());
            return (TCL_OK);
          }
        }
      }
      return (TCL_ERROR);
    }
    //
    else if(strcmp(argv[1], "get-rx-bytes") == 0) {
      int portId = atoi(argv[2]);
      std::vector<Port*>::iterator it;
      for(it = switchPorts.begin(); it != switchPorts.end(); it++){
        // If the port with id "portId" has been defined for this switch
        if((*it)->portId == portId){
          // Check if the port is connected to a link
          if((*it)->active == true){
            // Pass the requested parameter to the OTCl domain
            Tcl::instance().result(std::to_string((*it)->rxBytes).c_str());
            return (TCL_OK);
          }
        }
      }
      return (TCL_ERROR);
    }
    //
    else if(strcmp(argv[1], "get-tx-bytes") == 0) {
      int portId = atoi(argv[2]);
      std::vector<Port*>::iterator it;
      for(it = switchPorts.begin(); it != switchPorts.end(); it++){
        // If the port with id "portId" has been defined for this switch
        if((*it)->portId == portId){
          // Check if the port is connected to a link
          if((*it)->active == true){
            // Pass the requested parameter to the OTCl domain
            Tcl::instance().result(std::to_string((*it)->txBytes).c_str());
            return (TCL_OK);
          }
        }
      }
      return (TCL_ERROR);
    }
    //
    else if(strcmp(argv[1], "get-rx-byteDrops") == 0) {
      int portId = atoi(argv[2]);
      std::vector<Port*>::iterator it;
      for(it = switchPorts.begin(); it != switchPorts.end(); it++){
        // If the port with id "portId" has been defined for this switch
        if((*it)->portId == portId){
          // Check if the port is connected to a link
          if((*it)->active == true){
            // Pass the requested parameter to the OTCl domain
            Tcl::instance().result(std::to_string((*it)->rxByteDrops).c_str());
            return (TCL_OK);
          }
        }
      }
      return (TCL_ERROR);
    }
    //
    else if(strcmp(argv[1], "get-tx-byteDrops") == 0) {
      int portId = atoi(argv[2]);
      std::vector<Port*>::iterator it;
      for(it = switchPorts.begin(); it != switchPorts.end(); it++){
        // If the port with id "portId" has been defined for this switch
        if((*it)->portId == portId){
          // Check if the port is connected to a link
          if((*it)->active == true){
            // Pass the requested parameter to the OTCl domain
            Tcl::instance().result(std::to_string((*it)->txByteDrops).c_str());
            return (TCL_OK);
          }
        }
      }
      return (TCL_ERROR);
    }
    //
    else if(strcmp(argv[1], "get-rx-packetDrops") == 0) {
      int portId = atoi(argv[2]);
      std::vector<Port*>::iterator it;
      for(it = switchPorts.begin(); it != switchPorts.end(); it++){
        // If the port with id "portId" has been defined for this switch
        if((*it)->portId == portId){
          // Check if the port is connected to a link
          if((*it)->active == true){
            // Pass the requested parameter to the OTCl domain
            Tcl::instance().result(std::to_string((*it)->rxPacketDrops).c_str());
            return (TCL_OK);
          }
        }
      }
      return (TCL_ERROR);
    }
    //
    else if(strcmp(argv[1], "get-tx-packetDrops") == 0) {
      int portId = atoi(argv[2]);
      std::vector<Port*>::iterator it;
      for(it = switchPorts.begin(); it != switchPorts.end(); it++){
        // If the port with id "portId" has been defined for this switch
        if((*it)->portId == portId){
          // Check if the port is connected to a link
          if((*it)->active == true){
            // Pass the requested parameter to the OTCl domain
            Tcl::instance().result(std::to_string((*it)->txPacketDrops).c_str());
            return (TCL_OK);
          }
        }
      }
      return (TCL_ERROR);
    }
    //
    else if(strcmp(argv[1], "get-rx-linkLoad") == 0) {
      int portId = atoi(argv[2]);
      std::vector<Port*>::iterator it;
      for(it = switchPorts.begin(); it != switchPorts.end(); it++){
        // If the port with id "portId" has been defined for this switch
        if((*it)->portId == portId){
          // Check if the port is connected to a link
          if((*it)->active == true){
            // Pass the requested parameter to the OTCl domain
            Tcl::instance().result(std::to_string((*it)->rxLinkLoad).c_str());
            return (TCL_OK);
          }
        }
      }
      return (TCL_ERROR);
    }
    //
    else if(strcmp(argv[1], "get-tx-linkLoad") == 0) {
      int portId = atoi(argv[2]);
      std::vector<Port*>::iterator it;
      for(it = switchPorts.begin(); it != switchPorts.end(); it++){
        // If the port with id "portId" has been defined for this switch
        if((*it)->portId == portId){
          // Check if the port is connected to a link
          if((*it)->active == true){
            // Pass the requested parameter to the OTCl domain
            Tcl::instance().result(std::to_string((*it)->txLinkLoad).c_str());
            return (TCL_OK);
          }
        }
      }
      return (TCL_ERROR);
    }
    //
    else if(strcmp(argv[1], "get-rx-loadPerc") == 0) {
      int portId = atoi(argv[2]);
      std::vector<Port*>::iterator it;
      for(it = switchPorts.begin(); it != switchPorts.end(); it++){
        // If the port with id "portId" has been defined for this switch
        if((*it)->portId == portId){
          // Check if the port is connected to a link
          if((*it)->active == true){
            double rxLoadPerc = ((*it)->rxLinkLoad*100)/(*it)->bandwidth;
            // Pass the requested parameter to the OTCl domain
            Tcl::instance().result(std::to_string(rxLoadPerc).c_str());
            return (TCL_OK);
          }
        }
      }
      return (TCL_ERROR);
    }
    //
    else if(strcmp(argv[1], "get-tx-loadPerc") == 0) {
      int portId = atoi(argv[2]);
      std::vector<Port*>::iterator it;
      for(it = switchPorts.begin(); it != switchPorts.end(); it++){
        // If the port with id "portId" has been defined for this switch
        if((*it)->portId == portId){
          // Check if the port is connected to a link
          if((*it)->active == true){
            double txLoadPerc = ((*it)->txLinkLoad*100)/(*it)->bandwidth;
            // Pass the requested parameter to the OTCl domain
            Tcl::instance().result(std::to_string(txLoadPerc).c_str());
            return (TCL_OK);
          }
        }
      }
      return (TCL_ERROR);
    }
    //
    else if(strcmp(argv[1], "change-sampling-period") == 0) {
      int sp = atoi(argv[2]);
      if(sp > 0){
        this->samplingPeriod = sp;
        return (TCL_OK);
      }else{
        std::cerr << "The sampling period must be greater than zero";
        return (TCL_ERROR);
      }

    }
  //
  }else if (argc == 4) {
    //
    if(strcmp(argv[1], "link-fault-at") == 0) {
      double time = atof(argv[2]);
      int portId = atoi(argv[3]);
      if(scheduleLinkFault(time, portId) == true)
        return (TCL_OK);
      return (TCL_ERROR);
    }
    //
    else if(strcmp(argv[1], "link-reactivation-at") == 0) {
      double time = atof(argv[2]);
      int portId = atoi(argv[3]);
      if(scheduleLinkReactivation(time, portId) == true)
        return (TCL_OK);
      return (TCL_ERROR);
    }
    //
    else if(strcmp(argv[1], "link-pm-to-port") == 0) {
      Pm* pm = (Pm*) (TclObject::lookup(argv[2]));
      int portId = atoi(argv[3]);
      if(addLinkToPort(portId, pm) == true)
        return (TCL_OK);
      return (TCL_ERROR);
    //
    }
  } else if (argc == 5) {
    if(strcmp(argv[1], "link-switch-to-port") == 0) {
      Switch* otherSwitch = dynamic_cast<Switch*> (TclObject::lookup(argv[2]));
      int portId = atoi(argv[3]);
      int otherSwitchPortId = atoi(argv[4]);
      if(addLinkToPort(portId, otherSwitch, otherSwitchPortId) == true)
        return (TCL_OK);
      return (TCL_ERROR);
    }
  }
  return (TCL_ERROR);

}


Switch::switch_state Switch::stringToSwitchState(std::string type){

  std::map<std::string, switch_state>::iterator it = switchStateCheck.find(type);
  if (it == switchStateCheck.end())
    throw std::runtime_error("The chosen switch state is not defined\n");
  return it->second;

}

std::string Switch::switchStateToString(Switch::switch_state state){

  std::map<std::string, switch_state>::iterator it;
  for(it = switchStateCheck.begin(); it != switchStateCheck.end(); it++)
    if (it->second == state)
      return it->first;
  throw std::runtime_error("The chosen switch state is not defined\n");

}

Switch::switch_type Switch::stringToSwitchType(std::string type){

  std::map<std::string, Switch::switch_type>::iterator it = switchTypeCheck.find(type);
  if(it == switchTypeCheck.end())
    throw std::runtime_error("The chosen switch type is not defined\n");
  return it->second;

}

std::string Switch::switchTypeToString(Switch::switch_type type){

  std::map<std::string, Switch::switch_type>::iterator it;
  for(it = switchTypeCheck.begin(); it != switchTypeCheck.end(); it++)
    if(it->second == type)
      return it->first;
  throw std::runtime_error("The chosen switch type is not defined\n");

}

Switch::port_type Switch::stringToPortType(std::string type){

  std::map<std::string, port_type>::iterator it = portTypeCheck.find(type);
  if(it == portTypeCheck.end())
    throw std::runtime_error("The chosen port type is not defined\n");
  return it->second;

}

std::string Switch::portTypeToString(Switch::port_type type){

  std::map<std::string, port_type>::iterator it;
  for(it = portTypeCheck.begin(); it != portTypeCheck.end(); it++)
    if(it->second == type)
      return it->first;
  throw std::runtime_error("The chosen port type is not defined\n");

}

void Switch::expire(Event* e){

  collectStatistics();
  // Restart the switch timer and set it to expire at "samplingPeriod" seconds in future
  this->resched(samplingPeriod);

}

void Switch::blockLinkQueue_OTcl(Port* p){

  Tcl& tcl = Tcl::instance();
  // If the port is active (i.e. a node has been linked to port p)
  if(p->active == true){
    // Block the queues of the two Simplexlink object (rx e tx) connected to this port
    tcl.evalf("[[Simulator instance] link %s %s] queue",
            this->getNode_OTcl().c_str(), p->node_OTcl.c_str());
    Queue* queue1 = (Queue*) (TclObject::lookup(tcl.result()));
    queue1->block();
    tcl.evalf("[[Simulator instance] link %s %s] queue",
            p->node_OTcl.c_str(), this->getNode_OTcl().c_str());
    Queue* queue2 = (Queue*) (TclObject::lookup(tcl.result()));
    queue2->block();
  }

}

void Switch::unblockLinkQueue_OTcl(Port* p){

  Tcl& tcl = Tcl::instance();
  // If the port is active (i.e. a node has been linked to port p)
  if(p->active == true){
    // Unblock the queues of the two Simplexlink object (rx e tx) connected to this port
    tcl.evalf("[[Simulator instance] link %s %s] queue",
        this->getNode_OTcl().c_str(), p->node_OTcl.c_str());
    Queue* queue1 = (Queue*) (TclObject::lookup(tcl.result()));
    queue1->resume();
    tcl.evalf("[[Simulator instance] link %s %s] queue",
        p->node_OTcl.c_str(), this->getNode_OTcl().c_str());
    Queue* queue2 = (Queue*) (TclObject::lookup(tcl.result()));
    queue2->resume();
  }

}
