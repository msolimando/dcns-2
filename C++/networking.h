/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NETWORKING_H_
#define NETWORKING_H_

#include <iostream>
#include <map>
#include <stdexcept>
#include <cstdlib>
#include <string>
#include <vector> 

#include <tclcl.h>

#include "presource.h"
#include "presourceunit.h"
#include "util.h"

/** */
class Networking : public PResource, public TclObject {

public:

  //
  enum net_type {NET_1GbE, NET_10GbE, NET_40GbE, NET_100GbE};
	
	/** Class constructor */
  Networking(int netId, std::string netType);
	
	/** Class destructor */
	~Networking();

	/** Returns the Networking type */
	net_type getNetType();

	/** */
	void print() override;

	/** Defines the commands available for the class objects in the OTcl domain */
	int command(int argc, const char*const* argv);

private:

	/** */
	net_type stringToNetType(std::string type);

	/** */
	std::string netTypeToString(net_type type);

	/** */
	net_type netType;

	/** */
	static std::map<std::string, net_type> netTypeCheck;

};

#endif /* NETWORKING_H_ */
