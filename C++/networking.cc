/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#include "networking.h"

/** */
static class NetworkingClass : public TclClass {
public:
	NetworkingClass() : TclClass("Networking") {}
	TclObject* create(int argc, const char*const* argv) {
	  return (new Networking(atoi(argv[4]), argv[5]));
	}
} class_networking;

std::map<std::string, Networking::net_type> Networking::netTypeCheck = {
   {"NET_1GbE", Networking::NET_1GbE},
   {"NET_10GbE", Networking::NET_10GbE},
   {"NET_40GbE", Networking::NET_40GbE},
   {"NET_100GbE", Networking::NET_100GbE}
};

////////////

Networking::Networking(int netId, std::string netType) {

  this->resId = netId;
  this->resType = Resource::NETWORKING;
  this->netType = stringToNetType(netType);
  this->nominalCap = 0;
  this->hostId = -1;
  //
  switch(this->netType){
  case NET_1GbE:{
    this->addResUnit(new PResourceUnit(1, 125000000)); //125 MBps = 1Gbps
    break;
  }
  case NET_10GbE:{
    this->addResUnit(new PResourceUnit(1, 1250000000));
    break;
  }
  case NET_40GbE:{
      this->addResUnit(new PResourceUnit(0, 5000000000));
      break;
  }
  case NET_100GbE:{
    this->addResUnit(new PResourceUnit(0, 12500000000));
    break;
  }
  default:
    //throw std::runtime_error("The chosen Networking type is not defined\n");
    break;
  }

}

Networking::~Networking(){

}

Networking::net_type Networking::getNetType(){

  return this->netType;

}

void Networking::print(){

  //
  std::string nCap = getFormattedString(getNominalCapacity(), util::BYTEPS);
  std::string aCap = getFormattedString(getAvailableCapacity(), util::BYTEPS);
  std::string uCap = getFormattedString(getUsedCapacity(), util::BYTEPS);

  std::cout << "Res " << getId() << ": ";
	std::cout << "Type = " << Resource::resTypeToString(getResType());
	std::cout << " [" << netTypeToString(getNetType()) << "], ";
	std::cout << "Pm_Id = " << getHostId() << ", ";
	std::cout << "nomCap = " << nCap << ", ";
	std::cout << "usedCap = " << uCap << ", ";
	std::cout << "availCap = " << aCap << "\n";

	std::vector <ResourceUnit*>::iterator iter;
	for(iter=resUnitList.begin(); iter!=resUnitList.end(); iter++)
			(*iter)->print(util::BYTEPS);

}

/** OTcl commands:
 * print -
 * get-available-cap -
 * update-id NEWID -
 **/
int Networking::command(int argc, const char*const* argv) {

  Tcl& tcl = Tcl::instance();
  //
	if(argc == 2) {
		if(strcmp(argv[1], "print") == 0) {
			this->print();
			return (TCL_OK);
		} else if (strcmp(argv[1], "get-available-cap") == 0) {
		  double result = 0;
		  result= getAvailableCapacity();
		  char out[100];
		  sprintf(out, "set net_Acap %.0f", result);
		  tcl.eval(out);
			return (TCL_OK);
		}
	//
	} else if(argc == 3){
	  //
	  if(strcmp(argv[1], "update-id") == 0) {
	    int newId = atoi(argv[2]);
	    if(this->updateId(newId) == false){
	      std::cerr << "A resource can change its id only if it's not attached to any server\n";
	      return (TCL_ERROR);
	    }
	    return (TCL_OK);
	  }
	}
	return (TCL_ERROR);

}

Networking::net_type Networking::stringToNetType(std::string type){

  std::map<std::string, Networking::net_type>::iterator it = netTypeCheck.find(type);
  if (it == netTypeCheck.end())
    throw std::runtime_error("The chosen Networking type is not defined\n");
  return it->second;

}

std::string Networking::netTypeToString(Networking::net_type type){

  std::map<std::string, Networking::net_type>::iterator it;
  for(it = netTypeCheck.begin(); it!= netTypeCheck.end(); it++)
    if (it->second == type)
      return it->first;
  throw std::runtime_error("The chosen Networking type is not defined\n");

}
