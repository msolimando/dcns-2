/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#include "vstorage.h"

/** */
static class VStorageClass : public TclClass {
public:
  VStorageClass() : TclClass("VStorage") {}
	TclObject* create(int argc, const char*const* argv) {
	  return (new VStorage(atoi(argv[4]), argv[5]));
	}
} class_vstorage;

std::map<std::string, VStorage::vstor_type> VStorage::vstorTypeCheck = {
    {"STORAGE_10GB", VStorage::STORAGE_10GB},
    {"STORAGE_100GB", VStorage::STORAGE_100GB},
    {"STORAGE_250GB", VStorage::STORAGE_250GB},
    {"STORAGE_500GB", VStorage::STORAGE_500GB},
    {"STORAGE_1TB", VStorage::STORAGE_1TB}
};

////////////

VStorage::VStorage(int vstorId, std::string vstorType) {

  this->resId = vstorId;
  this->resType = Resource::STORAGE;
  this->vstorType = stringToVStorType(vstorType);
  this->nominalCap = 0;
  this->hostId = -1;
  //
  switch(this->vstorType){
  case STORAGE_10GB:{
    this->addResUnit(new VResourceUnit(1, 10000000000)); // 10GB
    break;
  }
  case STORAGE_100GB:{
    this->addResUnit(new VResourceUnit(1, 100000000000)); // 100GB
    break;
  }
  case STORAGE_250GB:{
    this->addResUnit(new VResourceUnit(1, 250000000000)); // 250GB
    break;
  }
  case STORAGE_500GB:{
    this->addResUnit(new VResourceUnit(1, 500000000000)); // 500GB
    break;
  }
  case STORAGE_1TB:{
    this->addResUnit(new VResourceUnit(1, 1000000000000)); // 1TB
    break;
  }
  default:
    //throw std::runtime_error("The chosen virtual storage type is not defined\n");
    break;
  }
  //
  this->nomCapToUse = this->getNominalCapacity();

}

VStorage::~VStorage(){

}

VStorage::vstor_type VStorage::getVStorType(){

  return this->vstorType;

}

void VStorage::print(){

  //
  std::string nCap = getFormattedString(getNominalCapacity(), util::BYTE);
  std::string aCap = getFormattedString(getAvailableCapacity(), util::BYTE);
  std::string uCap = getFormattedString(getUsedCapacity(), util::BYTE);
  //
  double nCapToUse = (getNominalCapacityToUse()/getNominalCapacity())*100;

  std::cout << "VRes " << getId() << ": ";
  std::cout << "Type = " << Resource::resTypeToString(getResType());
  std::cout << " [" << vstorTypeToString(getVStorType()) << "], ";
  std::cout << "Vm_id " << getHostId() << ", ";
  std::cout << "nomCap = " << nCap << " (" << nCapToUse << "%), ";
  std::cout << "usedCap = " << uCap  << ", ";
  std::cout << "availCap = " << aCap << "\n";

	std::vector <ResourceUnit*>::iterator iter;
	for(iter=resUnitList.begin(); iter!=resUnitList.end(); iter++)
			(*iter)->print(util::BYTE);

}

/** OTcl commands:
 * print -
 * get-available-cap -
 * update-id NEWID -
 **/
int VStorage::command(int argc, const char*const* argv) {

  Tcl& tcl = Tcl::instance();
  //
	if(argc == 2) {
	  //
		if(strcmp(argv[1], "print") == 0) {
			this->print();
			return (TCL_OK);
		//
		} else if (strcmp(argv[1], "get-available-cap") == 0) {
		  double result = 0;
		  result= getAvailableCapacity();
		  char out[100];
		  sprintf(out, "set vstor_Acap %.0f", result);
		  tcl.eval(out);
			return (TCL_OK);
		}
	//
	} else if(argc == 3){
	  //
	  if(strcmp(argv[1], "update-id") == 0) {
	    int newId = atoi(argv[2]);
	    if(this->updateId(newId) == false){
	      std::cerr << "A virtual resource can change its id only if it's not attached to any vm\n";
	      return (TCL_ERROR);
	    }
	    return (TCL_OK);
	  }
	}
	return (TCL_ERROR);

}

VStorage::vstor_type VStorage::stringToVStorType(std::string type){

  std::map<std::string, VStorage::vstor_type>::iterator it = vstorTypeCheck.find(type);
  if (it == vstorTypeCheck.end())
    throw std::runtime_error("The chosen virtual storage type is not defined\n");
  return it->second;

}

std::string VStorage::vstorTypeToString(VStorage::vstor_type type){

  std::map<std::string, VStorage::vstor_type>::iterator it;
  for(it = vstorTypeCheck.begin(); it != vstorTypeCheck.end(); it++)
    if (it->second == type)
      return it->first;
  throw std::runtime_error("The chosen virtual storage type is not defined\n");

}
