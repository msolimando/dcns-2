/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TORSWITCH_H_
#define TORSWITCH_H_

#include <iostream>
#include <map>
#include <string>
#include <vector>

#include <tclcl.h>

#include "switch.h"

/** Each object of this class represents an access switch into the datacenter architecture */
class ToRSwitch : public Switch {

public:
	
  /** Access Switch constructor */
  ToRSwitch(int switchId, std::string switchType);

  /** Access Switch destructor */
  ~ToRSwitch();

  /** */
  int getRackId();

  /** */
  void setRackId(int rackId);

  /** */
  void print() override;

private:

	/** Switch id */
	int rackId;

};

#endif /* TORSWITCH_H_ */
