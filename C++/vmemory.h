/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef VMEMORY_H_
#define VMEMORY_H_

#include <iostream>
#include <map>
#include <stdexcept>
#include <cstdlib>
#include <string>
#include <vector> 

#include <tclcl.h>

#include "util.h"
#include "vresource.h"
#include "vresourceunit.h"

/** */
class VMemory : public VResource, public TclObject {

public:

  //
  enum vmem_type {MEMORY_1GB, MEMORY_2GB, MEMORY_4GB, MEMORY_8GB};
	
	/** Class constructor */
	VMemory(int vmemId, std::string vmemType);
	
	/** Class destructor */
	~VMemory();

	/** Returns the memory type */
	vmem_type getVMemType();

	/** */
	void print() override;

	/** Defines the commands available for the class objects in the OTcl domain */
	int command(int argc, const char*const* argv);

private:

	/** */
	vmem_type stringToVMemType(std::string type);

	/** */
	std::string vmemTypeToString(vmem_type type);

	/** */
	vmem_type vmemType;

	/** */
	static std::map<std::string, vmem_type> vmemTypeCheck;

};

#endif /* VMEMORY_H_ */
