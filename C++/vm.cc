/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#include "dcmanager.h"
#include "vm.h"
#include "vmmonitor.h"

/** */
static class VmClass : public TclClass {
public:
  VmClass(): TclClass("Vm") {}
  TclObject* create(int argc, const char*const* argv) {
    //OTcl domain: set newVm [new Vm id VM_TYPE]
    return (new Vm(atoi(argv[4]), argv[5]));
  }
} class_vm;

//
std::map<std::string, Vm::vm_state> Vm::vmStateCheck = {
  {"RUNNING", Vm::RUNNING},
  {"INACTIVE", Vm::INACTIVE},
  {"PAUSED", Vm::PAUSED},
  {"SUSPENDED", Vm::SUSPENDED}
};

std::map<std::string, Vm::vm_type> Vm::vmTypeCheck = {
   {"NANO", Vm::NANO},
   {"MICRO", Vm::MICRO},
   {"SMALL", Vm::SMALL},
   {"MEDIUM", Vm::MEDIUM},
   {"LARGE", Vm::LARGE}
};

////////////

Vm::Vm(int vmId, std::string vmType) : hypervisor(NULL), vmState(Vm::INACTIVE),
    migrationState(false) {

  this->id = vmId;
  this->vmType =  stringToVmType(vmType);

  // Create a new VmSinkAgent object and assign it to the vm (Note that the agent
  // will be attached to an OTcl Node when the vm will be added to a pm)
  Tcl::instance().evalc("new Agent/VmSinkAgent");
  std::string sinkAgent = std::string(Tcl::instance().result());
  this->sinkAgent_OTcl = sinkAgent;
  Tcl::instance().evalf("%s set vmId %d", sinkAgent.c_str(), this->id);

  // The "addResource" operation is always safe in the constructor;
  // there is no need to check the return value
  switch(this->vmType){
  case NANO:{
    this->addResource(new VCpu(1, "CPU_1CORE"));
    this->addResource(new VMemory(2, "MEMORY_1GB"));
    this->addResource(new VStorage(3, "STORAGE_10GB"));
    this->addResource(new VNetworking(4, "NET_10MbE"));
    break;
  }
  case MICRO:{
    this->addResource(new VCpu(1, "CPU_1CORE"));
    this->addResource(new VMemory(2, "MEMORY_2GB"));
    this->addResource(new VStorage(3, "STORAGE_100GB"));
    this->addResource(new VNetworking(4, "NET_100MbE"));
    break;
  }
  case SMALL:{
    this->addResource(new VCpu(1, "CPU_2CORE"));
    this->addResource(new VMemory(2, "MEMORY_2GB"));
    this->addResource(new VStorage(3, "STORAGE_250GB"));
    this->addResource(new VNetworking(4, "NET_100MbE"));
    break;
  }
  case MEDIUM:{
    this->addResource(new VCpu(1, "CPU_2CORE"));
    this->addResource(new VMemory(2, "MEMORY_4GB"));
    this->addResource(new VStorage(3, "STORAGE_500GB"));
    this->addResource(new VNetworking(4, "NET_1GbE"));
    break;
  }
  case LARGE:{
    this->addResource(new VCpu(1, "CPU_2CORE"));
    this->addResource(new VCpu(2, "CPU_2CORE"));
    this->addResource(new VMemory(3, "MEMORY_8GB"));
    this->addResource(new VStorage(4, "STORAGE_1TB"));
    this->addResource(new VNetworking(5, "NET_1GbE"));
    break;
  }
  default:
    //throw std::runtime_error("The chosen vm type is not defined\n");
    break;
  }

  //
  DcManager::Instance()->addVmToVmList(this);

}

Vm::~Vm() {

  sourceAgents_OTcl.~vector();

}

VmMonitor* Vm::getVmMonitor(){

  return this->hypervisor;

}

void Vm::setVmMonitor(VmMonitor* vmm){

  this->hypervisor = vmm;

}

int Vm::getHostPmId(){

  if(this->hypervisor != NULL)
    return this->hypervisor->getPmId();
  return -1;

}


Vm::vm_type Vm::getVmType(){

  return this->vmType;

}

Vm::vm_state Vm::getVmState(){

  return this->vmState;

}

bool Vm::getMigrationState(){

  return this->migrationState;

}

void Vm::setMigrationState(bool migState){

  this->migrationState = migState;

}

double Vm::getNominalCapacityToUse(Resource::res_type type){

  double nCap = 0;
  ResMultimap::iterator it;
  // Get the iterators to the beginning and the end of the group with key equals to "type"
  std::pair<ResMultimap::iterator, ResMultimap::iterator> range = resourcesList.equal_range(type);
  // Iterate over all map elements with key equals to "type"
  for(it = range.first; it != range.second; it++){
    VResource* vres = dynamic_cast<VResource*>((*it).second);
    nCap += vres->getNominalCapacityToUse();
  }
  return nCap;
}

unsigned long long Vm::getDataSent(){

  unsigned long long bOUT = 0;
  //
  if(!sourceAgents_OTcl.empty()){
    std::vector<std::string>::iterator it;
    // Iterate over all source Agents (i.e. all the TrafficGenerator) linked with the vm
    for(it = sourceAgents_OTcl.begin(); it != sourceAgents_OTcl.end(); it++){
      VmSourceAgent* source = (VmSourceAgent*) (TclObject::lookup((*it).c_str()));
      bOUT += source->getTxBytes();
    }
  }
  return bOUT;

}

unsigned long long Vm::getDataReceived(){

  //
  VmSinkAgent* sink = (VmSinkAgent*) (TclObject::lookup(sinkAgent_OTcl.c_str()));
  return sink->getVmRxBytes();

}

bool Vm::setVmState(vm_state newState, bool freeStorage){

  //
  if(this->getHostPmId() == -1){
    std::cerr << "The state of vm " << this->getId() << " cannot be updated because the vm "
        "is not attached to any host; add the vm to a Pm if you want update its state";
    return false;
  }
  // The state of virtual machine wont'be updated if it's equal to the current one
  vm_state currentState = this->getVmState();
  if(currentState == newState){
    std::cerr << "The state of vm " << this->getId() << " is already " << newState << "\n";
    return false;
  // If the vm is currently INACTIVE or SUSPENDED and its state must be updated
  // to be RUNNING or PAUSED
  }else if(((currentState == INACTIVE) && (newState != SUSPENDED)) ||
      ((currentState == SUSPENDED) && (newState != INACTIVE))){
    // Update the usedCapacity of each virtual resource currently assigned to this vm
    ResMultimap::iterator it;
    for(it = resourcesList.begin(); it != resourcesList.end(); it++){
      VResource* vres = dynamic_cast<VResource*>((*it).second);
      double capToUse = vres->getNominalCapacityToUse();
      if(vres->setUsedCapacity(capToUse) == false)
        return false;
    }
  // If the vm is currently RUNNING or PAUSED and its state must be updated to
  // be INACTIVE or SUSPENDED
  }else if(((currentState == RUNNING) && (newState != PAUSED)) ||
      ((currentState == PAUSED) && (newState != RUNNING))){
    ResMultimap::iterator it;
    // Update the usedCapacity of each virtual resource currently assigned to this vm
    for(it = resourcesList.begin(); it != resourcesList.end(); it++){
      //
      if((*it).second->getResType() != Resource::STORAGE)
        (*it).second->setUsedCapacity(0);
      // Free the host storage only if freeStorage == true
      else if (freeStorage == true)
        (*it).second->setUsedCapacity(0);
    }
  }
  // Update the vm state
  this->vmState = newState;
  return true;

}

bool Vm::addResource(VResource* vres){

  int vNetId = -1;
  // To avoid complications with the pm that host the vm, a new virtual resource
  // can be added to the vm only if the vm is currently inactive
  if(this->getVmState() != INACTIVE){
    std::cerr << "To add a virtual resource to vm " << this->getId()
        << " update its state to be INACTIVE\n";
    return false;
  }
  // Check if the virtual resource is already assigned to this or another vm
  int vresId = vres->getId();
  int vmHostId = vres->getHostId();
  if(vmHostId != -1){
    if (vmHostId == this->getId())
      std::cerr << "The virtual resource " << vresId << " is already assigned to vm "
          << vmHostId <<"\n";
    else
      std::cerr << "The virtual resource " << vresId << " is already assigned to another "
          "vm (vm " << vmHostId << "); remove the virtual resource from vm " << vmHostId <<
          " if you want to assign it to vm " << this->getId() << " or select another "
          "virtual resource\n";
    return false;
  }
  // If the virtual resource is currently not assigned to any vm
  // check if this vm has already a virtual resource with the same id
  ResMultimap::iterator it;
  // Iterate over all map elements to see if another virtual resource
  // with the same id is already there (this is inefficient if there are many
  // virtual resources attached to this vm; generally this is not the case)
  for(it = resourcesList.begin(); it != resourcesList.end(); it++){
    if((*it).second->getId() == vresId){
      std::cerr << "Another virtual resource of type "
          << Resource::resTypeToString((*it).second->getResType()) <<
          " with id equals to "<< vresId << " is already assigned to vm " << this->getId() <<
          "; update the resource id if you want to assign it to the vm\n";
      return false;
    }
    // If the resource type is equals to "Networking" save its id for the following check
    if((*it).second->getResType() == Resource::NETWORKING)
      vNetId = (*it).second->getId();
  }
  // If none of the previously error condition is verified, the virtual resource will be added
  // to the vm, unless the resource type is equals to "Networking" and the vm has already
  // a resource of that kind; in that case the new virtual resource will replace the old one
  if((vres->getResType() == Resource::NETWORKING) && (vNetId != -1))
    this->removeResource(vNetId);
  resourcesList.insert(std::pair<Resource::res_type, VResource*>(vres->getResType(), vres));
  // Update the id of the vm that hosts the virtual resource to complete the insertion
  vres->setHostId(this->getId());
  return true;

}

bool Vm::removeResource(int vresId){

  // To avoid complications with the host pm, a virtual resource
  // can be removed only if the vm is currently inactive
  if(this->getVmState() != INACTIVE){
    std::cerr << "To remove a virtual resource from vm " << this->getId()
        << " update its state to be INACTIVE\n";
    return false;
  }
  ResMultimap::iterator it;
  // Iterate over all vm virtual resources
  for(it = resourcesList.begin(); it != resourcesList.end(); it++){
    // If the virtual resource has been previously assigned to this vm...
    if((*it).second->getId() == vresId){
      // ... update the hostId and the used capacity of the resource
      // and remove it from resourcesList
      (*it).second->setHostId(-1);
      (*it).second->setUsedCapacity(0);
      resourcesList.erase(it);
      return true;
    }
  }
  // If the virtual resource hasn't been previously assigned to this vm show an error message
  std::cerr << "There isn't any virtual resource with id equals to " << vresId
      << " in vm " << this->getId() << "\n";
  return false;

}

bool Vm::updateVResourceTypeUse(Resource::res_type type, int perc){

  ResMultimap::iterator it;
  // Get the iterators to the beginning and the end of the group with key equals to "type"
  std::pair<ResMultimap::iterator, ResMultimap::iterator> range = resourcesList.equal_range(type);
  // Iterate over all vm resources of type vresType
  for(it = range.first; it != range.second; it++){
    VResource* vres = dynamic_cast<VResource*>((*it).second);
    // Update the percentage of nominal capacity that will be used when the vm is active
    if(vres->setNominalCapacityToUse(perc) == false)
      return false;
    // If the update completes successfully, check the vm state
    // and if the vm is currently active, update the used resource
    // of the vm, accordingly to the previous update
    if((this->getVmState() == Vm::RUNNING) || (this->getVmState() == Vm::PAUSED)){
      double newCapToUse = vres->getNominalCapacityToUse();
      if(vres->setUsedCapacity(newCapToUse) == false)
        return false;
    }
  }
  return true;

}

std::vector<std::string> Vm::getSourceAgentList_OTcl(){

  return this->sourceAgents_OTcl;

}

std::string Vm::getSinkAgent_OTcl(){

  return this->sinkAgent_OTcl;

}

bool Vm::createConnection_OTcl(Vm* destVm, std::string trafficGen){

  // Show an error message if one or both of the vms hasn't been previously assigned to a pm
  if((this->getHostPmId() == -1) || (destVm->getHostPmId() == -1)){
    if((this->getHostPmId() == -1) && (destVm->getHostPmId() != -1))
      std::cerr << "Assign the vm " << this->getId() << " to a pm before create a connection\n";
    else if((this->getHostPmId() != -1) && (destVm->getHostPmId() == -1))
      std::cerr << "Assign the vm " << destVm->getId() << " to a pm before create a connection\n";
    else
      std::cerr << "The vm " << this->getId() << " and the vm " << destVm->getId() <<
          " must be assigned to a pm before create a connection\n";
    return false;
  }
  //
  Tcl& tcl = Tcl::instance();
  // Create a new VmSourceAgent and assign it to SourceAgentList
  tcl.evalc("new Agent/UDP/VmSourceAgent");
  std::string sourceAgent = std::string(tcl.result());
  this->sourceAgents_OTcl.push_back(sourceAgent);
  //
  tcl.evalf("%s set vmId %d", sourceAgent.c_str(), this->id);
  // Attach the VmSourceAgent to the Node (nodeOTcl) linked with the pm in which the vm is running
  tcl.evalf("[Simulator instance] attach-agent %s %s",
      hypervisor->getPm()->getNode_OTcl().c_str(), sourceAgent.c_str());
  // Check if the sending rate of the TrafficGenerator is compatible with the
  // virtual network resources of the two vm
  tcl.evalf("%s set rate_", trafficGen.c_str());
  double trafficRate = atof(tcl.result());  // bps
  double vm1MaxRate = this->getNominalCapacityToUse(Resource::NETWORKING)*8; // bps
  double vm2MaxRate = destVm->getNominalCapacityToUse(Resource::NETWORKING)*8; // bps
  //
  if((vm1MaxRate < trafficRate) || (vm2MaxRate < trafficRate)){
    if (vm1MaxRate <= vm2MaxRate)
      tcl.evalf("%s set rate_ %f", trafficGen.c_str(), vm1MaxRate);
    else
      tcl.evalf("%s set rate_ %f", trafficGen.c_str(), vm2MaxRate);
  }
  // Connect the VmSourceAgent to the Traffic Generator
  tcl.evalf("%s attach-agent %s", trafficGen.c_str(), sourceAgent.c_str());
  // Connect the VmSourceAgent with the VmSinkAgent of destVm
  tcl.evalf("[Simulator instance] connect %s %s", sourceAgent.c_str(),
      destVm->getSinkAgent_OTcl().c_str());
  //
  tcl.evalf("%s set sinkVmId %d", sourceAgent.c_str(), destVm->getId());
  tcl.evalf("%s add-source-vmIds %d", destVm->getSinkAgent_OTcl().c_str(), this->getId());
  return true;

}

void Vm::printNoDetails(){

  //
  std::string nCpu = getFormattedString(getNominalCapacity(Resource::CPU), util::MIPS);
  std::string nMem = getFormattedString(getNominalCapacity(Resource::MEMORY), util::BYTE);
  std::string nStor = getFormattedString(getNominalCapacity(Resource::STORAGE), util::BYTE);
  std::string nNet = getFormattedString(getNominalCapacity(Resource::NETWORKING)*8, util::BITPS);
  //
  double nCpuToUse = (getNominalCapacityToUse(Resource::CPU)/getNominalCapacity(Resource::CPU))*100;
  double nMemToUse = (getNominalCapacityToUse(Resource::MEMORY)/getNominalCapacity(Resource::MEMORY))*100;
  double nStorToUse = (getNominalCapacityToUse(Resource::STORAGE)/getNominalCapacity(Resource::STORAGE))*100;
  //
  std::string uCpu = getFormattedString(getUsedCapacity(Resource::CPU), util::MIPS);
  std::string uMem = getFormattedString(getUsedCapacity(Resource::MEMORY), util::BYTE);
  std::string uStor = getFormattedString(getUsedCapacity(Resource::STORAGE), util::BYTE);
  //
  std::string aCpu = getFormattedString(getAvailableCapacity(Resource::CPU), util::MIPS);
  std::string aMem = getFormattedString(getAvailableCapacity(Resource::MEMORY), util::BYTE);
  std::string aStor = getFormattedString(getAvailableCapacity(Resource::STORAGE), util::BYTE);
  //
  std::string rxBytes = getFormattedString(getDataReceived(), util::BYTE);
  std::string txBytes = getFormattedString(getDataSent(), util::BYTE);

  std::cout << "Vm " << getId() << ": ";
  std::cout << "Type = " << vmTypeToString(getVmType()) << ", ";
  std::cout << "State = " << vmStateToString(getVmState()) << ", ";
  std::cout << "Host_Pm = " << getHostPmId() << ", ";
  std::cout << "vNIC = " << nNet << ", ";
  std::cout << "RxBytes = " <<  rxBytes << ", ";
  std::cout << "TxBytes = " << txBytes << "\n";
  std::cout << "      VCpu = " << nCpu << "(" << nCpuToUse << "%), ";
  std::cout << "usedVCpu = " << uCpu << ", ";
  std::cout << "availVCpu = " << aCpu << "\n";
  std::cout << "      VMemory = " << nMem << "(" << nMemToUse << "%), ";
  std::cout << "usedVMemory = " << uMem << ", ";
  std::cout << "availVMemory = " << aMem << "\n";
  std::cout << "      VStorage = " << nStor << "(" << nStorToUse << "%), ";
  std::cout << "usedVStorage = " << uStor << ", ";
  std::cout << "availVStorage = " << aStor << "\n";

}

void Vm::print(){

  std::cout << "--------------------------------------------------------------";
  std::cout << "--------------------------------------------------------------\n";
  this->printNoDetails();
  std::cout << "------------------------------------------------------- Resources";
  std::cout << " ----------------------------------------------------------\n";
  // Print Resources List
  ResMultimap::iterator it;
  for(it = resourcesList.begin(); it != resourcesList.end(); it++)
    (*it).second->print();
  // Print VmSourceAgent List
  std::cout << "------------------------------------------------------- Connections";
  std::cout << " --------------------------------------------------------\n";
  std::vector<std::string>::iterator itSource;
  for(itSource = sourceAgents_OTcl.begin(); itSource != sourceAgents_OTcl.end(); itSource++){
    VmSourceAgent* source = (VmSourceAgent*) (TclObject::lookup((*itSource).c_str()));
    std::string txBytes = util::getFormattedString(source->getTxBytes(), util::BYTE);
    std::cout << "- Source (Vm_Id) = " << source->getVmId() << ", " <<
        "Destination (Vm_Id) = " << source->getSinkVmId() << ", " <<
        "TxBytes = " << txBytes << "\n";
  }
  // Print VmSinkAgent
  VmSinkAgent* sink = (VmSinkAgent*) (TclObject::lookup(sinkAgent_OTcl.c_str()));
  std::string rxBytes = util::getFormattedString(sink->getVmRxBytes(), util::BYTE);
  std::vector<int>::iterator itSink;
  std::vector<int> sourceVmIds = sink->getSourceVmIds();
  //
  if(!sourceVmIds.empty()){
    std::cout << "- Destination (Vm_Id) = " << sink->getVmId() << ", ";
    std::cout << "Source (Vm_Id) =";
    for(itSink = sourceVmIds.begin(); itSink != sourceVmIds.end(); itSink++)
      std::cout << " " << (*itSink);
    std::cout << ", RxBytes = " << rxBytes << "\n";
  }
  std::cout << "--------------------------------------------------------------";
  std::cout << "--------------------------------------------------------------\n";

}

/** OTcl commands:
 * commands-list {} -
 * print {} -
 * get-id {} -
 * get-source-agents {} -
 * get-sink-agent {} -
 * start {} -
 * suspend {} -
 * pause {} -
 * stop {} -
 * get-used-cap {resource_type} -
 * add-vresource {resource*} -
 * remove-vresource {resource_id} -
 * update-vresource-use {resource_type new_percentage}
 * attach-application {destVm* TrafficGenerator*}
 **/
int Vm::command(int argc, const char*const* argv){

  Tcl& tcl = Tcl::instance();
  //
  if (argc == 2) {
    if(strcmp(argv[1], "commands-list") == 0) {
      std::cout << "- commands-list {}\n"
                   "- print {}\n"
                   "- get-id {}\n"
                   "- get-source-agents {}\n"
                   "- get-sink-agent {}\n"
                   "- start {}\n"
                   "- suspend{}\n"
                   "- pause{}\n"
                   "- stop {}\n"
                   "- get-used-cap {resource_type}\n"
                   "- add-vresource {vresource*}\n"
                   "- remove-vresource {vresource_id}\n"
                   "- update-vresource-use {resource_type new_percentage}\n"
                   "- create-connection {destVm* TrafficGenerator*}\n";
      return (TCL_OK);
    }else if(strcmp(argv[1], "print") == 0) {
      print();
      return (TCL_OK);
    //
    } else if(strcmp(argv[1], "get-id") == 0) {
      // Pass the vm id to the OTCl domain
      tcl.result(std::to_string(this->getId()).c_str());
      return (TCL_OK);
    //
    }else if(strcmp(argv[1], "get-source-agents") == 0) {
        // Pass the sourceAgents_OTcl vector object to the OTCl domain in form
        // of associative array
        // TODO
        return (TCL_OK);
    }else if(strcmp(argv[1], "get-sink-agent") == 0) {
        // Pass the sinkAgent_OTcl object to the OTCl domain in form of const char pointer
        tcl.result(this->getSinkAgent_OTcl().c_str());
        return (TCL_OK);
    }else if(strcmp(argv[1], "start") == 0) {
      //
      if(hypervisor->updateVmState(this->getId(), Vm::RUNNING, false) == false)
        std::cout << "\"Start vm " << this->getId() << "\" failed at " <<
            Scheduler::instance().clock() << "s\n";
      return(TCL_OK);
    }else if(strcmp(argv[1], "suspend") == 0) {
      //
      if(hypervisor->updateVmState(this->getId(), Vm::SUSPENDED, false) == false)
        std::cout << "\"Suspend vm " << this->getId() << "\" failed at " <<
            Scheduler::instance().clock() << "s\n";
      return(TCL_OK);
    }else if(strcmp(argv[1], "pause") == 0) {
      //
      if(hypervisor->updateVmState(this->getId(), Vm::PAUSED, false) == false)
        std::cout << "\"Pause vm " << this->getId() << "\" failed at " <<
            Scheduler::instance().clock() << "s\n";
      return(TCL_OK);
    }else if(strcmp(argv[1], "stop") == 0) {
      //
      if(hypervisor->updateVmState(this->getId(), Vm::INACTIVE, false) == false)
        std::cout << "\"Stop vm " << this->getId() << "\" failed at " <<
            Scheduler::instance().clock() << "s\n";
      return(TCL_OK);
    }
  //
  }else if(argc == 3) {
    //
    if(strcmp(argv[1], "get-used-cap") == 0) {
      Resource::res_type t = Resource::stringToResType(argv[2]);
      double result = getUsedCapacity(t);
      tcl.evalf("set aVcap %.0f", result);
      return (TCL_OK);
    //
    }else if(strcmp(argv[1], "add-vresource") == 0){
      if(VResource* res = dynamic_cast<VResource*> (TclObject::lookup(argv[2]))){
        if(addResource(res) == false)
          return (TCL_ERROR);
        return (TCL_OK);
      }else{
        std::cerr << "The argument (" << argv[2] << ") of add-vresource doesn't represent "
            "a virtual resource. ";
        return (TCL_ERROR);
      }
    //
    }else if (strcmp(argv[1], "remove-vresource") == 0){
      int vresId = atoi(argv[2]);
      if(removeResource(vresId) == false)
        return (TCL_ERROR);
      return(TCL_OK);
    }
  //
  } else if (argc == 4){
    //
    if(strcmp(argv[1], "update-vresource-use") == 0){
      Resource::res_type t = Resource::stringToResType(argv[2]);
      int perc = atoi(argv[3]);
      //
      if(this->getHostPmId() == -1){
        std::cerr << "The vm must be assigned to a pm before updating its resource use";
        return (TCL_ERROR);
      }
      if(hypervisor->updateVmResourceTypeUse(this->getId(), t, perc)==false)
        return (TCL_ERROR);
      return(TCL_OK);
    //
    }else if(strcmp(argv[1], "create-connection") == 0) {
      Vm* destVm = (Vm*) (TclObject::lookup(argv[2]));
      std::string trafficGen = std::string(argv[3]);
      // Check if the argument "argv[3]" of the instproc is actually a TrafficGenerator
      if(dynamic_cast<TrafficGenerator*>(TclObject::lookup(argv[3])) != NULL){
        // Check if the TrafficGenerator contain the instvar rate_
        // (see createConnection_OTcl() to understands why TrafficGenerator must
        // contains this instance variable)
        tcl.evalf("lsearch [%s info vars] rate_", argv[3]);
        if(atof(tcl.result()) == -1){
          std::cerr << "If you want to use a TrafficGenerator to define a connection between "
              "two virtual machines,  it must contain an instvar rate_ that "
              "specifies the max sending rate (in bps)";
          return (TCL_ERROR);
        }else if(createConnection_OTcl(destVm, trafficGen) == false)
          return (TCL_ERROR);
        return (TCL_OK);
      }else{
        std::cerr << "The argument (" << argv[3] << ") of create-connection doesn't represent "
                  "a TrafficGenerator. ";
        return (TCL_ERROR);
      }
    }
  } // end if (argc == 4)
  return (TCL_ERROR);

}

Vm::vm_state Vm::stringToVmState(std::string type){

  std::map<std::string, vm_state>::iterator it = vmStateCheck.find(type);
  if(it == vmStateCheck.end())
    throw std::runtime_error("The chosen vm state is not defined\n");
  return it->second;

}

std::string Vm::vmStateToString(Vm::vm_state state){

  std::map<std::string, vm_state>::iterator it;
  for(it = vmStateCheck.begin(); it != vmStateCheck.end(); it++)
    if(it->second == state)
      return it->first;
  throw std::runtime_error("The chosen vm state is not defined\n");

}

Vm::vm_type Vm::stringToVmType(std::string type){

  std::map<std::string, Vm::vm_type>::iterator it = vmTypeCheck.find(type);
  if(it == vmTypeCheck.end())
    throw std::runtime_error("The chosen vm type is not defined\n");
  return it->second;

}

std::string Vm::vmTypeToString(Vm::vm_type type){

  std::map<std::string, Vm::vm_type>::iterator it;
  for(it = vmTypeCheck.begin(); it != vmTypeCheck.end(); it++)
    if(it->second == type)
      return it->first;
  throw std::runtime_error("The chosen vm type is not defined\n");

}
