/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef STORAGE_H_
#define STORAGE_H_

#include <iostream>
#include <map>
#include <stdexcept>
#include <cstdlib>
#include <string>
#include <vector> 

#include <tclcl.h>

#include "presource.h"
#include "presourceunit.h"
#include "util.h"

/** */
class Storage : public PResource, public TclObject {

public:

  //
  enum stor_type {STORAGE_1TB, STORAGE_10TB, STORAGE_25TB, STORAGE_100TB};
	
	/** Class constructor */
  Storage(int storId, std::string storType);
	
	/** Class destructor */
	~Storage();

	/** Returns the Storage type */
	stor_type getStorType();

	/** */
	void print() override;

	/** Defines the commands available for the class objects in the OTcl domain */
	int command(int argc, const char*const* argv);

private:

	/** */
	stor_type stringToStorType(std::string type);

	/** */
	std::string storTypeToString(stor_type type);

	/** */
	stor_type storType;

	/** */
	static std::map<std::string, stor_type> storTypeCheck;

};

#endif /* STORAGE_H_ */
