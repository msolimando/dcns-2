/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#include "vmsinkagent.h"

/** */
static class VmSinkAgentClass : public TclClass {
public:
  VmSinkAgentClass(): TclClass("Agent/VmSinkAgent") {}
	TclObject* create(int argc, const char*const* argv) {
	  //OTcl domain: set newVmSinkMonitor [new Agent/VmSinkAgent]
		return (new VmSinkAgent());
	}
} class_vmsinkagent;

////////////

VmSinkAgent::VmSinkAgent() : Agent(PT_NTYPE), vmId(-1), active(0), bytesRx(0), pktsRx(0),
    bytes_(0), npkts_(0), nlost_(0), expected_(-1), seqno_(0), last_packet_time_(0.0) {

  bind("vmId", &vmId);
  bind("active", &active);
  bind("expected_", &expected_);
  bind("lastPktTime_", &last_packet_time_);

}

VmSinkAgent::~VmSinkAgent(){

  sourceVmIds.~vector();

}

void VmSinkAgent::setVmId(int vmId){

  this->vmId = vmId;

}

int VmSinkAgent::getVmId(){

  return this->vmId;

}

void VmSinkAgent::setActive(bool state){

  if(state == true)
    active = 1;
  else
    active = 0;

}

bool VmSinkAgent::isActive(){

  if(active == 1)
    return true;
  else
    return false;

}

unsigned long long VmSinkAgent::getVmRxBytes(){

  return this->bytesRx;

}

unsigned long long VmSinkAgent::getVmRxPkts(){

  return this->pktsRx;

}

unsigned long long VmSinkAgent::getRxBytes(){

  return this->bytes_;

}

unsigned long long VmSinkAgent::getRxPkts(){

  return this->npkts_;

}

unsigned long long VmSinkAgent::getLostBytes(){

  return this->nlost_;

}

void VmSinkAgent::addSourceVmId(int sourceVmId){

  this->sourceVmIds.push_back(sourceVmId);

}

std::vector<int> VmSinkAgent::getSourceVmIds(){

  return this->sourceVmIds;

}

void VmSinkAgent::recv(Packet* pkt, Handler* hdl){

  // Retrieve the protocol-specific packet header (i.e. rtp) to update seqno
  hdr_rtp* p = hdr_rtp::access(pkt);
  seqno_ = p->seqno();
  // Retrieve the common packet header and update the number of bytes received
  bytes_ += hdr_cmn::access(pkt)->size();
  // Update the number of received packets
  ++npkts_;
  // If the vm linked with this agent is currenty RUNNING or PAUSED
  if(this->active == true){
    // Update the number of bytes and packets actually received from the vm
    this->bytesRx += hdr_cmn::access(pkt)->size();
    this->pktsRx++;
  }
  //Check for lost packets
  if (expected_ >= 0) {
    int loss = seqno_ - expected_;
    if(loss > 0)
      nlost_ += loss;
  }
  //
  last_packet_time_ = Scheduler::instance().clock();
  //
  expected_ = seqno_ + 1;
  //
  Packet::free(pkt);

}

/** OTcl commands:
 * commands-list {} -
 * clear {}
 * get-source-vmIds {}
 * get-bytesRx {}
 * get-pktsRx {}
 * add-source-vmId {vm_id}
 **/
int VmSinkAgent::command(int argc, const char*const* argv){

  Tcl& tcl = Tcl::instance();
  //
  if(argc == 2) {
    if(strcmp(argv[1], "commands-list") == 0){
      std::cout << "- commands-list {}\n"
                   "- clear {}"
                   "- get-source-vmIds {}\n"
                   "- get-bytesRx {}"
                   "- get-pktsRx {}"
                   "- add-source-vmId {vm_id}\n";
      return (TCL_OK);
    }else if(strcmp(argv[1], "get-source-vmIds") == 0) {
      tcl.evalc("set source-vmIds { }");
      std::vector<int>::iterator it;
      for(it = sourceVmIds.begin(); it != sourceVmIds.end(); it++){
        tcl.evalf("lappend source-vmIds %d", (*it));
      }
      return (TCL_OK);
    }else if(strcmp(argv[1], "clear") == 0) {
      expected_ = -1;
      return (TCL_OK);
    }else if(strcmp(argv[1], "get-bytesRx") == 0) {
      tcl.evalf("set rxBytes %llu", getRxBytes());
      return (TCL_OK);
    }else if(strcmp(argv[1], "get-pktsRx") == 0) {
      tcl.evalf("set rxBytes %llu", getRxPkts());
      return (TCL_OK);
    }
  //
  }else if(argc == 3) {
    if(strcmp(argv[1], "add-source-vmIds") == 0) {
     this->addSourceVmId(atoi(argv[2]));
     return (TCL_OK);
    }
  }
	return (Agent::command(argc, argv));

}
