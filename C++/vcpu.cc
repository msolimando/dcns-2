/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#include "vcpu.h"

/** */
static class VCpuClass : public TclClass {
public:
  VCpuClass() : TclClass("VCpu") {}
  TclObject* create(int argc, const char*const* argv) {
    return (new VCpu(atoi(argv[4]), argv[5]));
  }
} class_vcpu;

std::map<std::string, VCpu::vcpu_type> VCpu::vcpuTypeCheck = {
   {"CPU_1CORE", VCpu::CPU_1CORE},
   {"CPU_2CORE", VCpu::CPU_2CORE}
};

////////////

VCpu::VCpu(int vcpuId, std::string vcpuType) {

  this->resId = vcpuId;
  this->resType = Resource::CPU;
  this->vcpuType = stringToVCpuType(vcpuType);
  this->nominalCap = 0;
  this->hostId = -1;
  //
  switch(this->vcpuType){
  case CPU_1CORE:{
    this->addResUnit(new VResourceUnit(1, 250000000));
    break;
  }
  case CPU_2CORE:{
    this->addResUnit(new VResourceUnit(1, 250000000));
    this->addResUnit(new VResourceUnit(1, 250000000));
    break;
  }
  default:
    //throw std::runtime_error("The chosen virtual cpu type is not defined\n");
    break;
  }
  //
  this->nomCapToUse = this->getNominalCapacity();
}

VCpu::~VCpu(){

}

VCpu::vcpu_type VCpu::getVCpuType(){

  return this->vcpuType;

}

void VCpu::print(){

  //
  std::string nCap = getFormattedString(getNominalCapacity(), util::MIPS);
  std::string aCap = getFormattedString(getAvailableCapacity(), util::MIPS);
  std::string uCap = getFormattedString(getUsedCapacity(), util::MIPS);
  //
  double nCapToUse = (getNominalCapacityToUse()/getNominalCapacity())*100;

  std::cout << "VRes " << getId() << ": ";
  std::cout << "Type = " << Resource::resTypeToString(getResType());
  std::cout << " [" << vcpuTypeToString(getVCpuType()) << "], ";
  std::cout << "Vm_Id = " << getHostId() << ", ";
  std::cout << "nomCap = " << nCap << " (" << nCapToUse << "%), ";
  std::cout << "usedCap = " << uCap  << ", ";
  std::cout << "availCap = " << aCap << "\n";

  std::vector <ResourceUnit*>::iterator it;
  for(it = resUnitList.begin(); it != resUnitList.end(); it++)
    (*it)->print(util::MIPS);

}

/** OTcl commands:
 * print -
 * get-available-cap -
 * update-id NEWID -
 **/
int VCpu::command(int argc, const char*const* argv) {

  Tcl& tcl = Tcl::instance();
  //
  if(argc == 2) {
    //
    if(strcmp(argv[1], "print") == 0) {
      this->print();
      return (TCL_OK);
    //
    } else if (strcmp(argv[1], "get-available-cap") == 0) {
      double result = 0;
      result= getAvailableCapacity();
      char out[100];
      sprintf(out, "set vcpu_Acap %.0f", result);
      tcl.eval(out);
      return (TCL_OK);
    }
  //
  } else if(argc == 3){
    //
    if(strcmp(argv[1], "update-id") == 0) {
      int newId = atoi(argv[2]);
      if(this->updateId(newId) == false){
        std::cerr << "A virtual resource can change its id only if it's not attached to any vm\n";
        return (TCL_ERROR);
      }
      return (TCL_OK);
    }
  }
  return (TCL_ERROR);

}

VCpu::vcpu_type VCpu::stringToVCpuType(std::string type){

  std::map<std::string, VCpu::vcpu_type>::iterator it = vcpuTypeCheck.find(type);
  if (it == vcpuTypeCheck.end())
    throw std::runtime_error("The chosen virtual cpu type is not defined\n");
  return it->second;

}

std::string VCpu::vcpuTypeToString(VCpu::vcpu_type type){

  std::map<std::string, VCpu::vcpu_type>::iterator it;
  for(it = vcpuTypeCheck.begin(); it!= vcpuTypeCheck.end(); it++)
    if (it->second == type)
      return it->first;
  throw std::runtime_error("The chosen virtual cpu type is not defined\n");

}
