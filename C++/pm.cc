/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#include "dcmanager.h"
#include "pm.h"

static class PmClass : public TclClass {
public:
  PmClass() : TclClass("Pm") {}
	TclObject* create(int argc, const char*const*argv) {
	  //OTcl domain: set newHost [new Pm id]
		return (new Pm(atoi(argv[4])));
	}
} class_pm;

//
std::map<std::string, Pm::pm_state> Pm::pmStateCheck = {
  {"ON", Pm::ON},
  {"OFF", Pm::OFF},
  {"SLEEP", Pm::SLEEP}
};

////////////

Pm::Pm(int pmId): rackId(-1), pmState(Pm::OFF), rxMonitor(NULL), txMonitor(NULL) {

	this->id = pmId;
	// Create a new Node object in OTcl domain and assign it to nodeOTcl
	Tcl::instance().evalc("[Simulator instance] node");
	this->node_OTcl = std::string(Tcl::instance().result());
	//
	this->hyperV = new VmMonitor();
	this->hyperV->setPm(this);

	//
	DcManager::Instance()->addPmToPmList(this);

}

Pm::~Pm(){

  this->hostedVMs.~vector();

}

int Pm::getRackId(){

  return this->rackId;

}

void Pm::setRackId(int rackId){

  this->rackId = rackId;

}

Pm::pm_state Pm::getPmState(){

  return this->pmState;

}

bool Pm::setPmState(pm_state newState){

  pm_state currentState = this->getPmState();
  if(currentState == newState){
    std::cerr << "The state of server " << this->getId() << " is already " << newState << "\n";
    return false;
  //
  }else if((currentState == ON) && (newState == OFF)){
    std::vector<Vm*>::iterator it;
    // Update the state of each virtual machine currently not INACTIVE
    for(it = hostedVMs.begin(); it != hostedVMs.end(); it++){
      int vmId = (*it)->getId();
      Vm::vm_state vmState = (*it)->getVmState();
      // At this point its always safe to call updateVmState;
      // for this reason the method is called without checking its return value
      if(vmState != Vm::INACTIVE)
        hyperV->updateVmState(vmId, Vm::INACTIVE, false);
    }
    // Update the state of dcHost
    this->pmState=newState;
  //
  }else if((currentState == ON) && (newState == SLEEP)){
    std::vector<Vm*>::iterator it;
    // Update the state of each virtual machine currently ACTIVE or PAUSED on this server
    for(it = hostedVMs.begin(); it != hostedVMs.end(); it++){
      int vmId = (*it)->getId();
      Vm::vm_state vmState = (*it)->getVmState();
      // At this point its always safe to call updateVmState;
      // for this reason the method is called without checking its return value
      if((vmState == Vm::RUNNING) || (vmState == Vm::PAUSED) )
        hyperV->updateVmState(vmId, Vm::SUSPENDED, false);
      }
    // Update the state of dcHost
    this->pmState=newState;
  //
  }else if((currentState == OFF) || (currentState == SLEEP)){
    // Simply update the state of dcHost
    this->pmState=newState;
  }
  return true;

}

VmMonitor* Pm::getVmMonitor(){

  return this->hyperV;

}

void Pm::setTxMonitor(ULLQueueMonitor* qm){

  this->txMonitor = qm;

}

ULLQueueMonitor* Pm::getTxMonitor(){

  return this->txMonitor;

}

void Pm::setRxMonitor(ULLQueueMonitor* qm){

  this->rxMonitor = qm;

}

ULLQueueMonitor* Pm::getRxMonitor(){

  return this->rxMonitor;

}

std::string Pm::getNode_OTcl(){

 return this->node_OTcl;

}

bool Pm::addResource(PResource* res){

  // To avoid pointless complications with the hosted vms, a new resource
  // can be added only if the server is currently powered off
  if(this->getPmState() != OFF){
    std::cerr << "To add a new resource to server " << this->getId() << " set its state to OFF\n";
    return false;
  }
  // Check if the resource is already assigned to this or another server
  int resId = res->getId();
  int pmId = res->getHostId();
  if(pmId != -1){
    if (pmId == this->getId())
      std::cerr << "The resource " << resId << " is already assigned to pm " << pmId <<"\n";
    else
      std::cerr << "The resource " << resId << " is already assigned to another server "
          "(pm " << pmId << "); remove the resource from pm " << pmId <<
          " if you want to assign it to pm " << this->getId() << " or select another resource\n";
    return false;
  }
  // If the resource is currently not assigned to any dcHost
  // check if this dcHost has already another resource with the same id
  ResMultimap::iterator it;
  // Iterate over all map elements to see if another resource
  // with the same id is already there (this is inefficient if there are many
  // resources attached to the dcHost; generally this is not the case)
  for(it = resourcesList.begin(); it != resourcesList.end(); it++){
    if((*it).second->getId() == resId){
      std::cerr << "Another resource of type "
          << Resource::resTypeToString((*it).second->getResType()) <<
          " with id equals to "<< resId << " is already assigned to server " << this->getId() <<
          "; update the resource id if you want to assign it to the server\n";
      return false;
    }
  }
  // If none of the previously error condition is verified, add the resource to dcHost
  resourcesList.insert(std::pair<Resource::res_type, PResource*>(res->getResType(), res));
  // Update the id of the dcHost that hosts the resource to complete the insertion
  res->setHostId(this->getId());
  return true;

}

bool Pm::removeResource(int resId){

  // To simulate a real case and avoid complications with the hosted vms,
  // a resource used by the server can be removed only if the host is currently powered off
  if(this->getPmState() != OFF){
    std::cerr << "To remove a resource from server " << this->getId() << " set its state to OFF\n";
    return false;
  }
  ResMultimap::iterator it;
  // Iterate over all map elements
  for(it = resourcesList.begin(); it != resourcesList.end(); it++){
    // If the resource has been previously assigned to this dcHost...
    if((*it).second->getId() == resId){
      // ... update the hostId and the used capacity of the resource
      // and remove it from resourcesList
      (*it).second->setHostId(-1);
      (*it).second->setUsedCapacity(0);
      resourcesList.erase(it);
      return true;
    }
  }
  // If the resource hasn't been previously assigned to this dcHost show an error message
  std::cerr << "There isn't any resource with id equals to " << resId
      << " in dcHost " << this->getId() << "\n";
  return false;

}

unsigned long long Pm::getDataSent(){

  unsigned long long txBytes = 0;
  if(this->txMonitor != NULL)
    txBytes = this->txMonitor->getBdepartures();
  return txBytes;

}

unsigned long long Pm::getDataReceived(){

  unsigned long long rxBytes = 0;
  if(this->rxMonitor != NULL)
    rxBytes = this->rxMonitor->getBdepartures();
  return rxBytes;

}


std::vector<Vm*> Pm::getHostedVms(){

  return this->hostedVMs;

}

void Pm::printNoDetails(){

  //
  std::string nCpu = getFormattedString(getNominalCapacity(Resource::CPU), util::MIPS);
  std::string nMem = getFormattedString(getNominalCapacity(Resource::MEMORY), util::BYTE);
  std::string nStor = getFormattedString(getNominalCapacity(Resource::STORAGE), util::BYTE);
  std::string nNet = getFormattedString(getNominalCapacity(Resource::NETWORKING)*8, util::BITPS);

  //
  std::string uCpu = getFormattedString(getUsedCapacity(Resource::CPU), util::MIPS);
  std::string uMem = getFormattedString(getUsedCapacity(Resource::MEMORY), util::BYTE);
  std::string uStor = getFormattedString(getUsedCapacity(Resource::STORAGE), util::BYTE);
  //
  std::string aCpu = getFormattedString(getAvailableCapacity(Resource::CPU), util::MIPS);
  std::string aMem = getFormattedString(getAvailableCapacity(Resource::MEMORY), util::BYTE);
  std::string aStor = getFormattedString(getAvailableCapacity(Resource::STORAGE), util::BYTE);

  //
  std::string rxBytes = getFormattedString(getDataReceived(), util::BYTE);
  std::string txBytes = getFormattedString(getDataSent(), util::BYTE);

  std::cout << "Pm " << getId() << ": ";
  std::cout << "State = " << pmStateToString(getPmState()) << ", ";
  std::cout << "Rack_Id = " << getRackId() << ", ";
  std::cout << "NIC = " << nNet << ", ";
  std::cout << "RxBytes = " <<  rxBytes << ", ";
  std::cout << "TxBytes = " << txBytes << "\n";
  std::cout << "      Cpu = " << nCpu << ", ";
  std::cout << "usedCpu = " << uCpu << ", ";
  std::cout << "availCpu = " << aCpu << "\n";
  std::cout << "      Memory = " << nMem << ", ";
  std::cout << "usedMemory = " << uMem << ", ";
  std::cout << "availMemory = " << aMem << "\n";
  std::cout << "      Storage = " << nStor << ", ";
  std::cout << "usedStorage = " << uStor << ", ";
  std::cout << "availStorage = " << aStor << "\n";

}

void Pm::print(){

  std::cout << "==============================================================";
  std::cout << "==============================================================\n";
  this->printNoDetails();
  // Print Resources List
  std::cout << "======================================================= Resources";
  std::cout << " ==========================================================\n";
  ResMultimap::iterator itRes;
  for(itRes=resourcesList.begin(); itRes!=resourcesList.end(); itRes++)
    (*itRes).second->print();
  // Print VMs list
  std::cout << "==================================================== Virtual Machines";
  std::cout << " ======================================================\n";
  std::vector<Vm*>::iterator itVm;
  for(itVm=hostedVMs.begin(); itVm!=hostedVMs.end(); itVm++){
    (*itVm)->printNoDetails();
    std::cout << "----------------------------------------------------------------";
    std::cout << "------------------------------------------------------------\n";
  }
  std::cout << "==============================================================";
  std::cout << "==============================================================\n";
}

/** OTcl commands:
 * commands-list {}
 * print {} -
 * get-id {} -
 * get-node {} -
 * get-vm-list {} -
 * set-current-state {pm_state} -
 * get-available-cap {resource_type} -
 * add-resource {resource*} -
 * remove-resource {resource_id} -
 * add-vm {vm*} -
 * remove-vm {vm_id} -
 **/
int Pm::command(int argc, const char*const* argv){

	Tcl& tcl = Tcl::instance();
	//
	if(argc == 2) {
	  if(strcmp(argv[1], "commands-list") == 0) {
	    std::cout << "- commands-list {}\n"
	                 "- print {}\n"
	                 "- get-id {}\n"
	                 "- get-node {}\n"
	                 "- get-vm-list {}\n"
	                 "- set-current-state {pm_state}\n"
	                 "- get-available-cap {resource_type}\n"
	                 "- add-resource {resource*}\n"
	                 "- remove-resource {resource_id}\n"
	                 "- add-vm {vm*}\n"
	                 "- remove-vm {vm_id}\n";
	        return (TCL_OK);
	  } else if (strcmp(argv[1], "print") == 0) {
			print();
			return (TCL_OK);
		//
	  } else if(strcmp(argv[1], "get-id") == 0) {
	     // Pass the server id to the OTCl domain
	     tcl.result(std::to_string(this->getId()).c_str());
	     return (TCL_OK);
	  //
		} else if(strcmp(argv[1], "get-node") == 0) {
		  // Pass the node object to the OTCl domain in form of const char pointer
      tcl.result(this->getNode_OTcl().c_str());
      return (TCL_OK);
	  //
	  }else if(strcmp(argv[1], "get-vm-list") == 0){
	    tcl.evalc("set vmListOTcL { }");
	    std::vector<Vm*>::iterator itVm;
	    for(itVm = hostedVMs.begin(); itVm != hostedVMs.end(); itVm++)
	      tcl.evalf("lappend vmListOTcL %s", (*itVm)->name());
	    tcl.evalc("lrange $vmListOTcL 0 end");
	    return (TCL_OK);
	  }
	//
	}else if(argc == 3) {
	  //
	  if(strcmp(argv[1], "set-current-state") == 0) {
	    if(setPmState(stringToPmState(argv[2]))==false)
	      return (TCL_ERROR);
	    return (TCL_OK);
	  //
	  }else if(strcmp(argv[1], "get-available-cap") == 0) {
	    Resource::res_type t = Resource::stringToResType(argv[2]);
      double result = getAvailableCapacity(t);
      tcl.evalf("set aCap %.0f", result);
      return (TCL_OK);
    //
    }else if(strcmp(argv[1], "add-resource") == 0) {
      PResource* res = dynamic_cast<PResource*> (TclObject::lookup(argv[2]));
      if((res == 0) || (addResource(res)==false))
        return (TCL_ERROR);
      return (TCL_OK);
    //
    }else if (strcmp(argv[1], "remove-resource") == 0){
      if(removeResource(atoi(argv[2]))==false)
        return (TCL_ERROR);
      return (TCL_OK);
    //
    }else if(strcmp(argv[1], "add-vm") == 0) {
      Vm* vm = (Vm*) (TclObject::lookup(argv[2]));
	    if((vm == 0) || (hyperV->addVm(vm) == false))
	      return (TCL_ERROR);
	    return (TCL_OK);
	  //
	  }else if(strcmp(argv[1], "remove-vm") == 0){
	    if(hyperV->removeVm(atoi(argv[2]))==false)
	      return (TCL_ERROR);
	    return (TCL_OK);
	  }
	//
	}// end if (argc == 3)
	return (TCL_ERROR);

}

Pm::pm_state Pm::stringToPmState(std::string type){

  std::map<std::string, pm_state>::iterator it = pmStateCheck.find(type);
  if (it == pmStateCheck.end())
    throw std::runtime_error("The chosen state is not defined\n");
  return it->second;
}

std::string Pm::pmStateToString(Pm::pm_state state){

  std::map<std::string, pm_state>::iterator it;
  for(it = pmStateCheck.begin(); it!=pmStateCheck.end(); it++)
    if (it->second == state)
      return it->first;
  throw std::runtime_error("The chosen state is not defined\n");

}
