/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#include "resource.h"

std::map<std::string, Resource::res_type> Resource::resTypeCheck = {
   {"CPU", Resource::CPU},
   {"MEMORY", Resource::MEMORY},
   {"STORAGE", Resource::STORAGE},
   {"NETWORKING", Resource::NETWORKING}
};

////////////

Resource::~Resource() {

  this->resUnitList.~vector();

}

int Resource::getId(){

  return this->resId;

}

bool Resource::updateId(int newId){

  // To avoid complications with other resources in the same host,
  // a resource can change its id only if it's currently not attached to any host
  if(this->getHostId() != -1)
    return false;
  // Update the resId member variable of the resource
  this->resId = newId;
  // Update the hostResId member variable of each resource unit
  std::vector<ResourceUnit*>::iterator it;
  for(it = resUnitList.begin(); it != resUnitList.end(); it++)
    (*it)->setHostResourceId(newId);
  return true;

}

Resource::res_type Resource::getResType(){

  return this->resType;

}

double Resource::getNominalCapacity(){

  return this->nominalCap;

}

double Resource::getUsedCapacity(){

  std::vector<ResourceUnit*>::iterator iter;
  double result = 0;
  for(iter=this->resUnitList.begin(); iter!=this->resUnitList.end(); iter++)
    result+=(*iter)->getUsedCapacity();
  return result;

}

bool Resource::setUsedCapacity(double cap){

  std::vector<ResourceUnit*>::iterator iter;
  //
  if(cap < 0){
    std::cerr << "Error in updating the used capacity of the resource: "
        "the new capacity value must be greater than or equal to zero";
    return false;
  // Set all used Capacity of the Resource to zero
  }else if (cap == 0){
    for(iter=this->resUnitList.begin(); iter!=this->resUnitList.end(); iter++)
      (*iter)->setUsedCapacity(0);
    return true;
  // Update the used capacity only if the resource has enough nominal capacity
  }else if(getNominalCapacity() >= cap){
    double remains = cap;
    for(iter=this->resUnitList.begin(); iter!=this->resUnitList.end(); iter++){
      double nomCap = (*iter)->getNominalCapacity();
      // If the nominal capacity of the resourceUnit is greater than
      // the residual capacity to be assigned
      if(nomCap >= remains){
        (*iter)->setUsedCapacity(remains);
        return true;
      }else{
        (*iter)->setUsedCapacity(nomCap);
        remains-=nomCap;
      }
    }
  }
  std::cerr << "Error in updating the used capacity of the resource: "
      "the resource hasn't enough nominal capacity";
  return false;

}

bool Resource::increaseUsedCapacity(double increase){

  std::vector<ResourceUnit*>::iterator iter;
  // Increase the used capacity only if the resource has enough available capacity
  if(getAvailableCapacity()>=increase){
    for(iter=this->resUnitList.begin(); iter!=this->resUnitList.end(); iter++){
      double availCap = (*iter)->getAvailableCapacity();
      double usedCap = (*iter)->getUsedCapacity();
      // If the available capacity of the resourceUnit is greater than
      // the residual capacity to be assigned
      if(availCap >= increase){
        (*iter)->setUsedCapacity(usedCap+increase);
        return true;
      }else{
        (*iter)->setUsedCapacity(usedCap+availCap);
        increase-=availCap;
      }
    }
  }
  std::cerr << "Error in updating the used capacity of the resource: "
      "the resource hasn't enough available capacity";
  return false;
}

bool Resource::decreaseUsedCapacity(double decrease){

  std::vector<ResourceUnit*>::iterator iter;
  // Decrease the used capacity only if its current value is greater than zero
  if(getUsedCapacity() > 0){
    for(iter=this->resUnitList.begin(); iter!=this->resUnitList.end(); iter++){
      double usedCap = (*iter)->getUsedCapacity();
      // If the used capacity of the current unit is greater than zero decrease
      // its value, otherwise check the following resource unit (remember that a Resource
      // can have multiple unit Resources and some of them may be inactive)
      if(usedCap>0){
        // If the used capacity of the resourceUnit is greater than
        // the "decrease" value, update only the used capacity of this unit...
        if(usedCap >= decrease){
          (*iter)->setUsedCapacity(usedCap-decrease);
          return true;
        // ...otherwise set to 0 the used capacity of the current resource unit
        // and remove the remainder of the "decrease" from the following units
        // (in this case the "decrease" is greater than the used capacity of the current
        // unit; for this reason the decrease cannot be confined to the current
        // unit but must be extended also to the following ones)
        }else{
          decrease-=usedCap;
          (*iter)->setUsedCapacity(0);
        }
      }
    }
  }
  std::cerr << "Error in decreasing the used capacity of the resource: "
      "the operation cannot be completed because the used capacity of the resource is 0";
  return false;

}

double Resource::getAvailableCapacity(){

  return this->nominalCap-getUsedCapacity();

}

double Resource::getUtilization(){

  return getUsedCapacity()/getNominalCapacity();

}

int Resource::getHostId(){

  return this->hostId;

}

void Resource::setHostId(int id){

  this->hostId = id;

}

std::vector<ResourceUnit*> Resource::getResUnitList(){

  return this->resUnitList;

}

void Resource::addResUnit(ResourceUnit* unit){

  this->resUnitList.push_back(unit);
  this->nominalCap+=unit->getNominalCapacity();
  unit->setHostResourceId(this->getId());

}

Resource::res_type Resource::stringToResType(std::string type){

  std::map<std::string, Resource::res_type>::iterator it = resTypeCheck.find(type);
  if (it == resTypeCheck.end())
    throw std::runtime_error("The chosen Resource type is not defined\n");
  return it->second;

}

std::string Resource::resTypeToString(Resource::res_type type){

  std::map<std::string, Resource::res_type>::iterator it;
  for(it = resTypeCheck.begin(); it!= resTypeCheck.end(); it++)
    if (it->second == type)
      return it->first;
  throw std::runtime_error("The chosen Resource type is not defined\n");

}
