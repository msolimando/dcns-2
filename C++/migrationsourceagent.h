/*
 * DCNs-2 Network Simulator
 *
 * Copyright (c) 2016-2018 Alma Mater Studiorum - Università di Bologna
 * This file is part of DCNs-2.
 * DCNs-2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 * DCNs-2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * DCNs-2 See the GNU General Public License for more details.
 * DCNs-2 You should have received a copy of the GNU General Public License along with DCNs-2. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MIGRATIONSOURCEAGENT_H_
#define MIGRATIONSOURCEAGENT_H_

#include <iostream>

#include <tclcl.h>
#include <tcp.h>

/** */
class MigrationSourceAgent : public TcpAgent, public TimerHandler {

public:

  /** */
  MigrationSourceAgent();

  /** */
  ~MigrationSourceAgent();

  /** */
  void init(unsigned long long data, int pktSize, double bitrate);

  /** */
  void setDataToTransfer(unsigned long long data);

  /** */
  unsigned long long getDataToTransfer();

  /** */
  void setRate(double rate);

  /** */
  double getRate();

  /** */
  void setPacketSize(int pktSize);

  /** */
  int getPacketSize();

  /** */
  unsigned long long getLastPacketSeqNo();

  /** */
  unsigned long long getTxBytes();

  /** */
  unsigned long long getTxPackets();

  /** */
  void startDataTransfer();

  /** */
  void stopDataTransfer();

private:

  /** */
  void expire(Event* e) override;

  /** */
  double nextInterval(int pktSize);

  /** */
  unsigned long long dataToTransfer;

  /* Send rate in bps */
  double rate;

  /** Packet Size in Byte */
  int packetSize;

  /** */
  unsigned long long lastPacketSeqNo;

  /** */
  unsigned long long bytesTx;

  /** */
  unsigned long long packetsTx;

  /** */
  double nextPktTime;

  /* Packet inter-arrival time (sec) */
  double interval;

  /** */
  bool running;

};

#endif /* MIGRATIONSOURCEAGENT_H_ */
